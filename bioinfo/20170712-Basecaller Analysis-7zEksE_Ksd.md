# Basecaller Analysis

```
  
```

# Basecaller Analysis

### Stats about samples

`20170301_MT_3x_Arnold` => 147594 reads in pass 8 in fail
`20170320_GN_179` => 6449 reads in pass 7677 in fail
`20170322_c4_watermanag_S10` => 31248 reads in pass 12302 in fail
`20170328_a3_watermanag_S18` => 15012 reads in pass 5264 in fail
`TB` => 28217 in pass 58172 in fail
`Lambda2` => 444 reads in pass no fail

## ctcbns

\#ctcbns

### 20170328_a3_watermanag_S18

The files for this analysis are stored on nectar at: `/DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/ctcbns_testing/`

Concatenate all `fasta` files for the reads into one:

```
paste --delimiter=\\n --serial *.fasta > ../merged.fasta
```

Moved all the individual fasta files into their own folder within the result folder

```
sudo find  -name '*.fasta' -exec mv {} individual_fasta_files/ \;graphmap align -r /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta -d /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/ctcbns_testing/result/merged.fasta -o /home/ubuntu/nanoporeBasecaller/20170328_a3_watermanag_S18_ctcbns_graphmap_aln.sam
```

Had to remove line 10223 in the sam file as it had gave the following error when I tried converting it to BAM

```
Parse error at line 10223: missing colon in auxiliary data
Abortedawk 'NR!~/^(10223)$/' 20170328_a3_watermanag_S18_ctcbns_graphmap_aln.sam > 20170328_a3_watermanag_S18_ctcbns_graphmap_aln_wo_10223.sam
```

After removing that line I also found there were 442 other files that had a SAM POS field of 0, which from the #samtools docs `"POS is set as 0 for an unmapped read without coordinate"`.

```
samtools view 20170328_a3_watermanag_S18_ctcbns_graphmap_aln_sorted.bam | awk '$4 <= 0 {print}' | wc -l
```

Talking to Minh, we decided that we will align with #bwa mem# instead and hope that works out better.

```
bwa index /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta
bwa mem -t 8 -x ont2d /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/ctcbns_testing/result/merged.fasta | samtools -bS - > 20170328_a3_watermanag_S18_ctcbns_bwa.bam
samtools sort 20170328_a3_watermanag_S18_ctcbns_bwa.bam 20170328_a3_watermanag_S18_ctcbns_bwa-sorted
```

Running #japsa's error analysis tool to get stats

```
jsa.hts.errorAnalysis --bamFile=/home/ubuntu/nanoporeBasecaller/20170328_a3_watermanag_S18_ctcbns_bwa-sorted.bam --reference=/DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta > ctcbns_errorAnalysis.txt
```

The output from this was

```
Deletion 8493462 5076281 0.08325598534891991
Insertion 2879029 2127730 0.0282212831755903
MisMatch 8157503 0.07996279376440021
Match 85365268
Clipped 30315660
ReadBase 96401800
ReferenceBase 102016233
Identity 0.813814 0.077768 0.027447 0.080971
Probs 0.912775 0.087225 0.022751 0.054279 0.260956 0.402331
========================= SUMMARY============================
Total reads      :  18388
Unaligned reads  :  4
Deletion rate    : 0.0833
Insertion rate   : 0.0282
Mismatch rate    : 0.0800
Identity rate    : 0.8368
=============================================================
```

Moved all files to `/mnt/npBasecallerTesting/20170328_a3_watermanag_S18/`

Also ran analysis with #graphmap

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/ctcbns_testing/result/merged.fasta 20170328_a3_watermanag_S18_ctcbns_graphmap
```

Results from graphmap analysis

```
Deletion 8882025 6308504 0.09012449290783017
Insertion 3805904 3101538 0.03861790166723044
MisMatch 6389685 0.06483511591847228
Match 83281133
Clipped 3568981
ReadBase 93476722
ReferenceBase 98552843
Identity 0.813620 0.062424 0.037182 0.086773
Probs 0.928743 0.071257 0.034588 0.070352 0.185072 0.289745
========================= SUMMARY============================
Total reads      :  15012
Unaligned reads  :  13
Deletion rate    : 0.0901
Insertion rate   : 0.0386
Mismatch rate    : 0.0648
Identity rate    : 0.8450
=============================================================
```

### 20170322_c4_watermanag_S10

Concatenate all `fasta` files for the reads into one:

```
echo *.fasta | xargs paste --delimiter=\\n --serial > ctcbns_merged.fasta
```

Moved all the individual fasta files into their own folder within the result folder

```
sudo find  -name '*.fasta' -exec mv {} individual_fasta_files/ \;
```

Then ran analysis with the following

```
../../analysis.sh /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/ctcbns/ctcbns_merged.fasta 20170322_c4_watermanag_S10_ctcbns_bwa
```

Also ran analysis with graphmap

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/ctcbns/ctcbns_merged.fasta c4_watermanag_S10_ctcbns_graphmap
```

Results from graphmap analysis

```
Deletion 21669037 15558226 0.08728237330989577
Insertion 9902307 8010821 0.039886260575548144
MisMatch 16605624 0.06688706438646834
Match 209988948
Clipped 11535475
ReadBase 236496879
ReferenceBase 248263609
Identity 0.813388 0.064322 0.038356 0.083935
Probs 0.926717 0.073283 0.035353 0.068661 0.191015 0.282007
========================= SUMMARY============================
Total reads      :  31240
Unaligned reads  :  23
Deletion rate    : 0.0873
Insertion rate   : 0.0399
Mismatch rate    : 0.0669
Identity rate    : 0.8458
=============================================================
```

### Lambda_old

Basecalling was run by Teng on delta. Transferred the data from delta `/shares/coin/haotian.teng/deepBNS/data/Lambda_R9.4/output/result` to `/DataOnline/Data/Nanopore/Labelling/Lambda_old/ctcbns/`. Using

```
scp -3 -r m.hall@delta.imb.uq.edu.au:/shares/coin/haotian.teng/deepBNS/data/Lambda_R9.4/output/result michael@genomicsresearch.org:/DataOnline/Data/Nanopore/Labelling/Lambda_old/ctcbns/
```

Concatenate all fasta files into one and move it up a directory.

```
echo *.fasta | xargs paste --delimiter=\\n --serial > ../lambda_old_ctcbns_merged.fasta
```

Run analysis

```
/mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/ctcbns/lambda_old_ctcbns_merged.fasta lambda_old_ctcbns_graphmap 12
```

Results from analysis

```
Deletion 24866245 17270249 0.10441219037472778
Insertion 9763126 7561293 0.040994905767415005
MisMatch 16962690 0.07122553556226488
Match 196325680
Clipped 1386716
ReadBase 223051496
ReferenceBase 238154615
Identity 0.791898 0.068421 0.039381 0.100300
Probs 0.920471 0.079529 0.035451 0.080971 0.225525 0.305474
========================= SUMMARY============================
Total reads      :  34383
Unaligned reads  :  425
Deletion rate    : 0.1044
Insertion rate   : 0.0410
Mismatch rate    : 0.0712
Identity rate    : 0.8244
=============================================================
```

Calculating the time from the `meta` folder

```
# calculate user time
echo *.meta | xargs tail -n1 | awk '{sum += $3} END {printf "%.2f\n", sum}'
### 46374434.22

# calculate sys time
echo *.meta | xargs tail -n1 | awk '{sum += $2} END {printf "%.2f\n", sum}'
###  4357170.87
```

### Lambda_old subset

For this analysis we will only use 1000 reads.
I created a folder on nectar under the Lambda_old folder called `1000subset` to store this data. To get the 100 reads that we will use I generated a random subset of 1000 filenames `find . -type f | shuf -n 1000 > ../../1000subset/filenames.txt`

Put together a script to copy the files into the right folder.

```
#!/bin/bash

BASE=/DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass/

while IFS='' read -r line || [[ -n "$line" ]]; do
	cp ${BASE}${line:2} .
done < "$1"
```

I ran this script from the reads folder within the subdirectory as `../copyfiles.sh ../filenames.txt .`

## GN_179

**Step-size 20**

This analysis and #basecalling was run by Teng.

Results:

```
Deletion 10962052 7759452 0.09358451324857
Insertion 4586008 3680598 0.03915136750254861
MisMatch 8734834 0.07457045343308531
Match 97438435
Clipped 8428004
ReadBase 110759277
ReferenceBase 117135321
Identity 0.800504 0.071761 0.037676 0.090059
Probs 0.917730 0.082270 0.034666 0.073083 0.197429 0.292153
========================= SUMMARY============================
Total reads      :  6448
Unaligned reads  :  7
Deletion rate    : 0.0936
Insertion rate   : 0.0392
Mismatch rate    : 0.0746
Identity rate    : 0.8318
=============================================================
```

Extract the time information from the `meta` folder.

```
 tail -n1 *.meta | awk '{sum += $3} END {printf "%.2f\n", sum}' 
```

The time output was

```
user time (seconds): 24777456.18
system time (seconds): 3639296.87
wall clock (seconds): 6440529.97
```

**Step-size 100**

Extract signal

```
python /home/ubuntu/sw/ctcbns_package/ctcbns/utils/extract_sig_ref.py --input_dir /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass/ --output_dir /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/ctcbns/output
```

Run evaluation

```
/usr/bin/time -v -o /mnt/npBasecallerTesting/GN_179/ctcbns/GN_179_ctcbns_time.log python /home/ubuntu/sw/ctcbns_package/ctcbns/ctcbns_eval.py --input /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/ctcbns/output/raw/ --output /mnt/npBasecallerTesting/GN_179/ctcbns/reads --model /home/ubuntu/sw/ctcbns_package/model/crnn5+5_resnet --start 0 --jump 100 --segment_len 200
```

* * *

## Metrichor

### 20170328_a3_watermanag_S18

The files for this analysis are stored on nectar at: `/DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall`

Extracting the fastq file for the #Metrichor data using japsa

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/ --output=/DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/metrichor.fastq --format='fastq'
mv metrichor.fastq Metrichor_Basecall/
```

Made a script to run the exact analysis as above - called `analysis.sh`

```
#!/bin/bash 
                                                                                
# example usage ./analysis.sh    
                                                                                
reference=$1                                                                    
inFile=$2                                                                       
outFile=$3                                                                      
                                                                                
                                                                                
# index reference 
bwa index $reference                                                            
                                                                                
echo "Indexing finished."                                                       
echo "Starting alignment with bwa mem..."                                       
                                                                                
# align 
bwa mem -t8 -x ont2d $reference $inFile | samtools view -bS - > \               
    ${outFile}.bam                                                              
                                                                                
echo "Alignment finished."                                                      
echo "Sorting file..."                                                          
                                                                                
samtools sort ${outFile}.bam ${outFile}-sorted                                  
                                                                                
echo "File sorted and saved as ${outFile}-sorted.bam"                           
echo "Starting analysis..."                                                     
                                                                                
jsa.hts.errorAnalysis --bamFile=${outFile}-sorted.bam --reference=$reference > \
    ${outFile}_errorAnalysis.txt                                                
                                                                                
echo "Error analysis finished. Summary saved in ${outFile}_errorAnalysis.txt" 
```

Then ran analysis with the following

```
../analysis.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/metrichor.fastq 20170328_a3_watermanag_S18_metrichor_bwa
```

The output from the analysis was

```
Deletion 7345067 4232496 0.07192164601285797
Insertion 1311725 952554 0.012844187958559957
MisMatch 4729565 0.046311095559074214
Match 90051326
Clipped 27565774
ReadBase 96092616
ReferenceBase 102125958
Identity 0.870585 0.045724 0.012681 0.071010
Probs 0.950100 0.049900 0.010050 0.044656 0.273816 0.423763
========================= SUMMARY============================
Total reads      :  18170
Unaligned reads  :  4
Deletion rate    : 0.0719
Insertion rate   : 0.0128
Mismatch rate    : 0.0463
Identity rate    : 0.8818
=============================================================
```

Also ran analysis with graphmap

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/metrichor.fastq 20170328_a3_watermanag_S18_metrichor_graphmap
```

Results from graphmap analysis

```
Deletion 7546342 5145376 0.07673080027624683
Insertion 1885717 1538005 0.01917386920769339
MisMatch 3657804 0.037192354676432206
Match 87144131
Clipped 3579617
ReadBase 92687652
ReferenceBase 98348277
Identity 0.869407 0.036493 0.018813 0.075287
Probs 0.959717 0.040283 0.016938 0.056666 0.184393 0.318163
========================= SUMMARY============================
Total reads      :  15012
Unaligned reads  :  8
Deletion rate    : 0.0767
Insertion rate   : 0.0192
Mismatch rate    : 0.0372
Identity rate    : 0.8861
=============================================================
```

### 20170322_c4_watermanag_S10

Data stored at `/DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall`

Extracting the fastq file for the Metrichor data using japsa

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall --output=/DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/metrichor.fastq --format='fastq'
mv metrichor.fastq Metrichor_Basecall/
```

Then ran analysis with the following

```
../../analysis.sh /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/metrichor.fastq 20170322_c4_watermanag_S10_metrichor_bwa
```

The output from the analysis was

```
Deletion 18126231 10613660 0.07032281411043463
Insertion 3269888 2400014 0.012685909496902079
MisMatch 12279986 0.04764162901580255
Match 227351258
Clipped 88283602
ReadBase 242901132
ReferenceBase 257757475
Identity 0.870986 0.047045 0.012527 0.069442
Probs 0.948755 0.051245 0.010015 0.044292 0.266026 0.414459
========================= SUMMARY============================
Total reads      :  39370
Unaligned reads  :  1
Deletion rate    : 0.0703
Insertion rate   : 0.0127
Mismatch rate    : 0.0476
Identity rate    : 0.8820
=============================================================
```

Also ran analysis with graphmap

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/metrichor.fastq 20170322_c4_watermanag_S10_metrichor_graphmap
```

Results from graphmap analysis

```
Deletion 18524560 12813834 0.07523080638768861
Insertion 4748709 3879085 0.019285165605578455
MisMatch 9454429 0.03839574692220212
Match 218257372
Clipped 10614292
ReadBase 232460510
ReferenceBase 246236361
Identity 0.869603 0.037669 0.018920 0.073807
Probs 0.958481 0.041519 0.017035 0.056272 0.183129 0.308279
========================= SUMMARY============================
Total reads      :  31248
Unaligned reads  :  5
Deletion rate    : 0.0752
Insertion rate   : 0.0193
Mismatch rate    : 0.0384
Identity rate    : 0.8864
=============================================================
```

### Lambda_old

Extract fastq file and run analysis with

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass --output=/DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/metrichor.fastq --format='fastq' && /mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/metrichor.fastq lambda_old_metrichor_graphmap
```

Result of error analysis

```
Deletion 21289710 14552137 0.08931257494555592
Insertion 5670781 4466968 0.023789523345425306
MisMatch 10887281 0.04567329006669546
Match 206196049
Clipped 1182582
ReadBase 222754111
ReferenceBase 238373040
Identity 0.844914 0.044612 0.023237 0.087237
Probs 0.949847 0.050153 0.020577 0.067035 0.212284 0.316471
========================= SUMMARY============================
Total reads      :  34383
Unaligned reads  :  228
Deletion rate    : 0.0893
Insertion rate   : 0.0238
Mismatch rate    : 0.0457
Identity rate    : 0.8650
=============================================================
```

### Lambda_old subset

Data stored at `/DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall`

Extracting the fastq file for the Metrichor data using japsa

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/reads --output=/DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/metrichor/metrichor.fastq --format='fastq'
```

Then ran analysis with the following

```
../../analysis.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/metrichor/metrichor.fastq lambda_old_metrichor_bwa
```

The output from the analysis was

```
Deletion 593069 339725 0.08528570045998589
Insertion 89421 63800 0.01285909838624578
MisMatch 361204 0.05194258366049944
Match 5999636
Clipped 2043452
ReadBase 6450261
ReferenceBase 6953909
Identity 0.851818 0.051283 0.012696 0.084203
Probs 0.943214 0.056786 0.010030 0.053409 0.286526 0.427175
========================= SUMMARY============================
Total reads      :  1192
Unaligned reads  :  3
Deletion rate    : 0.0853
Insertion rate   : 0.0129
Mismatch rate    : 0.0519
Identity rate    : 0.8628
=============================================================
```

Also ran analysis with graphmap

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/metrichor/metrichor.fastq lambda_old_metrichor_graphmap
```

Results from graphmap analysis

### GN_179

Extract fastq file and run analysis with

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass --output=/DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/metrichor.fastq --format='fastq' && /mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/metrichor.fastq GN_179_metrichor_graphmap
```

Result of error analysis

```
Deletion 9112564 6160371 0.07794831718570615
Insertion 2070102 1714799 0.017707526367196396
MisMatch 4314142 0.036902907787553174
Match 103478499
Clipped 8314444
ReadBase 109862743
ReferenceBase 116905205
Identity 0.869748 0.036261 0.017399 0.076592
Probs 0.959977 0.040023 0.015908 0.057150 0.171636 0.323970
========================= SUMMARY============================
Total reads      :  6449
Unaligned reads  :  7
Deletion rate    : 0.0779
Insertion rate   : 0.0177
Mismatch rate    : 0.0369
Identity rate    : 0.8851
=============================================================
```

### MT_3x_Arnold

Extract fastq file and run analysis with

```
jsa.np.npreader --folder=/DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass --output=/DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/metrichor.fastq --format='fastq' && /mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/metrichor.fastq MT_3x_Arnold_metrichor_graphmap
```

Result of error analysis

```
Deletion 43833678 30082897 0.07631264201199499
Insertion 13757608 10382903 0.02395143328482174
MisMatch 24978688 0.04348687498396359
Match 505583656
Clipped 30620199
ReadBase 544319952
ReferenceBase 574396022
Identity 0.859612 0.042470 0.023391 0.074528
Probs 0.952920 0.047080 0.019570 0.056700 0.245297 0.313704
========================= SUMMARY============================
Total reads      :  147594
Unaligned reads  :  178
Deletion rate    : 0.0763
Insertion rate   : 0.0240
Mismatch rate    : 0.0435
Identity rate    : 0.8802
=============================================================
```

## Human 10K subset

Extract the fastq from the fast5 files and run the analysis with `bwa`:

```
jsa.np.npreader \
--folder=/DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads/ \
--output=/DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/metrichor/human_10k_subset_metrichor.fastq \
--format='fastq'
```

Ran assessment with:

```
/mnt/npBasecallerTesting/basecaller_assessment.sh \
-r /DataOnline/Data/H.sapiens/Reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa \
-i /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/metrichor/human_10k_subset_metrichor.fastq \
-o human_10k_subset_metrichor \
-t 14 \
-a minimap2
```

Assessment output:

```
Deletion 6199831 3152300 0.11945657031261847
Insertion 1470628 711731 0.028335639646581572
MisMatch 2950476 0.0568489276158807
Match 42749986
Clipped 61509014
ReadBase 47171090
ReferenceBase 51900293
Identity 0.800998 0.055282 0.027555 0.116165
Probs 0.935439 0.064561 0.015574 0.068977 0.516036 0.491551
========================= SUMMARY============================
Total reads      :  11420
Unaligned reads  :  3242
Deletion rate    : 0.1195
Insertion rate   : 0.0283
Mismatch rate    : 0.0568
Identity rate    : 0.8237
============================================================= 
```

* * *

## basecRAWller

\#basecrawller Downloaded the software from [the website](https://basecrawller.lbl.gov/download/basecrawller/) and then copied it onto my VM under `~/sw/`. Installation:\```
tar -zxvf basecrawller-0.1.tar.gz
cd basecrawller
sudo pip install .

```
  

  
  
  

### 20170328\_a3\_watermanag\_S18

  
Basecalled the data with the following command
  
```

/usr/bin/time -v -o a3_watermanag_S18_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass --out-filename basecrawller.fasta

```
  

Time output
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass --out-filename basecrawller.fasta"
        User time (seconds): 856200.68
        System time (seconds): 194136.65
        Percent of CPU this job got: 768%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 37:56:26
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 1958928
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 113
        Minor (reclaiming a frame) page faults: 43233438
        Voluntary context switches: 14414298340
        Involuntary context switches: 2371706688
        Swaps: 0
        File system inputs: 29408648
        File system outputs: 245272
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

  
  
```

../analysis.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /mnt/npBasecallerTesting/20170328_a3_watermanag_S18/basecrawller/basecrawller.fasta 20170328_a3_watermanag_S18_basecrawller_bwa

```
  

The results are
  
```

Deletion 5648088 4018172 0.05572661964582753
Insertion 8794855 6319455 0.08677406220037727
MisMatch 13784084 0.136000077589821
Match 81921329
Clipped 27476893
ReadBase 104500268
ReferenceBase 101353501
Identity 0.743736 0.125141 0.079846 0.051277
Probs 0.855974 0.144026 0.066030 0.041985 0.281460 0.288578
========================= SUMMARY============================
Total reads      :  17812
Unaligned reads  :  147
Deletion rate    : 0.0557
Insertion rate   : 0.0868
Mismatch rate    : 0.1360

# Identity rate    : 0.8083

```
  

Also ran analysis with graphmap
  
```

/mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/basecRAWller/basecrawller.fasta 20170328_a3_watermanag_S18_basecrawller_graphmap

```
  

Results from graphmap analysis
  
```

Deletion 7146575 5809569 0.07280496085239946
Insertion 10313105 8595912 0.10506364318455835
MisMatch 10139018 0.1032901506766211
Match 80874957
Clipped 3890039
ReadBase 101327080
ReferenceBase 98160550
Identity 0.745572 0.093470 0.095075 0.065883
Probs 0.888599 0.111401 0.094446 0.063832 0.166506 0.187084
========================= SUMMARY============================
Total reads      :  15012
Unaligned reads  :  79
Deletion rate    : 0.0728
Insertion rate   : 0.1051
Mismatch rate    : 0.1033

# Identity rate    : 0.8239

```
  

  
  
  
  
  
  

### 20170322\_c4\_watermanag\_S10

  
Basecalled the data with the following command
  
  
```

/usr/bin/time -v -o c4_watermanag_S10_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass/ --out-filename 20170322_c4_watermanag_S10_basecrawller.fasta

```
  

Time output from this run
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass/ --out-filename 20170322_c4_watermanag_S10_basecrawller.fasta"
        User time (seconds): 2187347.38
        System time (seconds): 501703.78
        Percent of CPU this job got: 748%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 99:43:49
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 6914388
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 82
        Minor (reclaiming a frame) page faults: 146528191
        Voluntary context switches: 37736605814
        Involuntary context switches: 6133353639
        Swaps: 0
        File system inputs: 44628888
        File system outputs: 612064
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

Ran error analysis with
  
```

/mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/basecRAWller/20170322_c4_watermanag_S10_basecrawller.fasta c4_watermanag_S10_basecrawller_graphmap

```
  

Results from error analysis
  
```

Deletion 17557287 14376597 0.0716001146317163
Insertion 25510675 21326402 0.10403470959564874
MisMatch 25246795 0.10295858443752966
Match 202409031
Clipped 11533500
ReadBase 253166501
ReferenceBase 245213113
Identity 0.747659 0.093257 0.094231 0.064853
Probs 0.889101 0.110899 0.093678 0.063151 0.164021 0.181161
========================= SUMMARY============================
Total reads      :  31248
Unaligned reads  :  259
Deletion rate    : 0.0716
Insertion rate   : 0.1040
Mismatch rate    : 0.1030

# Identity rate    : 0.8254

```
  

  
  
  

### Lambda\_old

  
Basecalled the data with the following command
  
```

/usr/bin/time -v -o lambda_old_basecrawller_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass --out-filename /DataOnline/Data/Nanopore/Labelling/Lambda_old/basecRAWller/lambda_old_basecrawller.fasta

```
  

Time output from this run
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass --out-filename /DataOnline/Data/Nanopore/Labelling/Lambda_old/basecRAWller/lambda_old_basecrawller.fasta"
        User time (seconds): 1980742.12
        System time (seconds): 437504.20
        Percent of CPU this job got: 713%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 94:05:01
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 6401036
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 90
        Minor (reclaiming a frame) page faults: 262385591
        Voluntary context switches: 32734314271
        Involuntary context switches: 5414937019
        Swaps: 0
        File system inputs: 48577768
        File system outputs: 570560
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

Ran analysis with graphmap
  
```

../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/basecRAWller/lambda_old_basecrawller.fasta lambda_old_basecrawller_graphmap

```
  

Results from graphmap analysis
  
  
```

Deletion 18771693 15079391 0.07891994099843337
Insertion 23808866 19847409 0.10009722085054376
MisMatch 25128598 0.10564563737183166
Match 193957122
Clipped 1520460
ReadBase 242894586
ReferenceBase 237857413
Identity 0.741239 0.096033 0.090989 0.071739
Probs 0.885302 0.114698 0.090592 0.068829 0.166386 0.196695
========================= SUMMARY============================
Total reads      :  34380
Unaligned reads  :  963
Deletion rate    : 0.0789
Insertion rate   : 0.1001
Mismatch rate    : 0.1056

# Identity rate    : 0.8154

```
  

  
  
  
  
  

### Lambda\_old subset

  
Basecalled the data with the following command
  
```

/usr/bin/time -v -o lambda_old_basecrawller_time.log basecRAWller call --processes 8 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/reads --out-filename /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/basecrawller/lambda_old_basecrawller.fasta

```
  

Time output from this run
  
```

 Command being timed: "basecRAWller call --processes 8 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/reads --out-filename /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/basecrawller/lambda_old_basecrawller.fasta"
        User time (seconds): 61038.92
        System time (seconds): 8472.99
        Percent of CPU this job got: 736%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 2:37:22
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 1737828
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 1911224
        Voluntary context switches: 654386535
        Involuntary context switches: 108555612
        Swaps: 0
        File system inputs: 323568
        File system outputs: 16576
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

After this the basecalled data was analysed with
  
```

../../analysis.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/basecrawller/lambda_old_basecrawller.fasta lambda_old_basecrawller_bwa

```
  

The results from this were
  
```

Deletion 416550 292488 0.061200661328572256
Insertion 543409 395869 0.0798391313693389
MisMatch 910915 0.1338341145459522
Match 5478834
Clipped 723794
ReadBase 6933158
ReferenceBase 6806299
Identity 0.745449 0.123939 0.073936 0.056676
Probs 0.857441 0.142559 0.061954 0.045775 0.271509 0.297833
========================= SUMMARY============================
Total reads      :  1052
Unaligned reads  :  48
Deletion rate    : 0.0612
Insertion rate   : 0.0798
Mismatch rate    : 0.1338
Identity rate    : 0.8050
============================================================= 

```
  

Also ran analysis with graphmap
  
```

../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/1000subset/basecrawller/lambda_old_basecrawller.fasta lambda_old_basecrawller_graphmap

```
  

Results from graphmap analysis
  
  

  

  
  
  
  
  

### 20170320\_GN\_179

  
Basecalled with command
  
```

/usr/bin/time -v -o GN_179_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass/ --out-filename GN_179_basecrawller.fasta

```
  

Time results for this run are:
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass/ --out-filename GN_179_basecrawller.fasta"
        User time (seconds): 1068396.93
        System time (seconds): 237059.66
        Percent of CPU this job got: 758%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 47:47:16
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 3296488
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 1
        Minor (reclaiming a frame) page faults: 82764681
        Voluntary context switches: 18150552761
        Involuntary context switches: 2558970240
        Swaps: 0
        File system inputs: 24141944
        File system outputs: 284472
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0 

```
  

Ran analysis with graphmap as follows
  
```

../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/basecRAWller/GN_179_basecrawller.fasta GN_179_basecrawller_graphmap

```
  

Results from graphmap analysis
  
  
```

Deletion 8628100 7069408 0.07402993932360963
Insertion 12061498 10110271 0.10348882895328508
MisMatch 11954531 0.10257104166296292
Match 95966161
Clipped 9293565
ReadBase 119982190
ReferenceBase 116548792
Identity 0.746178 0.092952 0.093783 0.067087
Probs 0.889229 0.110771 0.093682 0.065506 0.161773 0.180653
========================= SUMMARY============================
Total reads      :  6449
Unaligned reads  :  37
Deletion rate    : 0.0740
Insertion rate   : 0.1035
Mismatch rate    : 0.1026
Identity rate    : 0.8234
============================================================= 

```
  

  

### 20170301\_MT\_3x\_Arnold

  
Basecalled and error analysis with
  
```

/usr/bin/time -v -o 20170301_MT_3x_Arnold_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass/ --out-filename /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/basecRAWller/20170301_MT_3x_Arnold_basecrawller.fasta && /mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/basecRAWller/20170301_MT_3x_Arnold_basecrawller.fasta MT_3x_Arnold_basecrawller_graphmap

```
  

Time output
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass/ --out-filename /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/basecRAWller/20170301_MT_3x_Arnold_basecrawller.fasta"
        User time (seconds): 5118274.80
        System time (seconds): 1419708.35
        Percent of CPU this job got: 734%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 247:20:46
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 4221364
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 109
        Minor (reclaiming a frame) page faults: 228933314
        Voluntary context switches: 102421911627
        Involuntary context switches: 23348369501
        Swaps: 0
        File system inputs: 135976384
        File system outputs: 1483384
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

Results from error analysis
  
```

Deletion 41084347 33757044 0.07174132950693324
Insertion 62122694 50998909 0.10847841052730824
MisMatch 59672835 0.10420047611680085
Match 471916161
Clipped 32402156
ReadBase 593711690
ReferenceBase 572673343
Identity 0.743414 0.094003 0.097862 0.064721
Probs 0.887746 0.112254 0.095937 0.063502 0.179062 0.178348
========================= SUMMARY============================
Total reads      :  147594
Unaligned reads  :  2273
Deletion rate    : 0.0717
Insertion rate   : 0.1085
Mismatch rate    : 0.1042

# Identity rate    : 0.8241

```
  

  
  

Human 10k subset
----------------

Full dataset located on nectar at `/DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/reads`
To generate the subset of 10000 files I ran the following:
```

cd /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/reads
mkdir ../subset_for_basecaller_project
mkdir ../subset_for_basecaller_project/reads
find `pwd` -type f | shuf -n 10000 > ../subset_for_basecaller_project/human_np_10k_subset_filenames.txt

```
  

I then copied those files into a separate directory using the script from the Lambda subset:
```

./copyfiles.sh human_np_10k_subset_filenames.txt reads/ 

```
  

Then ran base calling on the subset with basecrawller with the following:
```

/usr/bin/time -v -o human_10k_subset_time.log basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads --out-filename /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/basecrawller/human_10k_subset_basecrawller.fasta 

# error anaysis with

/mnt/npBasecallerTesting/basecaller_assessment.sh \\
\-r /DataOnline/Data/H.sapiens/Reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa \\
\-i /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/basecrawller/human_10k_subset_basecrawller.fasta \\
\-o human_10k_subset_basecrawller \\
\-t 12 \\
\-a minimap2

```
  

Output from time:
  
```

Command being timed: "basecRAWller call --processes 4 --fast5-basedir /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads --out-filename /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/basecrawller/human_10k_subset_basecrawller.fasta"
        User time (seconds): 2724557.91
        System time (seconds): 964598.30
        Percent of CPU this job got: 1188%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 86:12:24
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 16745692
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 255
        Minor (reclaiming a frame) page faults: 996734019
        Voluntary context switches: 63900905762
        Involuntary context switches: 21891465136
        Swaps: 0
        File system inputs: 42963928
        File system outputs: 208240
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0

```
  

Output from assessment:
```

Deletion 3694841 2200310 0.07698387813213114
Insertion 5211991 2873872 0.10859446454387736
MisMatch 4850863 0.10107018029400022
Match 39449293
Clipped 76803336
ReadBase 49512147
ReferenceBase 47994997
Identity 0.741431 0.091170 0.097957 0.069443
Probs 0.890500 0.109500 0.064873 0.049668 0.448604 0.404491
========================= SUMMARY============================
Total reads      :  11991
Unaligned reads  :  3472
Deletion rate    : 0.0770
Insertion rate   : 0.1086
Mismatch rate    : 0.1011
Identity rate    : 0.8219
============================================================= 

`````
* * *

Albacore
--------

### Installation

Following instructions on the [Albacore website](https://community.nanoporetech.com/protocols/albacore-offline-basecalli/v/abec_2003_v1_revq_29nov2016/albacore-software-overview) Install `pip3` for #python 3 `sudo apt-get install python3-pip````
sudo apt-get update
sudo apt-get install wget
cd ~/sw/
mkdir albacore
cd albacore
wget https://mirror.oxfordnanoportal.com/software/analysis/python3-ont-albacore_2.0.2-1~xenial_all.deb
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt trusty-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt-get update
wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.0.2-cp36-cp36m-manylinux1_x86_64.whl
sudo pip3 install ont_albacore-1.1.1-cp35-cp35m-manylinux1_x86_64.whl
`````

Need #MinKnow version to be able to run #albacore.

### 20170328_a3_watermanag_S18

Run basecalling

```
/usr/bin/time -v -o a3_watermanag_S18_albacore_time.log read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass -s /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Albacore/pass_folder_only/ -c r94_450bps_linear.cfg -o 'fastq'
```

The time output for this basecall was

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass -s /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Albacore/pass_folder_only/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 28125.85
        System time (seconds): 149.17
        Percent of CPU this job got: 385%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 2:02:13
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 1849276
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 20
        Minor (reclaiming a frame) page faults: 1408316
        Voluntary context switches: 4125785
        Involuntary context switches: 1423809
        Swaps: 0
        File system inputs: 44396272
        File system outputs: 531616
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

In this

* **Real** is wall clock time - time from start to finish of the call. This is all elapsed time including time slices used by other processes and time the process spends blocked (for example if it is waiting for I/O to complete).
* **User** is the amount of #CPU time# spent in user-mode code (outside the kernel) within the process. This is only actual CPU time used in executing the process. Other processes and time the process spends blocked do not count towards this figure.
* **Sys** is the amount of CPU time spent in the kernel within the process. This means executing CPU time spent in system calls within the kernel, as opposed to library code, which is still running in user-space. Like 'user', this is only CPU time used by the process. See below for a brief description of kernel mode (also known as 'supervisor' mode) and the system call mechanism.

Joined all the fastq files together using the following command

```
paste --delimiter=\\n --serial *.fastq > ../20170328_a3_watermanag_S18_passOnly_albacore_merged.fastq
```

Ran the analysis using

```
../../analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Albacore/pass_folder_only/20170328_a3_watermanag_S18_passOnly_albacore_merged.fastq 20170328_a3_watermanag_S18_albacore_graphmap
```

The results from this were

```
Deletion 5807627 4038321 0.059109215961869015
Insertion 3207142 2499125 0.03264184306230075
MisMatch 3946920 0.040171200158725766
Match 88497932
Clipped 3679356
ReadBase 95651994
ReferenceBase 98252479
Identity 0.872248 0.038901 0.031610 0.057241
Probs 0.957305 0.042695 0.027034 0.043684 0.220763 0.304652
========================= SUMMARY============================
Total reads      :  15012
Unaligned reads  :  7
Deletion rate    : 0.0591
Insertion rate   : 0.0326
Mismatch rate    : 0.0402
Identity rate    : 0.9007
=============================================================
```

### 20170322_c4_watermanag_S10

Run basecalling

```
/usr/bin/time -v -o a3_watermanag_S18_albacore_time.log read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass -s /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Albacore/ -c r94_450bps_linear.cfg -o 'fastq'
```

Script to run Albacore and then do error analysis. Saved as

```
#!/bin/bash

filename=$1
analysisOutput=$2
inputDir=$3
baseCallOutput=$4
reference=$5

# basecall
/usr/bin/time -v -o ${analysisOutput}/${filename}_albacore_time.log read_fast5_basecaller.py -r -t 4 -i $inputDir -s $baseCallOutput -c r94_450bps_linear.cfg -o 'fastq'

# join fastq files together
paste --delimiter=\\n --serial ${baseCallOutput}/workspace/*.fastq > ${baseCallOutput}/${filename}_albacore_merged.fastq

# run error analysis
/mnt/npBasecallerTesting/analysis_graphmap.sh $reference ${baseCallOutput}/${filename}_albacore_merged.fastq ${analysisOutput}/${filename}_graphmap
```

Run the script with

```
/mnt/npBasecallerTesting/run_and_analyse_albacore.sh c4_watermanag_S10 /mnt/npBasecallerTesting/20170322_c4_watermanag_S10/Albacore /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass/ /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Albacore/ /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta
```

The time output for this basecall was

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass/ -s /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Albacore/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 68507.91
        System time (seconds): 360.27
        Percent of CPU this job got: 389%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 4:55:01
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 2354372
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 49
        Minor (reclaiming a frame) page faults: 1110709
        Voluntary context switches: 10020724
        Involuntary context switches: 3085749
        Swaps: 0
        File system inputs: 67990736
        File system outputs: 1279424
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

The results from analysis with graphmap were

```
Deletion 14181354 10042651 0.05763132299671059
Insertion 8052224 6294418 0.03272327326331921
MisMatch 10182399 0.04138004915823855
Match 221706496
Clipped 10932137
ReadBase 239941119
ReferenceBase 246070249
Identity 0.872440 0.040069 0.031686 0.055805
Probs 0.956089 0.043911 0.027144 0.043308 0.218301 0.291841
========================= SUMMARY============================
Total reads      :  31245
Unaligned reads  :  5
Deletion rate    : 0.0576
Insertion rate   : 0.0327
Mismatch rate    : 0.0414
Identity rate    : 0.9010
=============================================================
```

### Lambda_old

Basecall and analyse with

```
/mnt/npBasecallerTesting/run_and_analyse_albacore.sh Lambda_old /mnt/npBasecallerTesting/Lambda_old/Albacore/ /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass/ /DataOnline/Data/Nanopore/Labelling/Lambda_old/Albacore/ /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta
```

Time output

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass/ -s /DataOnline/Data/Nanopore/Labelling/Lambda_old/Albacore/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 64212.07
        System time (seconds): 186.92
        Percent of CPU this job got: 384%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 4:39:14
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 2490436
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 4
        Minor (reclaiming a frame) page faults: 649136
        Voluntary context switches: 9308602
        Involuntary context switches: 3066636
        Swaps: 0
        File system inputs: 71469920
        File system outputs: 1248424
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0 
```

Results

```
Deletion 15139244 10577418 0.06345504108015831
Insertion 9118850 7010086 0.03822099712203606
MisMatch 11191598 0.04690876974059059
Match 212251369
Clipped 1127926
ReadBase 232561817
ReferenceBase 238582211
Identity 0.856885 0.045182 0.036814 0.061119
Probs 0.949913 0.050087 0.031373 0.047338 0.231253 0.301325
========================= SUMMARY============================
Total reads      :  34330
Unaligned reads  :  196
Deletion rate    : 0.0635
Insertion rate   : 0.0382
Mismatch rate    : 0.0469
Identity rate    : 0.8896
=============================================================
```

### GN_179

Basecall and analysis with

```
/mnt/npBasecallerTesting/run_and_analyse_albacore.sh GN_179 /mnt/npBasecallerTesting/GN_179/Albacore/ /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass/ /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Albacore/ /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Reference/reference.fasta
```

Timeoutput

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Metrichor_Basecall/pass/ -s /DataOnline/Data/Nanopore/Labelling/20170320_GN_179/Albacore/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 33380.14
        System time (seconds): 157.82
        Percent of CPU this job got: 390%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 2:23:09
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 2051828
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 1
        Minor (reclaiming a frame) page faults: 871216
        Voluntary context switches: 4899872
        Involuntary context switches: 1171409
        Swaps: 0
        File system inputs: 26480136
        File system outputs: 539400
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

Error analysis results

```
Deletion 7027412 4865384 0.060142533470070626
Insertion 3600227 2838351 0.030811737357558083
MisMatch 4700110 0.040224839953600794
Match 105118437
Clipped 8558945
ReadBase 113418774
ReferenceBase 116845959
Identity 0.872742 0.039022 0.029891 0.058345
Probs 0.957201 0.042799 0.025846 0.044304 0.211619 0.307656
========================= SUMMARY============================
Total reads      :  6449
Unaligned reads  :  7
Deletion rate    : 0.0601
Insertion rate   : 0.0308
Mismatch rate    : 0.0402
Identity rate    : 0.8996
=============================================================
```

### 20170301_MT_3x_Arnold

Basecall and analysis with

```
/mnt/npBasecallerTesting/run_and_analyse_albacore.sh MT_3x_Arnold /mnt/npBasecallerTesting/20170301_MT_3x_Arnold/Albacore/ /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass/ /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Albacore/ /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Reference/reference.fasta
```

Time output

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass/ -s /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Albacore/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 167606.60
        System time (seconds): 1422.57
        Percent of CPU this job got: 360%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 13:01:23
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 3297888
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 5078442
        Voluntary context switches: 22190940
        Involuntary context switches: 9699310
        Swaps: 0
        File system inputs: 262853072
        File system outputs: 3704936
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

Error analysis results

```
Deletion 35084743 24513272 0.061217465082495606
Insertion 20467373 15272827 0.03571240900803843
MisMatch 26840816 0.04683308400650646
Match 511191001
Clipped 31141650
ReadBase 558499190
ReferenceBase 573116560
Identity 0.861194 0.045218 0.034481 0.059107
Probs 0.950113 0.049887 0.028386 0.045561 0.253796 0.301312
========================= SUMMARY============================
Total reads      :  147588
Unaligned reads  :  203
Deletion rate    : 0.0612
Insertion rate   : 0.0357
Mismatch rate    : 0.0468
Identity rate    : 0.8919
=============================================================
```

### Human 10k subset

Basecall and assessment with:

```
ALB_OUT=/DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/albacore/ && \
OUT_NAME=human_10k_subset_albacore && \
ALB_FQ=${ALB_OUT}/${OUT_NAME}_merged.fastq && \
/usr/bin/time -v -o ${OUT_NAME}_time.log \
read_fast5_basecaller.py \
-r -t 4 \
-i /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads/ \
-s $ALB_OUT \
-c r94_450bps_linear.cfg \
-o 'fastq' && \
paste --delimiter=\\n --serial ${ALB_OUT}/workspace/*.fastq > $ALB_FQ && \
/mnt/npBasecallerTesting/basecaller_assessment.sh \
-r /DataOnline/Data/H.sapiens/Reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa \
-i $ALB_FQ \
-o $ALB_OUT \
-t 8
```

Time output

```
Command being timed: "read_fast5_basecaller.py -r -t 4 -i /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads/ -s /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/albacore/ -c r94_450bps_linear.cfg -o fastq"
        User time (seconds): 93791.34
        System time (seconds): 193.34
        Percent of CPU this job got: 401%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 6:29:57
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 2956116
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 22
        Minor (reclaiming a frame) page faults: 2580969
        Voluntary context switches: 14818722
        Involuntary context switches: 1554806
        Swaps: 0
        File system inputs: 28275776
        File system outputs: 669680
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

Error analysis results

## Nanonet

\#nanonet
Installed following the instructions on github

### 20170328\\\_a3\\\_watermanag_S18

Basecalled with

```
/usr/bin/time -v -o a3_watermanag_S18_nanonet_time.log nanonetcall --chemistry r9.4 --jobs 4 --fastq /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass > a3_watermanag_S18.fastq
```

Time output from this is

```
Command being timed: "nanonetcall --chemistry r9.4 --jobs 4 --fastq /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Metrichor_Basecall/reads/pass"
        User time (seconds): 481367.96
        System time (seconds): 1441937.80
        Percent of CPU this job got: 838%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 63:41:03
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 935136
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 63909344
        Voluntary context switches: 3279983
        Involuntary context switches: 863204013354
        Swaps: 0
        File system inputs: 4184656
        File system outputs: 311512
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

Error analysis was done with

```
/mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/Reference/reference.fasta /DataOnline/Data/Nanopore/Labelling/20170328_a3_watermanag_S18/nanonet/a3_watermanag_S18.fastq a3_watermanag_S18_nanonet_graphmap
```

The output from this was

```
Deletion 6402174 4357877 0.08987500007545532
Insertion 1397013 1169719 0.019611548121062014
MisMatch 3479743 0.04884932874170011
Match 61352286
Clipped 2663543
ReadBase 66229042
ReferenceBase 71234203
Identity 0.844710 0.047910 0.019234 0.088146
Probs 0.946327 0.053673 0.018042 0.067218 0.162700 0.319313
========================= SUMMARY============================
Total reads      :  13863
Unaligned reads  :  124
Deletion rate    : 0.0899
Insertion rate   : 0.0196
Mismatch rate    : 0.0488
Identity rate    : 0.8613
=============================================================
```

### S10

```
/usr/bin/time -v -o s10_nanonet_time.log nanonetcall --chemistry r9.4 --jobs 10 --fastq /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Metrichor_Basecall/pass/ > /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/nanonet/s10_nanonet.fastq && /mnt/npBasecallerTesting/basecaller_assessment.sh \
-r /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/Reference/reference.fasta \
-i /DataOnline/Data/Nanopore/Labelling/20170322_c4_watermanag_S10/nanonet/s10_nanonet.fastq \
-o s10_nanonet \
-t 10 \
-a graphmap
```

### Lambda_old

Base called and ran error analysis with:

```
/usr/bin/time -v -o lambda_old_nanonet_time.log nanonetcall --chemistry r9.4 --jobs 4 --fastq /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass > /DataOnline/Data/Nanopore/Labelling/Lambda_old/nanonet/lambda_old_nanonet.fastq && /mnt/npBasecallerTesting/analysis_graphmap.sh /DataOnline/Data/Nanopore/Labelling/Lambda_old/Reference/lambda.fasta /DataOnline/Data/Nanopore/Labelling/Lambda_old/nanonet/lambda_old_nanonet.fastq lambda_old_nanonet_graphmap
```

time output is

```
Command being timed: "nanonetcall --chemistry r9.4 --jobs 4 --fastq /DataOnline/Data/Nanopore/Labelling/Lambda_old/Metrichor_Basecall/pass"
        User time (seconds): 1123100.79
        System time (seconds): 3420952.55
        Percent of CPU this job got: 1564%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 80:42:13
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 803788
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 1
        Minor (reclaiming a frame) page faults: 744303732
        Voluntary context switches: 4583158
        Involuntary context switches: 1566654187816
        Swaps: 0
        File system inputs: 54934256
        File system outputs: 788392
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
```

error analysis results:

```
Deletion 18170483 12300059 0.09420565004027975
Insertion 3886083 3215406 0.02014756432866867
MisMatch 9141622 0.04739513214549785
Match 165568927
Clipped 595572
ReadBase 178596632
ReferenceBase 192881032
Identity 0.841446 0.046459 0.019750 0.092345
Probs 0.947676 0.052324 0.018404 0.070403 0.172584 0.323075
========================= SUMMARY============================
Total reads      :  32288
Unaligned reads  :  382
Deletion rate    : 0.0942
Insertion rate   : 0.0201
Mismatch rate    : 0.0474
Identity rate    : 0.8584
=============================================================
```

## Human 10k subset

Basecalled with:

```
/usr/bin/time -v -o human_10k_subset_nanonet_time.log nanonetcall --chemistry r9.4 --jobs 4 --fastq /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/reads > /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/nanonet/human_10k_subset_nanonet.fastq 
```

I created a script with command line arguments to do the basecaller analysis. This script is called `basecaller_assessment.sh` and can be seen [here](bear://x-callback-url/open-note?id=3C356F78-95F5-424F-8CF3-225F203C9B80-1109-00006EE6C3412BA1). Did the assessment on this sample with the following:\```
/mnt/npBasecallerTesting/basecaller_assessment.sh \\
\-r /DataOnline/Data/H.sapiens/Reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa \\
\-i /DataOnline/Data/Nanopore/NA12878-WGS/fast5/rel3/subset_for_basecaller_project/nanonet/human_10k_subset_nanonet.fastq \\
\-o human_10k_subset_nanonet \\
\-t 14 \\
\-a minimap2

```
  

Ouput was:
```

Deletion 4205763 2149448 0.10341551822294687
Insertion 770463 445989 0.018944916871589366
MisMatch 1689034 0.04153166177128308
Match 34773790
Clipped 3360464
ReadBase 37233287
ReferenceBase 40668587
Identity 0.839155 0.040759 0.018593 0.101493
Probs 0.953678 0.046322 0.012231 0.058949 0.421142 0.488928
========================= SUMMARY============================
Total reads      :  8165
Unaligned reads  :  1908
Deletion rate    : 0.1034
Insertion rate   : 0.0189
Mismatch rate    : 0.0415

# Identity rate    : 0.8551

```
  

  
  
  

### 20170301\_MT\_3x\_Arnold

  
```

/usr/bin/time -v -o mt_3x_arnold_nanonet_time.log nanonetcall --chemistry r9.4 --jobs 10 --fastq /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Metrichor_Basecall/pass/ > /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/nanonet/mt_3x_arnold_nanonet.fastq && /mnt/npBasecallerTesting/basecaller_assessment.sh \\
\-r /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/Reference/reference.fasta \\
\-i /DataOnline/Data/Nanopore/Labelling/20170301_MT_3x_Arnold/nanonet/mt_3x_arnold_nanonet.fastq \\
\-o mt_3x_arnold_nanonet \\
\-t 10 \\
\-a graphmap 

```

```