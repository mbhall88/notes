# Batch scripting IGV snapshots

```
  
```

# Batch scripting IGV snapshots

Set up the pipeline for batch scripting the snapshots in IGV. Below is a little tutorial on how to set this up.

## Install and Testing

The first thing you should do though is set up an interactive job on the cluster to test everything. The first step involves logging into the cluster with the `-X` flag `ssh -X eb-cli` (or however you log in). You may be prompted to install/update X11. If so, do this and then you will need to restart your computer.
Once logged in with the above method, check X-forwarding is working by running `xclock`, you should get a little clock pop up on your screen after a moment.
Next, setup an interactive job on a worker node with X-forwarding (we’ll grab 8GB as well to run IGV).

```
bsub -XF -R "select[mem>8000] rusage[mem=8000]" -M8000 -Is $SHELL 
```

Again, run `xclock` to check it is all working ok. Now we need to download the [IGV Snapshot Automator](https://github.com/stevekm/IGV-snapshot-automator) repository and install IGV.\```
git clone <https://github.com/stevekm/IGV-snapshot-automator.git>
cd IGV-snapshot-automator/bin
./get_IGV.sh && cd ../

```
  

Let’s run a test to make sure everything is working ok
```

python make_IGV_snapshots.py test_data/test_alignments.bam test_data/test_alignments2.bam -r regions.bed 

````
This will take a moment to run, along with all the glorious Java `stderr` that looks like a million error messages. After this has run there will be a bunch of PNG files in the `IGV_Snapshots/` directory. Have a look at one of these with `eog IGV_Snapshots/chr1_714001_714001_h500.png`. This should bring up a PNG viewer on your machine with an IGV screenshot!

Setting up your own batch
-------------------------

As we can see from above, the key to this program is the `make_IGV_snapshots.py` script. The documentation for this script is quite good and gives you a fairly good idea of how to run it```
usage: make_IGV_snapshots.py [-h] [-r regions] [-g genome] [-ht image height]
                             [-o output directory] [-bin IGV bin path]
                             [-mem IGV memory MB] [-nosnap] [-suffix SUFFIX]
                             [-nf4] [-onlysnap ONLYSNAP]
                             input_files [input_files ...]

IGV snapshot automator

positional arguments:
  input_files           pathes to the files to create snapshots from e.g.
                        .bam, .bigwig, etc.

optional arguments:
  -h, --help            show this help message and exit
  -r regions            BED file with regions to create snapshots over
  -g genome             Name of the reference genome, Defaults to hg19
  -ht image height      Height for the IGV tracks
  -o output directory   Output directory for snapshots
  -bin IGV bin path     Path to the IGV jar binary to run
  -mem IGV memory (MB)  Amount of memory to allocate to IGV, in Megabytes (MB)
  -nosnap               Don't make snapshots, only write batchscript and exit
  -suffix SUFFIX        Filename suffix to place before '.png' in the
                        snapshots
  -nf4                  'Name field 4' mode; uses the value in the fourth
                        field of the regions file as the filename for each
                        region snapshot
  -onlysnap ONLYSNAP    Path to batchscript file to run in IGV. Performs no
                        error checking or other input evaluation, only runs
                        IGV on the batchscript and exits.
````

Things to take particular note are the `-g` flag - make sure you reference the genome you want to align to (your reference). If that isn’t the human reference than make sure you specify it. And `-r` which is the path to the BED file containing details about the locations you want to snapshot.
I would also suggest making an IGV `.genome` file of your reference. This can be done fairly easily. You need the fasta reference of the genome and the GFF file of the genome. To get the GFF file, go to NCBI, click **Send to > File >** select GFF3 format. Next, follow the step-by-step instructions on [IGV's website](https://software.broadinstitute.org/software/igv/LoadGenome) for generating a `.genome` file from them.

Let’s take a quick look at the BED file format they use in the template

```
chr1	713167	714758	test
chr1	713500	714900	foo
chr1	714000	715000	bar
chr1	714001 
```

I think this is fairly self-explanatory but just incase: The first column is the chromosome you want to look at, with the second and third columns being the start and end locations respectively. There is also the option of adding a fourth column which, when used in tandem with the `-nf4` flag, will set the name of the snapshot. For this example I have a SNP output file from `mummer dnadiff` that I want to look at mismatches from. An example of how this file type looks is\```
261	T	C	261	12	261	1539	1539	1	1	GC00000002_13	GC00000002_13
273	C	G	273	12	273	1539	1539	1	1	GC00000002_13	GC00000002_13
1017	T	C	1017	523	523	1539	1539	1	1	GC00000002_13	GC00000002_13
927	C	T	927	525	577	1503	1503	1	1	GC00000002_14	GC00000002_14
1452	G	A	1452	52	52	1503	1503	1	1	GC00000002_14	GC00000002_14
670	G	A	670	110	670	1434	1434	1	1	GC00000002_2	GC00000002_2 

````
For an in-depth description of this head to [the manual](http://mummer.sourceforge.net/manual/#program) of `mummer`. For the purposes of this exercise we are interested in column 1 (location of mismatch on reference) as we are only looking at mismatches for the moment. To filter out indels and keep the first 200 mismatches we can run```
awk '$2 != "." && $3 != "." {print;}' /hps/nobackup/research/zi/mbhall/Pandora_variation/Analysis/Loman_K12/loman_k12_merged_pass.pandora.mummer.standard_ref.snps | cut -f1 | head -n200 | xargs -I £ echo -e 'NC_000913.3\t£' > loman_k12_200_mismatches.bed 
````

As you can see above I have set up the BED file so that all the first columns have the “chromosome” NC_000913.3. Following this is the location of the mismatch on the reference. Only the first and second columns are used (will also take a stop position in the third column if you’re looking at indels). Before running the script to generate the snapshots ensure you have a sorted and indexed BAM file (I use `samtools sort/index`). For the above data I have run 200 snapshots of mismatches with the following - note: You may need to play around with the `-ht` (height) parameter a little bit to capture all the reads. I would suggest running this on 1 line of your BED file first to get the correct height.\```
python make_IGV_snapshots.py -group_by_strand -ht 3250 -r /hps/nobackup/research/zi/mbhall/Pandora_variation/Analysis/Loman_K12/loman_k12_5_mismatches.bed \\
\-g /hps/nobackup/research/zi/mbhall/Pandora_variation/Data/References/Escherichia_coli/NC_000913.3/NC_000913.3.genome \\
\-o /hps/nobackup/research/zi/mbhall/Pandora_variation/Analysis/Loman_K12/IGV_Snapshots/ \\
\-mem 7000 \\
/hps/nobackup/research/zi/mbhall/Pandora_variation/Data/Loman_K12/Alignments/loman_k12_merged_pass.mm2.sorted.bam

```
  

**Note:** In the above example I used a `-group_by_strand` flag. This is a feature I added onto a fork of the original repository. At the time of writing this I have sent a pull request to the main repository to add this feature. If it isn’t on the main repo when you are reading this, my fork can be found [here](https://github.com/mbhall88/IGV-snapshot-automator).
The final snapshots can be found at `/hps/nobackup/research/zi/mbhall/Pandora_variation/Analysis/Loman_K12/IGV_Snapshots/`
```