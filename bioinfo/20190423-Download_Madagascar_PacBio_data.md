# Download Madagascar PacBio data

```
  
```

# Download Madagascar PacBio data

Data is hosted by CSHL

sftp: `delabast@dropfiles.cshl.edu`
user: `delabast`
password: `d@t@drop`

In a directory called `TB`

Downloaded the files over `sftp` on to ebi-cli

```
cd /nfs/research1/zi/projects/tech_wars/data/madagascar/pacbio
sftp delabast@dropfiles.cshl.edu
# enter password 
get TB/*
```

### Samples to download

* **Pool 1**

  * bc1001
  * bc1002
  * bc1003
  * bc1008
  * bc1009
  * bc1010
  * bc1011
  * bc1012
  * bc1015
  * bc1016
  * bc1017
  * bc1018
  * bc1019
  * bc1020
  * bc1021
  * bc1022
* **Pool 2**

  * bc1001
  * bc1002
  * bc1003
  * bc1008
  * bc1009
  * bc1010
  * bc1011
  * bc1012
  * bc1015
  * bc1016
* **Pool 3**

  * bc1015
  * bc1016
  * bc1017
  * bc1018
  * bc1019
  * bc1020
  * bc1021
  * bc1022
  * bc1001