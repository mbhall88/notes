# Tubemap

```
  
```

# Tubemap

Downloaded the 2016 FASTA for *pol* from #HIV from [here](https://www.hiv.lanl.gov/content/sequence/NEWALIGN/align.html#web).

Made a subset of the file with just 5 samples with:

```
awk '/^>/ {c++} {print $0} c==6 {exit}' HIV1_ALL_2016_pol_DNA.fasta | ghead -n -1 > HIV1_ALL_2016_pol_DNA_5_samples.fasta
```

Create the PRG and associated files using Rachelle’s script

```
python make_gene_prg.py HIV1_ALL_2016_pol_DNA_5_samples.fasta --max_nesting 10 --min_match_length 5 -p five
```

The two files of interest from this are the `.gfa` and `.prg` ones. The `.gfa` is contains the information on the nodes and edges between them. Whereas the `.prg` is the linear representation of the graph.

From the [tubemap repository](https://github.com/vgteam/sequenceTubeMap) I copied the HTML, CSS, and JS file.

The tube map code is all written in ES6 so in order to get it to transpire I used \[`webpack`]([webpack](https://webpack.js.org)).

```
npm init -y
npm install --save-dev webpack jquery
```

Then I created a file called `index.js` in the `/src` folder and this runs the code to generate the #tubemap. To bundle all of these up together I ran:

```
./node_modules/.bin/webpack src/*.js dist/bundle.js
```

And in the body of the HTML file I just need to add a script tag which sources this bundle.js file.