# Clone and build pandora on cluster

```
  
```

# Clone and build pandora on cluster

```sh
# on yoda
software_dir=/nfs/leia/research/iqbal/mbhall/Software
grp_software=/nfs/leia/research/iqbal/software
# on ebi-cli
software_dir=/nfs/research1/zi/mbhall/Software
grp_software=/nfs/research1/zi/software

cd "$software_dir"
git clone https://github.com/mbhall88/pandora.git
cd pandora
mkcd build

export BOOST_ROOT=/nfs/research1/zi/software/boost_1_62_0
export BOOST_LIBRARYDIR=/nfs/research1/zi/software/boost_1_62_0

cmake -DCMAKE_BUILD_TYPE=Release ..
cmake -DCMAKE_BUILD_TYPE=Debug ..

# cmake -DCMAKE_PREFIX_PATH="$grp_software" ..
# or if that doesn't find the correct boost
# cmake -DBoost_NO_BOOST_CMAKE=TRUE -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT:PATHNAME=/nfs/leia/research/iqbal/software -DBoost_LIBRARY_DIRS:FILEPATH=/nfs/leia/research/iqbal/software/lib ..
make -j 4
ctest -VV
cd ..log=/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Pandora/local_assembly_test/pandora_tb_180918_local_assembly_test
script=/homes/mbhall88/Scripts/run_pandora_tb.sh
fastq=/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Pandora/tb_sputum_filtered.fastq
output=/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Pandora/local_assembly_test/
#container=/nfs/leia/research/iqbal/mbhall/Software/Singularity_images/pandora.simg
pandora=/nfs/leia/research/iqbal/mbhall/Software/pandora/build/pandora
bsub.py 60 "$log" bash "$script" \
  "$fastq" \
  "$output" \
  "$pandora"
```

Rebuild on cluster

```
# yoda
cd /nfs/leia/research/iqbal/mbhall/Software/pandora/build
grp_software=/nfs/leia/research/iqbal/software
cmake -DCMAKE_PREFIX_PATH="$grp_software" ..
make -j 4
ctest -VV

# ebi-cli
mkdir -p /nfs/research1/zi/mbhall/Software/pandora/build
cd /nfs/research1/zi/mbhall/Software/pandora/build
grp_software=/nfs/research1/zi/software
cmake -DCMAKE_PREFIX_PATH="$grp_software" ..
make
ctest -VV
```

Generate test coverage report

```
cd ~/Projects/pandora/cmake-build-debug/test/CMakeFiles/pandora_test.dir
lcov --directory . --zerocounters
# run pandora_test
../../pandora_test

lcov --directory . --capture \
  --exclude '/usr/local/include/boost/*' \
  --exclude '*_test.cpp' \
  --exclude '*.h' \
  --exclude '/usr/include/*' \
  --exclude  '*cmake-build-debug*' \
  --exclude  '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1/*' \
  --output-file pandora_test.info
genhtml --show-details --legend pandora_test.info
open index.html 
```
