# Pandora de novo simulations 2.0

## Simulations 2.0
There are three shortcomings in my current simulations to assess _de novo_:
1. I only look at SNPs.
2. I use simulated reads and I am beginning to become a little untrusting of them.
3. I do not compare my _de novo_ results to results without _de novo_.

I have an idea to solve all three of these issues and that will also (hopefully) help us with debugging the recall problem with the compare results for the paper.

One thing that makes debugging recall for the compare results a little messy is that you are comparing across multiple samples and this is hard. My solution I am proposing will just use two rounds of `pandora map` for a single sample, which I hope will show up the strange loss of recall when using _de novo_. If this loss doesn't show up then I guess we know that there must be something within compare that is causing the strange recall problem.

My idea is as follows.

1. Select one of the four samples we use for the paper analysis.
2. Take the assembly and chop it up into chunks (mimicking genes).
3. For each chunk, mutate the assembly sequence **once** using [`snp-mutator`](https://snp-mutator.readthedocs.io/en/latest/usage.html). (Here we can control the number of SNPs, deletions, insertions, and the number of sequences to produce). We now have a mutated reference sequence for each chunk.
4. Run `snp-mutator` again, but this time on the mutated reference sequence and produce _n_ mutated sequences (10, 50, 100, 500?). The reason to created mutants from anoter mutant is we don't want the true reference path to exist in our final PRG. If we just generated multiple mutants from the reference, we would still be able to find a path through the final PRG that equates to the original truth sequence. This would then effectively make the comparison of _de novo_ with no-_de novo_ useless.
5. For each chunk, do a multiple sequence alignment (clustal omega) of the mutated reference sequence, along with all the mutants generated in Step 4.
6. For each MSA, build a PRG.
7. Combine all PRGs into a single PRG.
8. Subsample the reads to the desired coverages (30, 60, 100x?) using the `filtlong` method I have setup. (There is some evidence that random subsampling could be better so we could also try that and see which method produces the best results).
9. Run `pandora map` **with `--discover`** enabled.
10. Add the _de novo_ paths to the ends of the MSAs.
11. Rerun the MSAs for those chunks with _de novo_ paths.
12. Rebuild the PRGs for the new MSAs from Step 11.
13. Run `pandora map` again, but this time **without `--discover`** enabled.

Now the part I am not fully clear on is how to best evaluate this. I see two options:

1. Separately, for the resulting pandora genotype VCFs from Steps 9 and 13, apply the genotype VCF variants to the consensus sequences to produce a "genotyped sequences" for each. Then run `dnadiff` for each of these genotyped sequences to the original assembly. An advantage of this method is we get a base-by-base comparison to the original assembly allowing us to easily define TN, FN, FP, and TP.
2. Evaluate using the current method of making probes out of the VCFs and mapping one set of probes to the others. 