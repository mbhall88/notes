# Yifei data information for mykrobe paper

```

```

# Yifei data information for mykrobe paper

## DST results

The DST reports for each of the samples can be found in `/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Data/TB_low_input_yifei/dst_reports`

## Raw fast5 data

Can be found at `/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Data/TB_low_input_yifei/TB_nanopore/rawFAST5/sf_20180103_1645_tb_low_input/20180103_1645_tb_low_input/fast5`

## Basecalled fastq files

Data was basecalled with ONT Albacore Sequencing Pipeline Software (version 2.1.7)
The exact code used to run the basecalling is:

```sh
input=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/rawFAST5/sf_20180103_1645_tb_low_input/20180103_1645_tb_low_input/fast5/
output=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/
threads=32
read_fast5_basecaller.py \
    --worker_threads "$threads" \
    --flowcell FLO-MIN106 \
    --kit SQK-LSK108 \
    --save_path "$output" \
    --recursive \
    --input "$input" \
    --barcoding \
    --output_format fastq
```

As this Albacore run split the fastqs according to barcode, a combined fastq with all (passed) reads can be found at `/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Data/TB_low_input_yifei/TB_nanopore/workspace/pass/all_passed.fastq.gz`

## Porechop

Porechop (v0.2.3) adapter trimming and demultiplexing was run with the following:

```sh
INPUT=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/workspace/pass
OUTPUT=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/porechop_binning
THREADS=32

porechop --input "$INPUT" \
    --barcode_dir "$OUTPUT" \
    --discard_unassigned \
    --threads "$THREADS" \
    --format fastq.gz \
    --check_reads 50000 \
    --extra_end_trim 10 \
    --discard_middle
```

Running porechop in this way means that it will only assign a read to a barcode if both porechop and albacore agree on its assignment. Otherwise it is binned as "unclassified". For full details of the number of reads discarded and adapters trimmed, see `/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Data/TB_low_input_yifei/TB_nanopore/porechop_binning.o` All of the porechop fastq files can be found in `/nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Data/TB_low_input_yifei/TB_nanopore/porechop_binning`

* * *

Now I am not sure which fastq files you ended up using in the end? If the filenames ended in `filtered` then you might be using ones that have had non-TB mapping reads removed. If that is the case, in `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/TB/Yifei_data/Nanopore/yifei_mykrobe` you will find a snakemake `Snakefile` that documents going from basecalled fastq files to filtered fastq files (runs porechop, mapping, filtering etc,)
