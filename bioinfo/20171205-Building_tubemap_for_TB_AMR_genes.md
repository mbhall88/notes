# Building tubemap for TB AMR genes

```
  
```

# Building tubemap for TB AMR genes

\#tubemap #phd #tb

## Aim

Build a tube map representation of the population reference graphs (PRGs) for selected antimicrobial resistance (AMR) genes within tuberculosis (TB).

## Data

All data is located in the `tubemap_pilot` [GitHub project](https://github.com/mbhall88/tubemaps_pilot).

The reference genome being used is NC_000962.3, which I downloaded from [Phelim’s Mykrobe Predictor repository](https://github.com/iqbal-lab/Mykrobe-predictor/blob/master/mykrobe/data/NC_000962.3.fasta).
I also downloaded the downloaded the GFF file for this reference from [NCBI](https://www.ncbi.nlm.nih.gov/nuccore/NC_000962.3). This file contains all the information about gene coordinates etc. This file is call `NC_000962.3.gff3`.
For the variants, I used [this probe set](https://github.com/iqbal-lab/Mykrobe-predictor/blob/master/mykrobe/data/panels/tb-walker-probe-set-feb-09-2017.fasta.gz), also from Phelim’s Mykrobe predictor repository, as recommended by Phelim in the Slack conversation about this. Within the header of each variant’s entry in the fasta file is contained the information about the variant.
For example:

```
>ref-T444F?var_name=ACC761136TTT&num_alts=37&ref=NC_000962.3&enum=0&gene=rpoB&mut=T444F 
```

This shows that the nucleotide variant (`var_name`) is `ACC761136TTT` with the coordinates being for the genome in the `ref=` field. The amino acid change (if any) is noted in the `mut=` field. Not every header has a reference noted, but Phelim has confirmed these are from NC_000962.3 as well. Created a folder for running analysis on genes within the `data/` directory using the line:\```
ls reference/genes/ | sed 's/.fa//g' | xargs  mkdir 

```
  

  

Analysis
--------

The first step was to generate a subset of the GFF file which contained only the coordinates information for the genes contained in the probe set from Mykrobe.
To do this I [created a script](https://github.com/mbhall88/tubemaps_pilot/blob/master/scripts/preprocess_gff.py) to do the subsetting.
  

### Create reference fasta for each gene

  
Next step was to create a fasta file for each of the relevant genes in the subsetted GFF file - these will be the templates for the MSA later. Again, I created a [python script](https://github.com/mbhall88/tubemaps_pilot/blob/master/scripts/generate_gene_fasta.py)to do this and wrote the fasta files into `data/tb/reference/genes/` within the GitHub repository.
  

### Generate MSA for all variants within Mykrobe probes

  
Wrote a [python script](https://github.com/mbhall88/tubemaps_pilot/blob/master/scripts/generate_msa_from_probes.py) to generate a multiple sequence alignment for each gene in the probe set. Specifically, each entry in the fasta is a version of the reference for the gene, but altered by one of the variants in the probe set. Therefore, for every variant in the probe set, there is a sequence in the resulting fasta file (MSA). To run this script from the repository home directory I used:
```

python3 scripts/generate_msa_from_probes.py -p data/tb/reference/tb-walker-probe-set-feb-09-2017.fasta -g data/tb/reference/genes -o data/tb 

````
This then generates a MSA in a separate folder for each gene. An example of how the directory tree looks after this is```
$ cd data/tb && tree
.
├── ahpC
│   └── ahpC_msa.fa
...
├── pncA
│   └── pncA_msa.fa
├── reference
│   ├── NC_000962.3.fasta
│   ├── NC_000962.3.gff3
│   ├── NC_000962.3_mykrobe_probes.gff3
│   ├── genes
│   │   ├── ahpC.fa
...
│   │   └── tlyA.fa
│   └── tb-walker-probe-set-feb-09-2017.fasta
├── rpoB
│   └── rpoB_msa.fa
...
└── tlyA
    └── tlyA_msa.fa
````

### Create PRG for each gene

Next, I wrote a script [`generate_prg_and_json.sh`](https://github.com/mbhall88/tubemaps_pilot/blob/master/scripts/generate_prg_and_json.sh). This script takes from `stdin` a list of directories and within each directory (except ‘reference’) will generate a PRG for the fasta file contained within the directory and then run the path finding script on the resulting GFA to generate the JSON file required to make the tube maps. An example of how this analysis was run is below.

```
ls ~/Dropbox/PhD/prg_tubemaps/data/tb | grep -v 'reference' | xargs -I _ realpath _ | ~/Dropbox/PhD/prg_tubemaps/scripts/generate_prg_and_json.sh
```

* * *

[GitHub commit](https://github.com/mbhall88/tubemaps_pilot/commit/f588fc34b222d95a829236607016fd506b81a3dc) at this stage.

* * *

TODO - The next thing to do will be to have a chat with Robyn about how we want the tubemaps to look. Also it would be good to add in something like a dropdown box to select the gene you want to look at the PRG for.

Checklist of genes

* ahpC
* eis
* embA
* embB
* fabG1
* gid - 331 variants
* gyrA - 586 variants
* inhA
* katG - 202 variants
* ndh
* pncA - 351 variants
* rpoB - 1557 variants
* rpsA
* rpsL
* rrs
* tlyA

### Update web packing

I created a little script in the repository home directory called `webpack_update` which will run webpacking. This is needed if there is an update made to any of the javascript files in the repository.