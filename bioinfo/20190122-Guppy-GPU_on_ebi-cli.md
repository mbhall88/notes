# Guppy-GPU on ebi-cli

```
  
```

# Guppy-GPU on ebi-cli

\#guppy #gpu
`export PATH=$PATH:/usr/local/cuda/bin`
`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64/`

Interactive session on GPU node on `ebi-cli`

```
mem=4000
bsub -R "select[mem>$mem] rusage[mem=$mem]" -M"$mem" -q research-rh74 -P gpu -gpu - -Is $SHELL
```

Guppy container location: `/nfs/research1/zi/mbhall/Software/Singularity_images/guppy.simg`

Get some toy data together

```
fast5_dir=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/rawFAST5/sf_20180103_1645_tb_low_input/20180103_1645_tb_low_input/fast5/333
mkdir -p /hps/nobackup/research/zi/mbhall/Guppy_testing/reads
cd !$
find "$fast5_dir" -name '*.fast5' | xargs -I @ cp @ .
```

Run guppy in GPU mode on the reads

```
cd /hps/nobackup/research/zi/mbhall/Guppy_testing
save=gpu_calling
mkdir -p "$save"
container=containers/guppy_test.simg
kit=SQK-LSK108
flowcell=FLO-MIN106
input=reads/

/usr/bin/time singularity -v exec -B /usr/local/cuda/bin -B /usr/local/cuda/lib64/ --nv "$container" guppy_basecaller \
  --input_path "$input" \
  --recursive \
  --save_path "$save" \
  --flowcell "$flowcell" \
  --kit "$kit" \
  --device auto 

# time output
5.73user 2.65system 0:09.66elapsed 86%CPU (0avgtext+0avgdata 403932maxresident)k
62866inputs+27656outputs (52major+103004minor)pagefaults 0swaps
```

Run Guppy on CPU to see time difference

```
cd /hps/nobackup/research/zi/mbhall/Guppy_testing
container=/nfs/research1/zi/mbhall/Software/Singularity_images/guppycpu.simg
save=cpu_calling
mkdir -p"$save"
kit=SQK-LSK108
flowcell=FLO-MIN106
input=reads/

/usr/bin/time singularity -v exec "$container" guppy_basecaller \
  --input_path "$input" \
  --recursive \
  --save_path "$save" \
  --flowcell "$flowcell" \
  --kit "$kit" | tee time.cpu.log

# time output
589.93user 8.03system 2:35.40elapsed 384%CPU (0avgtext+0avgdata 959264maxresident)k
31422inputs+28968outputs (26major+2782890minor)pagefaults 0swaps
```

The error that I keep getting is:

```
Loading fatbin file shared.fatbin failed with: CUDA error at /builds/ofan/ont_core_cpp/ont_core/common/CUDAHelper.cpp:40: CUDA_ERROR_NO_BINARY_FOR_GPU
```

Docker nvidia container with same drivers as cluster - \[Driver containers (Beta) · NVIDIA/nvidia-docker Wiki · GitHub]([https://github.com/NVIDIA/nvidia-docker/wiki/Driver-containers-(Beta))](https://github.com/NVIDIA/nvidia-docker/wiki/Driver-containers-(Beta)))
Download `.deb` for nvidia drivers on cluster - [NVIDIA DRIVERS Tesla Driver for Ubuntu 16.04](https://www.nvidia.com/download/driverResults.aspx/135839/en-us)

# Deepbinner

### Classify

```
cd /hps/nobackup/research/zi/mbhall/Guppy_testing
container=deepbinnergpu.simg
fast5_dir="/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/rawFAST5/sf_20180103_1645_tb_low_input/20180103_1645_tb_low_input/fast5"
output=/hps/nobackup/research/zi/mbhall/Guppy_testing/yifei_classification
script=/hps/nobackup/research/zi/mbhall/Guppy_testing/deepbinner_classify.sh
job=deepbinner_classify
log=/hps/nobackup/research/zi/mbhall/Guppy_testing/"$job"
bsub -o "$log".o -e "$log".e -J "$job" -q research-rh74 -R "select[mem>3000] rusage[mem=3000]" -M3000 -P gpu -gpu - bash "$script" "$fast5_dir" "$output" 
```

`/hps/nobackup/research/zi/mbhall/Guppy_testing/deepbinner_classify.sh`

```
#!/usr/bin/env bash
set -eux
fast5_dir="$1"
output="$2"
container=${3:-/nfs/research1/zi/mbhall/Software/Singularity_images/deepbinnergpu.simg}

singularity exec --nv "$container" deepbinner classify --native "$fast5_dir" > "$output"
```

### Bin

Deepbinner’s `bin` command does not use GPUs.

```
cd /hps/nobackup/research/zi/mbhall/Guppy_testing
container=deepbinnergpu.simg
classifications=yifei_classification
out_dir=.
reads_dir=/nfs/research1/zi/mbhall/Data/TB_low_input_yifei/TB_nanopore/workspace/
script=/hps/nobackup/research/zi/mbhall/Guppy_testing/deepbinner_bin.sh
job=deepbinner_bin
log=/hps/nobackup/research/zi/mbhall/Guppy_testing/"$job"
bsub.py 1 "$log" bash "$script" "$classifications" "$out_dir" "$reads_dir"
```

`/hps/nobackup/research/zi/mbhall/Guppy_testing/deepbinner_bin.sh`

```
#!/usr/bin/env bash
set -eux

container=/nfs/research1/zi/mbhall/Software/Singularity_images/deepbinnergpu.simg
classifications="$1"
out_dir="$2"
reads_dir="$3"

cat $(find $reads_dir -name '*.fastq') > tmp_reads.fastq

singularity exec "$container" deepbinner bin \
	--classes "$classifications" \
	--reads tmp_reads.fastq \
	--out_dir "$out_dir"
rm tmp_reads.fastq 
```