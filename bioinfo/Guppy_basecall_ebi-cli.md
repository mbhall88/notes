# Guppy basecall ebi-cli

```
  
```

# Guppy basecall ebi-cli

Example script for basecalling with guppy on GPU cluster

```
#!/usr/bin/env sh
cuda_bins="/usr/local/cuda/bin"
cuda_libs="/usr/local/cuda/lib64/"
# version 3.1.5
container="shub://mbhall88/Singularity_recipes:guppygpu@7ec66a8bfc44765d7eda4c615cebebf9"
kit="SQK-RAD004"
flowcell="FLO-MIN106"
input="reads/"
save="guppy/"
mem=4000
job=basecall_guppy

bsub -R "select[mem>$mem] rusage[mem=$mem]" \
    -M "$mem" \
    -P gpu \
    -gpu - \
    -o "$job".o \
    -e "$job".e \
    -J "$job" \
    singularity exec \
        --bind "$cuda_bins" --bind "$cuda_libs"  \
        --nv \
        "$container" \
        guppy_basecaller \
            --input_path "$input" \
            --recursive \
            --save_path "$save" \
            --flowcell "$flowcell" \
            --kit "$kit" \
            --compress_fastq \
            --device auto \
            --num_callers 8 
```