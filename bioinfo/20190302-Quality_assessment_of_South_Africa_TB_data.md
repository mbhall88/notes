# Quality assessment of South Africa TB data

```
  
```

# Quality assessment of South Africa TB data

Files relating to the following assessment can all be found on the FTP site under `analysis/south_africa/nanopore/`.

## Run ID mappings

* `UCTSU_04_02_19_BC16` and `UCT_SU_04FEB19_BC1-6_v02` have been combined into `UCTSU_04_02_19_BC16`
* `UCT_SU_06FEB19_1-24` has been split into `UCT_SU_06FEB19_1-24_Samples1-12` and `UCT_SU_06FEB19_1-24_Samples1-24` which relates directly to the subfolders within the original `UCT_SU_06FEB19_1-24` directory.
* The other run IDs remain the same as when they were uploaded.

* * *

# Quality Assessment

## UCT_SU_13FEB19_BC1-7

Path to files: `analysis/south_africa/nanopore/UCT_SU_13FEB19_BC1-7`

### Demultiplexing

```
Barcode     Count
      1      1711
      2      1061
      3      7303
      4     24566
      5     57220
      6     30410
      7      4477
      8       214
      9       189
     10       244
     11       298
     12       206
   none    604802
```

**It seems like the barcoding did not work for this run.** I ran the demultiplexing with two different tools that use a very different approach to demultiplexing and got the same results. This would mean that almost certainly there was a problem with the barcoding step of the sample prep. Samples 5 and 6 have an alright number of reads.

### Read length and quality

From looking at the stats and QC plots for each barcode, Read length distribution is great. Quality is also really good (except for barcodes 3 and 5). The 5 longest reads are:

```
68828
66201
54014
51384
51183
```

### Contamination

Due to the small number of reads assigned to barcodes 1 and 2 I won’t comment on them.
Barcode 3 (although it only has 7000 read assigned to it) shows **11% of its reads mapping to NTMs** (see the krona plot).
**Barcode 5 definitely has some contamination in it.** 69% reads map to TB, but 28% are unmapped and the GC distribution is strikingly bimodal (see below).

![](Quality%20assessment%20of%20South%20Africa%20TB%20data/Screen%20Shot%202019-03-02%20at%2012.29.11%20pm.png)
Barcodes 4 and 7 seem to be free of any significant contamination.

* * *

## UCT_SU_1302_1-9_ZOE

Path to files: `analysis/south_africa/nanopore/UCT_SU_1302_1-9_ZOE`

### Demultiplexing

```
Barcode     Count
      1       322
      2      2561
      3       330
      4      1663
      5      1309
      6      7511
      7      3483
      8       252
      9     10046
     10       218
     11       210
     12       161
   none    495081
```

**It seems like the barcoding did not work for this run.** I ran the demultiplexing with two different tools that use a very different approach to demultiplexing and got the same results. This would mean that almost certainly there was a problem with the barcoding step of the sample prep.

I won’t do as thorough a debrief of this sample in light of the barcoding problem. But aside from the barcoding problem the quality and length of the reads was very good.

* * *

## UCTSU_04_02_19_BC16

Path to files: `analysis/south_africa/nanopore/UCTSU_04_02_19_BC16`

### Demultiplexing

```
Barcode     Count
      1    225810
      2    423037
      3    226614
      4    263786
      5    272157
      6    148244
      7        74
      8        84
      9       105
     10        84
     11        29
     12        31
   none     74584
```

It seems like the barcoding was good for this sample.

### Read length and quality

From looking at the stats and QC plots for each barcode, Read length distribution is fantastic. Quality is also really good (except for barcode 2). The 5 longest reads are:

```
57177
56816
55288
55078
55039
```

### Contamination

Aside from barcode 2 the krona plots show that there does not seem to be any contamination in any of the samples.
**95% of the reads for barcode 2 do not map to anything within our contamination database (which includes NTMs, TB, and human).**

* * *

## UCT_SU_06FEB19_1-24_Samples1-12

Path to files: `analysis/south_africa/nanopore/UCT_SU_06FEB19_1-24_Samples1-12`

### Demultiplexing

```
Barcode     Count
      1     26580
      2     88124
      3     24221
      4     13168
      5    130213
      6     10675
      7     23316
      8     27854
      9     14820
     10    577018
     11     54493
     12     52145
   none     67437
```

It seems like the barcoding was reasonably efficient. Some of the barcodes though don’t have a very impressive number of reads (e.g 1, 3, 4, 6, 7, 8, 9).

### Read length and quality

From looking at the stats and QC plots for each barcode, Read length distribution is fantastic! Quality is also really good. The 5 longest reads are:

```
105408
101271
82660
80852
77274
```

### Contamination

Looking at the krona plots shows that there does not seem to be any contamination in any of the samples.

* * *

## UCT_SU_06FEB19_1-24_Samples1-24

Path to files: `analysis/south_africa/nanopore/UCT_SU_06FEB19_1-24_Samples1-24`

### Demultiplexing

```
Barcode     Count
      1     80845
      2     12765
      3      1456
      4     66896
      5     18231
      6    107200
      7     58172
      8     80374
      9     71964
     10     79693
     11     40097
     12     60334
   none    295725
```

It seems like the barcoding wasn’t as efficient as could be. Some of the barcodes have a decent number of reads, but there is nearly a third of the reads in the unclassified bin. However, aside from barcodes 2, 3, and 5 there is pretty good numbers in each bin.

### Read length and quality

From looking at the stats and QC plots for each barcode, Read length distribution is fantastic! Quality is also really good. The 5 longest reads are:

```
87481
83531
83146
80707
78667
```

### Contamination

Looking at the krona plots shows that there does not seem to be any contamination in any of the samples.