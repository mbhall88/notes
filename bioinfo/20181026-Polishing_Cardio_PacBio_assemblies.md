# Polishing Cardio PacBio assemblies

```
  
```

# Polishing Cardio PacBio assemblies

This relates to <https://github.com/iqbal-lab/pandora1_paper/issues/32>

Important README containing paths for all relevant files for each sample: `/nfs/leia/research/iqbal/rmcolq/data/cardio/README`

# Setup

Setup working directory for polishing

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio
mkdir -p Polishing/data && cd Polishing
mkdir logs
```

Create a directory for each sample with a PacBio assembly

```
from pathlib import Path
DIR = Path('/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data')
readme = Path('/nfs/leia/research/iqbal/rmcolq/data/cardio/README')
with readme.open() as fh:
    for i, line in enumerate(fh):
        if i == 0:
            continue
        if not line.split()[-1] == 'n':
            sample = line.split()[0]
            p = DIR / sample
            p.mkdir()
```

The Illumina data is contained within a BAM file and unfortunately there is a bunch of duplicated reads. In order to be able to extract the Illumina read pairs from the BAM files I [wrote a script with additional test suite](https://github.com/mbhall88/bam2fastq).

Applying this script now to the BAM files.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data
readme=/nfs/leia/research/iqbal/rmcolq/data/cardio/README
script=/homes/mbhall88/Scripts/bam2fastq/bam2fastq.py
for d in */ 
do 
sample="${d/\/}"
bam=$(grep  "$sample"  "$readme"  |  cut -f7)
bsub.py 3 ../logs/"$sample"_bam2fastq python3 "$script" --input "$bam" --output "$d" --prefix "$sample"
done
```

* * *

# Polish

First I will create symlinks to the PacBio assemblies for each sample to make it easier to put together a Snakemake pipeline.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data
readme=/nfs/leia/research/iqbal/rmcolq/data/cardio/README
for d in */ 
do 
sample="${d/\/}"
ref=$(grep  "$sample"  "$readme"  |  cut -f10)
ln -sf "$ref" "$d"/"$sample"_pacbio_assembly.fa.gz
done
```

I created a [Singularity container with all the tools required](https://github.com/mbhall88/Singularity_recipes/tree/master/assembly) and downloaded the built container from Singularity Hub.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/
mkdir containers
cd containers
singularity pull --force --name polish.simg shub://mbhall88/Singularity_recipes:polish
```

The `snakemake` pipeline runs all the necessary steps to do polishing.

The first thing Pilon needs is a BAM file containing the Illumina reads mapped to the initial PacBio-only assembly. To do this mapping I will use [`ngm`](https://github.com/Cibiv/NextGenMap). This BAM file is then indexed and sorted using `samtools`. Finally `pilon` is run.

The full `snakemake` file is at `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/Snakefile`

To submit and run the job:

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing
pipenv shell
JOB_NAME=snakemake_master_process_pilon_round6
bash scripts/submit_lsf.sh "$JOB_NAME"
```

* * *

After doing the above I realised that `ngm` mapping was producing some strange results. In particular it was only estimating the coverage of these samples to be ~10x when they should be closer to 100x. In addition, only ~13% of reads were considered "proper pairs". To check whether this was an `ngm` issue, I used `bwa mem` to map one of the samples to it’s assembly. This showed closer to 93% of reads being "proper pairs". When I ran `pilon` on this alignment it found this alignment covered >99% of the reference, as opposed to just 19% when using `ngm`.

With this in mind I moved all of the output files from the `ngm` pipeline into `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/ngm_data` and altered the `Snakefile` to use `bwa mem` instead (I kept an old copy of the `ngm Snakefile` at `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/ngm_data/Snakefile_ngm`).

I ran 6 rounds of `pilon` - there was still some changes being reported at the end but it was just changing a couple of regions back and forth between two different states.

Each time I altered the `Snakefile` to update the directories it wrote the outputs to to correspond to the round of polishing, i.e round1, round2 etc.

* * *

We will use the fasta files produced by `pilon` after the 6th round of polishing for each sample

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing
for d in $(find data/*/round6/ -name '*pilon.fasta')
do
sample=$(basename  $(dirname $(dirname $d)))
outpath=data/"$sample"/"$sample"_pacbio_assembly.pilon.fa.gz
gzip -c "$d" > "$outpath"
done
```

* * *

## Masking

First thing I did was plot, for each position within the pileup of each BAM, at what proportion did the major allele occur.

The rest of this process was done in a jupyter notebook and can be found [here](https://github.com/iqbal-lab/pandora1_paper/issues/32).