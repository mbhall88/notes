# Puntseq methods

```
  
```

# Puntseq methods

The pipeline to run the analysis was developed using the [snakemake](http://bioinformatics.oxfordjournals.org/content/28/19/2520) workflow management system and can be found at <https://github.com/d-j-k/puntseq> (note: we need to make this public and possibly clean up before submitting preprint). A visualisation of the pipeline can be seen in Figure X

![](Puntseq%20methods/puntseq_dag.png)

The "Loman" sample was downloaded from [Release 2](https://github.com/LomanLab/mockcommunity#release-2-2018-10-17) of the [Mock Community project](https://academic.oup.com/gigascience/article/8/5/giz043/5486468) and was the Zymo-GridION-EVEN-BB-SN dataset.
The first step in analysing each sample, which is not part of the pipeline, was the basecall the nanopore data. This was done using ONT’s proprietary basecaller Albacore (version X.X.X).
After basecalling the reads were demultiplexed and had adapters trimmed using [porechop](https://github.com/rrwick/Porechop) (version 0.2.4). The only non-default parameter changed was to increase the subset of reads to search for adapter sets in (`--check_reads`) and this was set to 50,000.
From the trimmed reads we filtered all reads shorter than 1Kbp using [filtlong](https://github.com/rrwick/Filtlong) (version 0.2.0).
We gathered statistics on reads, such as quality scores and read lengths, using [NanoStat](https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/bty149/4934939) (version )and performed quality control plotting using [pistis](https://github.com/mbhall88/pistis).
Two [centrifuge](https://genome.cshlp.org/content/26/12/1721) (commit 88ecab4) databases were used. The first was a database built using all bacterial and archaean genomes in the RefSeq database on 09/04/2019 (referred to from here as the "full" database). The second database was built using the 16S rRNA database [SILVA](http://nar.oxfordjournals.org/content/41/D1/D590). Samples were classified twice, once by each database, by centrifuge using default parameters. The centrifuge reports were then converted to "kraken-style" reports using the `centrifuge-kreport` tool, which comes with centrifuge, to make the reports compatible with downstream visualisation tools.
The other classifier we used was [kraken2](http://genomebiology.com/2014/15/3/R46) (version 2.0.7-beta). We built three databases for kraken. The first database was built using all bacterial and archaean genomes in the RefSeq database on 09/04/2019 (referred to from here as the "full" database) and using the default k-mer size of 35. The second database was built using the 16S rRNA database [SILVA](http://nar.oxfordjournals.org/content/41/D1/D590), also using the default k-mer size of 35. For the third database we built the same 16S database, but with a k-mer size of 21 as a way of determining whether a smaller k-mer size would produce better results for nanopore data (we did not initially do this for the "full" database due to the time and memory requirements in doing this). Samples were classified once with each database using default parameters.
We also ran [Bracken](https://peerj.com/articles/cs-104) (version 2.2) to compute the species abundance in each of the kraken classifications. A Bracken database was built from each of the kraken databases. The k-mer length parameter was set to the same as the database is was being built from. The read length parameter was set to 1450 as this is the approximate length of the 16S rRNA sequence we sequenced. (Bracken was designed with Illumina in mind and so setting a read length for nanopore is not simple. From correspondence with the Bracken authors we decided the median read length was the best solution.)