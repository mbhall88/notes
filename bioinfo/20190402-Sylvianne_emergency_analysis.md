# Sylvianne emergency analysis

```
  
```

# Sylvianne emergency analysis

```
cd /hps/nobackup/research/zi/mbhall
git clone https://github.com/mbhall88/head_to_head_pipeline.git sylvianne_emergency_analysis
cd sylvianne_emergency_analysis
```

### Running porechop with a script I made

```
#!/usr/bin/env
image_uri="docker://quay.io/biocontainers/porechop:0.2.3_seqan2.1.1--py36h2d50403_3"
fastq="$1"
out="$2"
threads="$3"
check_reads="$4"
output_fmt="$5"

singularity exec "$image_uri" porechop --input "$fastq" \
    --barcode_dir "$out" \
    --threads "$threads" \
    --check_reads "$check_reads" \
    --discard_middle \
    --discard_unassigned \
    --format "$output_fmt" cd /hps/nobackup/research/zi/mbhall/sylvianne_emergency_analysis/
script=analysis/scripts/run_porechop.sh
fastq="analysis/sylvianne/nanopore/sylvianne/basecalled.fastq"
out="analysis/sylvianne/nanopore/sylvianne/demultiplex/"
threads=8
check_reads=50000
output_fmt="fastq.gz"
memory=25
log=analysis/logs/demultiplex_and_trim
bsub.py --threads "$threads" "$memory" "$log" bash "$script" "$fastq" "$out" "$threads" "$check_reads" "$output_fmt"
touch analysis/sylvianne/nanopore/sylvianne/demultiplex/FIX_NAMES_COMPLETE
```

### Rename output

```
cd /hps/nobackup/research/zi/mbhall/sylvianne_emergency_analysis/analysis/sylvianne/nanopore/sylvianne/demultiplex/
rm BC08.fastq.gz
mv *.fastq.gz ../trimmed
cd ../trimmed
mv BC09.fastq.gz BC09.trimmed.fastq.gz
mv BC10.fastq.gz BC10.trimmed.fastq.gz
```

### Getting decontamination database

```
cd /hps/nobackup/research/zi/mbhall/sylvianne_emergency_analysis/data
rsync -av --progress ../../../projects/tech_wars/data/decontamination_db .
```

### Change report rule input

This input relied on log files from demultiplex and trim rules which have been commented out. Changed it to take the log for the porechop job `analysis/logs/demultiplex_and_trim.o`

Also the pre filter stats was changed to take the trimmed reads as input and the post filter was changed to take the filter output as it was only taking the trimmed output.

## Altering pipeline

As the reads were already bascalled and I couldn’t demultiplex them using `deepbinner` I commented out the lines in the `Snakefile` that included the rules for these.

### Create sample sheet

```
cd /hps/nobackup/research/zi/mbhall/sylvianne_emergency_analysis/docs
vim samples.csvregion,nanopore_run_id,sample_id,guuid,nanopore_barcode,flowcell_version
sylvianne,sylvianne,BC09,,barcode09,
sylvianne,sylvianne,BC10,,barcode10,
```