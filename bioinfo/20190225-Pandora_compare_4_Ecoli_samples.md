# Pandora compare 4 E coli samples

```
  
```

# Pandora compare 4 E coli samples

1. Run pandora map

### For Cardio

```
sample=H131800734 ; run=ecoli20170616 ; barcode=BC03

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"
mkdir -p pandora_map_with_discovery
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/Logs/pandora_map_"$sample"
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_pandora.sh
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/"$run"/data/porechopped/"$barcode".fastq.gz
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/
version="1bfe7ea9cfac33c70efa8ae92c236148"
pandora_url="shub://rmcolq/pandora:pandora@${version}"
prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_all/ecoli_pangenome_PRG_290818.fa
bsub.py 32 "$log" bash "$script" \
  "$fastq" \
  "$output" \
  "$prg" \
  "$pandora_url"
# after map finishes run the following
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/k15.w14.*
mkdir denovo
mv *K11.fa denovo/
###############################
sample=H151080744 ; run=ecoli20170412 ; barcode=BC11

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"
mkdir -p pandora_map_with_discovery
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/Logs/pandora_map_"$sample"
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_pandora.sh
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/"$run"/data/porechopped/"$barcode".fastq.gz
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/
version="1bfe7ea9cfac33c70efa8ae92c236148"
pandora_url="shub://rmcolq/pandora:pandora@${version}"
prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_all/ecoli_pangenome_PRG_290818.fa
bsub.py 32 "$log" bash "$script" \
  "$fastq" \
  "$output" \
  "$prg" \
  "$pandora_url"
# after map finishes run the following
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/
mkdir denovo
mv *K11.fa denovo/
```

### For 2 other samples

```
run=CFT073 ; sample=SRX5299443
run=RHB11-C04 ; sample=SRX5299467

mkcd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$run"
mkdir -p pandora_map_with_discovery
mkdir -p Logs
fastq=/nfs/leia/research/iqbal/rmcolq/data/trace/"$sample".fastq
cp "$fastq" .
fastq="$sample".fastq
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$run"/Logs/pandora_map_"$run"
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_pandora.sh
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$run"/pandora_map_with_discovery/
version="1bfe7ea9cfac33c70efa8ae92c236148"
pandora_url="shub://rmcolq/pandora:pandora@${version}"
prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_all/ecoli_pangenome_PRG_290818.fa
mem=40
bsub.py "$mem" "$log" bash "$script" \
  "$fastq" \
  "$output" \
  "$prg" \
  "$pandora_url"

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$run"/pandora_map_with_discovery/
mkdir denovo
mv *K11.fa denovo/
```

* * *

2. Convert genotype vcf to genotype fasta

In this step we apply the genotyped VCF variants to the pandora consensus fasta file - as this is the reference that the VCF variants is with respect to.

First we need to create a fasta version of the consensus fastq.

```
declare -a samples=("RHB11-C04" "CFT073" "H151080744" "H131800734")
for sample in "${samples[@]}"
do
	cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/ || break
	fq=pandora.consensus.fq.gz
	fa=pandora.consensus.fa
	gunzip -c "$fq" | awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' > "$fa"
done
```

Before applying the VCF changes to the fasta file, I need to filter the VCF as it contains some invalid entries.

This is done with the following python script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py`

And run by first indexing the gzipped version of the file with `tabix` and then copying the name of this file to equal that of the non-bgziped version of the VCF (for `pysam`)

```
declare -a samples=("RHB11-C04" "CFT073" "H151080744" "H131800734")
for sample in "${samples[@]}"
do
	cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/ || break
	vcf=pandora_genotyped.vcf
	bgzip -c "$vcf" > "$vcf".gz
	tabix -p vcf "$vcf".gz
	cp "$vcf".gz.tbi "$vcf".tbi
	script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py
	filtered_vcf="${vcf/.vcf}".filtered.vcf.gz
	python3 "$script" "$vcf" | bgzip -c > "$filtered_vcf"
	tabix -p vcf "$filtered_vcf"
done
```

Now we apply the changes in the pandora genotype VCF to the fasta file (`ref`) using a python script I wrote `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py`

```
declare -a samples=("RHB11-C04" "CFT073" "H151080744" "H131800734")
for sample in "${samples[@]}"
do
	cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/ || break
	vcf=pandora_genotyped.filtered.vcf.gz
	ref=pandora.consensus.fa
	script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py
	output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/"$sample"/pandora_map_with_discovery/pandora.genotyped."$sample".fa
	python3 "$script" --fasta "$ref" --vcf "$vcf" --output "$output"
done
```

* * *

3. Add local assembly paths to gene cluster MSAs

The original gene cluster multiple sequence alignments for /E. coli/ genes are located here: `/hps/nobackup/iqbal/rmcolq/data/panX/Escherichia_coli`. The location of the intergenic MSAs is in `/hps/nobackup/iqbal/rmcolq/data/piggy/E_coli_intergenic_50.tsv`.

I will make a copy of the gene directory and unzip all the files

```
mkcd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/original_msas
cp -r /hps/nobackup/iqbal/rmcolq/data/panX/Escherichia_coli genes
cd genes
for f in $(find  . -name '*.fa.gz')
do
gzip -d "$f"
done
```

I will also copy the intergenic genes into this directory using script `copy_intergenic_msas.sh`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/original_msas
mkdir -p intergenic && cd intergenic
tsv=/hps/nobackup/iqbal/rmcolq/data/piggy/E_coli_intergenic_50.tsv
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/copy_intergenic_msas.sh
bsub.py 1 copy_intergenic bash "$script" $(pwd) "$tsv"
```

Next is to run a python script - `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/add_paths_to_msa.py` - to add the de novo paths for their respective multiple sequence alignment fasta files.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/
mkdir -p panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/add_paths_to_single_msa.py
declare -a denovo_paths=($(realpath RHB11-C04/pandora_map_with_discovery/denovo) $(realpath CFT073/pandora_map_with_discovery/denovo) $(realpath H151080744/pandora_map_with_discovery/denovo) $(realpath H131800734/pandora_map_with_discovery/denovo))
msas=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/original_msas
output=panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/
bsub.py 4 add_denovo_paths_combined python3 "$script" --paths "${denovo_paths[@]}" --genes "$msas" --output "$output"
```

Need to make sure all read headers are unique. Use script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_all_headers_unique.sh`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/msa_fastas
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_all_headers_unique.sh
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/logs/make_all_msa_headers_unique
bsub.py 1 "$log" bash "$script" $(pwd)
```

* * *

5. Redo multiple sequence alignment of those gene clusters with the following script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/msa_master_submit.sh`

Submitted this with

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/logs/msa_master_submit
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/msa_master_submit.sh
input_dir=$(pwd)
threads=4
bsub.py 1 "$log" bash "$script" "$input_dir" "$threads"
```

* * *

6. Make PRGs for new gene clusters

Clone Rachel’s nextflow pipeline

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts
git clone https://github.com/rmcolq/make_prg.git
cd make_prg
pipenv install -r requirements.txt
pipenv run pytest
```

Next thing I need to do it is create a tsv index file containing sample ids in column 1 and the path to the MSA for that sample.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734
# move all the MSAs into a folder
mkdir -p msa_fastas
mv *.fa !$
# make prgs
mkdir -p make_prgs
cd !$
tsv_file=combined_msa_index.tsv
echo -e "sample_id\tinfile" > "$tsv_file"
for f in $(find  ../msa_fastas -name '*.fa')
do
filename=$(basename $f)
fullpath=$(realpath $f)
sample_id="${filename/_na_aln.fa}"
echo -e "$sample_id\t$fullpath" >> "$tsv_file"
done
```

Run the `make_prg` nextflow pipeline with `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/run_script.sh`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
pipenv shell
mkdir -p /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/nextflow_mess
work_dir=!$
cd /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
nf=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/make_prg_nexflow.nf
config=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/nextflow.config
pipeline_root=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
tsv=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/combined_msa_index.tsv
run_script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/run_script.sh
bash "$run_script" "$nf" "$config" "$work_dir" "$tsv" "$pipeline_root"
```

Expecting there to be 2849 PRGs in the resulting PRG file. Got 1 less. The failed file is `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/msa_fastas/GC00000562_1_na_aln.fa` and the log files are in directory `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/nextflow_mess/fa/4a85714965076380ac761c70025960`. I am going to continue without denovo variants for this gene.

After that has completed there is a PRG file containing all PRGs for the new MSAs. Move this into the correct directory.

```
cd /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
mv pangenome_PRG.fa /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/denovo_updated_prgs.fa
```

Get the PRGs for genes that didn’t have paths added to them and combine these with the newly updated ones. Using python script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/combine_updated_and_old_prgs.py`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs
original_prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_genes_only/ecoli_pangenome_PRG_290818.fa
new_prg=denovo_updated_prgs.fa
output=combined_prgs.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/combine_updated_and_old_prgs.py
python3 "$script" --original_prg "$original_prg" --new_prgs "$new_prg" --output "$output"

# as a sanity check, make sure you have the same number of seqs in new PRG and old PRG file
EXPECTED=$(grep -c '^>'  "$original_prg")
RESULT=$(grep -c '^>'  "$output")
if [ "$RESULT" = "$EXPECTED" ]; then
  echo "equal"
else
  echo "not equal"
fi
```

* * *

7. Run pandora index

We will now index this new combined PRG file.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs
prg=combined_prgs.fa
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/logs/pandora_index_updated_combined_prg
mem=16
pandora=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/containers/pandora.simg
bsub.py "$mem" "$log" singularity exec "$pandora" pandora index "$prg"
```

Also tried to run the parallel version using nextflow. This pipeline splits the PRG into chunks and then indexes. At the end all the output files are combined.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs
prg=$(realpath combined_prgs.fa)
num_prg=$(grep -c '^>' $prg)
mkdir -p /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/parallel_index
cd !$
bsub.py 4 index_prg nextflow run /nfs/leia/research/iqbal/rmcolq/git/DPhil_analysis/nextflow/index_prg.nf --pangenome_prg "$prg" --num_prg "$num_prg" -c /nfs/leia/research/iqbal/rmcolq/git/DPhil_analysis/nextflow/nextflow.config
```

That failed with a weird bus error. Trying something different

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs
prg=$(realpath combined_prgs.fa)
num_prg=$(grep -c '^>' $prg)
mkdir -p /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/parallel_index
cd !$
mkdir -p kmer_prgs
cp "$prg" .
prg=$(realpath combined_prgs.fa)
bsub.py 4 index_prg nextflow run /nfs/leia/research/iqbal/rmcolq/git/DPhil_analysis/nextflow/index_prg.nf --pangenome_prg "$prg" --num_prg "$num_prg" --final_outdir $(pwd) -c /nfs/leia/research/iqbal/rmcolq/git/DPhil_analysis/nextflow/nextflow.config 
```

* * *

8. Run pandora compare with the new PRG.

We will run `pandora compare` on the samples using the new PRG.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/
mkcd combine
# move parallel index and prg here
cp /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/parallel_index/combined_prgs.fa.w14.k15.idx .
cp /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/parallel_index/combined_prgs.fa .
cp -r /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/make_prgs/parallel_index/kmer_prgs .

# get the correct, latest pandora container
version="1bfe7ea9cfac33c70efa8ae92c236148"
singularity pull --force --name pandora.simg shub://rmcolq/pandora:pandora@"$version" 
```

Make the read index file for pandora compare

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/combine
sample=H131800734 ; run=ecoli20170616 ; barcode=BC03
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/"$run"/data/porechopped/"$barcode".fastq.gz
echo -e "$sample\t$fastq" > read_index.tsv
sample=H151080744 ; run=ecoli20170412 ; barcode=BC11
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/"$run"/data/porechopped/"$barcode".fastq.gz
echo -e "$sample\t$fastq" >> read_index.tsv
run=CFT073 ; sample=SRX5299443
fastq=/nfs/leia/research/iqbal/rmcolq/data/trace/"$sample".fastq
echo -e "$run\t$fastq" >> read_index.tsv
run=RHB11-C04 ; sample=SRX5299467
fastq=/nfs/leia/research/iqbal/rmcolq/data/trace/"$sample".fastq
echo -e "$run\t$fastq" >> read_index.tsv
```

Run compare

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx_modified_for_RHB11-C04_CFT073_H151080744_H131800734/combine
container=pandora.simg
read_index=read_index.tsv
prg=combined_prgs.fa
outdir=$(pwd)
max_covg=100
bsub.py 60 compare singularity exec "$container" pandora compare \
  --prg_file "$prg" \
  --outdir "$outdir" \
  --read_index "$read_index" \
  --genotype \
  --max_covg "$max_covg" 
```