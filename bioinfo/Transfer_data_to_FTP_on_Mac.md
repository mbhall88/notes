# Transfer data to FTP on Mac

```
  
```

# Transfer data to FTP on Mac

## Download and install Cyberduck

Head to the [Cyberduck homepage](https://cyberduck.io) to download and install Cyberduck. Cyberduck is a client that allows you to mount and see remote servers - such as FTP sites - from your local computer.

Open the Cyberduck application once installed.

## Connect to EBI FTP site

### Add a new connection

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.05.57.png)

Enter details as follows:
/Select FTP (File Transfer Protocol) from the dropdown box/

```
Nickname: EBI
Server: ftp-private.ebi.ac.uk
Username: madagascox
Port: 21
```

After entering those details, close that window (little red circle in top left corner).

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.19.29.png)

Highlight EBI server and *press Enter*.

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.21.00.png)

A popup box will appear - *press Continue*.

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.20.24.png)

You should then be prompted to enter the password for the server.

```
password: Sc2x5iTd
```

After this you should now see a directory called `upload` under the browser tab

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.24.53.png)

You can now drag and drop files/folders into the `upload` directory to upload them to our server.

If you have limited internet connectivity and your uploads are quite slow I would suggest putting all the data you want to transfer into a directory and then archiving the folder by right-clicking on it in Finder and selecting *compress* . That should now create a `.zip` file named after the directory. Then all you need to do is upload this zipped file.

![](Transfer%20data%20to%20FTP%20on%20Mac/Screenshot%202018-11-13%2014.34.57.png)