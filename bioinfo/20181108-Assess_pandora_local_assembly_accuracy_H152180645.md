# Assess pandora local assembly accuracy H152180645

```
  
```

# Assess pandora local assembly accuracy H152180645

Below are the singular commands for this pipeline on sample H152180645.

1. Run pandora map

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645
mkdir -p pandora_map_with_discovery
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/pandora_map_H152180645_080119
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_pandora.sh
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/ecoli20170616/data/porechopped/BC01.fastq.gz
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/
pandora=/nfs/leia/research/iqbal/mbhall/Software/pandora/build/pandora
prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_genes_only/ecoli_pangenome_PRG_290818.fa
bsub.py 32 "$log" bash "$script" \
  "$fastq" \
  "$output" \
  "$prg" \
  "$pandora"
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/k15.w14.08012019
mkdir denovo
mv *K11.fa denovo/
```

* * *

2. Convert genotype vcf to genotype fasta

In this step we apply the genotyped VCF variants to the pandora consensus fasta file - as this is the reference that the VCF variants is with respect to.

First we need to create a fasta version of the consensus fastq.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/k15.w14.08012019/
fq=pandora.consensus.fq.gz
fa=pandora.consensus.fa
gunzip -c "$fq" | awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' > "$fa"
```

Before applying the VCF changes to the fasta file, I need to filter the VCF as it contains some invalid entries.

This is done with the following python script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py`

And run by first indexing the gzipped version of the file with `tabix` and then copying the name of this file to equal that of the non-bgziped version of the VCF (for `pysam`)

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/k15.w14.08012019/
vcf=pandora_genotyped.vcf
bgzip -c "$vcf" > "$vcf".gz
tabix -p vcf "$vcf".gz
cp "$vcf".gz.tbi "$vcf".tbi
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py
filtered_vcf="${vcf/.vcf}".filtered.vcf.gz
python3 "$script" "$vcf" | bgzip -c > "$filtered_vcf"
tabix -p vcf "$filtered_vcf"
```

Now we apply the changes in the pandora genotype VCF to the fasta file (`ref`) using a python script I wrote `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/k15.w14.08012019
vcf=pandora_genotyped.filtered.vcf.gz
ref=pandora.consensus.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery/pandora.genotyped.H152180645.fa
python3 "$script" --fasta "$ref" --vcf "$vcf" --output "$output"
```

* * *

3. Compare genotype fasta to "truth"

Next, we’ll compare this pandora "genotyped" fasta to the polished pacbio assembly for that assembly ("truth") and see how close they are using `dnadiff`. The script I used to run dnadiff is `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery
mkdir -p dnadiff
assembly=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data/H152180645/H152180645_pacbio_assembly.pilon.fa
# setup variable for dnadiff script
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh
query=pandora.genotyped.H152180645.fa
prefix=dnadiff/H152180645_genotyped_to_pilon_assembly
log="../Logs/H152180645_genotyped_to_pilon_assembly"
bsub.py 2 "$log" "$script" "$assembly" "$query" "$prefix"

# also get dnadiff for consensus to truth
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_with_discovery
assembly=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data/H152180645/H152180645_pacbio_assembly.pilon.fa
# setup variable for dnadiff script
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh
query=k15.w14.08012019/pandora.consensus.fa
prefix=dnadiff/H152180645_consensus_to_pilon_assembly
log="../Logs/H152180645_consensus_to_pilon_assembly"
bsub.py 2 "$log" "$script" "$assembly" "$query" "$prefix"
```

* * *

4. Add local assembly paths to gene cluster MSAs

The original gene cluster multiple sequence alignments for /E. coli/ are located here: `/hps/nobackup/iqbal/rmcolq/data/panX/Escherichia_coli`

I will make a copy of this directory and unzip all the files

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/
cp -r /hps/nobackup/iqbal/rmcolq/data/panX/Escherichia_coli panx
cd panx
for f in $(find  . -name '*.fa.gz')
do
gzip -d "$f"
done
```

Next is to run a python script - `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/add_paths_to_msa.py` - to add the de novo paths for their respective multiple sequence alignment fasta files.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/
mkdir -p panx_modified
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/add_paths_to_msa.py
denovo_paths=pandora_map_with_discovery/k15.w14.08012019/denovo/
msas=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/panx
output=panx_modified/
python3 "$script" --paths "$denovo_paths" --genes "$msas" --output "$output"
```

* * *

5. Redo multiple sequence alignment of those gene clusters with the following script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/msa_master_submit.sh`

Submitted this with

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/panx_modified
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/msa_master_submit
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/msa_master_submit.sh
input_dir=$(pwd)
threads=4

bsub.py 1 "$log" bash "$script" "$input_dir" "$threads"
```

* * *

6. Make PRGs for new gene clusters

Clone Rachel’s nextflow pipeline

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts
git clone https://github.com/rmcolq/make_prg.git
cd make_prg
pipenv install -r requirements.txt
pipenv run pytest
```

Next thing I need to do it is create a tsv index file containing sample ids in column 1 and the path to the MSA for that sample.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/
mkdir -p make_prgs
cd !$
tsv_file=H152180645_msa_index.tsv
echo -e "sample_id\tinfile" > "$tsv_file"
for f in $(find  ../panx_modified -name '*.fa')
do
filename=$(basename $f)
fullpath=$(realpath $f)
sample_id="${filename/_na_aln.fa}"
echo -e "$sample_id\t$fullpath" >> "$tsv_file"
done
```

Run the `make_prg` nextflow pipeline with `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/run_script.sh`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
pipenv shell
mkdir -p /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/nextflow_mess
cd /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
nf=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/make_prg_nexflow.nf
config=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/nextflow.config
work_dir=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/nextflow_mess
pipeline_root=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
tsv=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/H152180645_msa_index.tsv
run_script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg/run_script.sh
bash "$run_script" "$nf" "$config" "$work_dir" "$tsv" "$pipeline_root"
```

Expecting there to be 2850 PRGs in the resulting PRG file

After that has completed there is a PRG file containing all PRGs for the new MSAs. Move this into the correct directory.

```
cd /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/make_prg
mv pangenome_PRG.fa /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/denovo_updated_prgs.fa
# clean up after nextflow
rm -rf /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/nextflow_mess
```

Get the PRGs for genes that didn’t have paths added to them and combine these with the newly updated ones. Using python script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/combine_updated_and_old_prgs.py`

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs
original_prg=/nfs/leia/research/iqbal/rmcolq/projects/pangenome_prg/ecoli/290818_genes_only/ecoli_pangenome_PRG_290818.fa
new_prg=denovo_updated_prgs.fa
output=H152180645_prgs.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/combine_updated_and_old_prgs.py
python3 "$script" --original_prg "$original_prg" --new_prgs "$new_prg" --output "$output"

# as a sanity check, make sure you have the same number of seqs in new PRG and old PRG file
EXPECTED=$(grep -c '^>'  "$original_prg")
RESULT=$(grep -c '^>'  "$output")
if [ "$RESULT" = "$EXPECTED" ]; then
  echo "equal"
else
  echo "not equal"
fi
```

* * *

7. Run pandora index

We will now index this new combined PRG file.

```
cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs
prg=H152180645_prgs.fa
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/pandora_index_updated_prg
mem=10
bsub.py "$mem" "$log" pandora index "$prg" 
```

Longest gene to index was `GC00004221` at approx. 28hrs. Also tried to run the parallel version using nextflow. This pipeline splits the PRG into chunks and then indexes and maps these chunks. At the end all the output files are combined. Note: `chunks` actually dictates the number of reads to put in each chunk, not the actual number of chunks.\```
mkdir -p /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow_mess
cd /nfs/leia/research/iqbal/mbhall/Projects/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/pandora_index
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/pandora_index_parallel/run_script.sh
reads=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/ecoli20170616/data/porechopped/BC01.fastq.gz
pangenome_prg=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/H152180645_prgs.fa
chunks=500
pipeline_root=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/pandora_index_parallel
final_outdir=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/
nf=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/pandora_index_parallel/pandora_index_and_map.nf
config=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/pandora_index_parallel/nextflow.config
work_dir=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow_mess
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/pandora_index_and_map
bash "$script" "$nf" "$config" "$work_dir" "$reads" "$pipeline_root" "$pangenome_prg" "$chunks" "$final_outdir" "$log"

```
  

At the moment I have to add the VCF header to the combined VCF by hand. This is doable in nextflow and I will update the nextflow script at some point.
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery
head -n 12 vcf_headers.txt | cat - pandora_genotyped.vcf > tmp.vcf
mv tmp.vcf pandora_genotyped.vcf

```
  

* * *

  

8.  Run 1 without `—discovery` and with the new PRG.

  
We will run `pandora map` on the sample using the new PRG and without the option to run de novo discovery.
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645
mkcd genotype_with_new_prg

```
  

  
```

script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_pandora_without_discovery.sh
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/pandora_map_H152180645_new_prg_140119
fastq=/hps/nobackup/iqbal/mbhall/Pandora_variation/Data/Cardio/ecoli20170616/data/porechopped/BC01.fastq.gz
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/genotype_with_new_prg
prg=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/make_prgs/H152180645_prgs.fa
pandora=/nfs/leia/research/iqbal/mbhall/Software/pandora/build/pandora

bsub.py 32 "$log" bash "$script" \\
  "$fastq" \\
  "$output" \\
  "$prg" \\
  "$pandora"

```
  

The resulting output is located in `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/genotype_with_new_prg/k15.w14.14012019`

* * *

An interesting point here is to note that when running `pandora index` and `map` through the nextflow pipeline and combining all results at the end versus running the programs on their own you get different numbers of genes in the consensus file. The nextflow combined consensus has 4835 genes, whereas the consensus produced by `pandora map` run on the whole PRG has 4621 genes. Prior to running denovo the consensus had 4620 genes.
I will run both through the rest of the pipeline. To delineate between them I will split them into folders.
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery
mv ../genotype_with_new_prg/k15.w14.14012019 .
rm -rf genotype_with_new_prg/
mkdir nextflow
mv nextflow_mess/ nextflow/
mv pandora_consensus.fastq nextflow/
mv pandora_genotyped.vcf nextflow/
mv vcf_headers.txt nextflow/ 

```
* * *

9.  Run 2. on new genotype VCF.

  
In this step we apply the genotyped VCF variants to the pandora consensus fasta file - as this is the reference that the VCF variants is with respect to.
  
First we need to create a fasta version of the consensus fastq.
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/k15.w14.14012019/
fq=pandora.consensus.fq.gz
fa=pandora.consensus.fa
gunzip -c "$fq" | awk '{if(NR%4==1) {printf(">%s\\n",substr($0,2));} else if(NR%4==2) print;}' > "$fa"

# for the nextflow files

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow
fq=pandora_consensus.fastq
fa=pandora.consensus.fa
awk '{if(NR%4==1) {printf(">%s\\n",substr($0,2));} else if(NR%4==2) print;}' "$fq" > "$fa"

```
  

Before applying the VCF changes to the fasta file, I need to filter the VCF as it contains some invalid entries.
  
This is done with the following python script `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py`
  
And run by first indexing the gzipped version of the file with `tabix` and then copying the name of this file to equal that of the non-bgziped version of the VCF (for `pysam`)
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/k15.w14.14012019/
vcf=pandora_genotyped.vcf
bgzip -c "$vcf" > "$vcf".gz
tabix -p vcf "$vcf".gz
cp "$vcf".gz.tbi "$vcf".tbi
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py
filtered_vcf="${vcf/.vcf}".filtered.vcf.gz
python3 "$script" "$vcf" | bgzip -c > "$filtered_vcf"
tabix -p vcf "$filtered_vcf"

# for the nextflow files

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow/
vcf=pandora_genotyped.vcf
bgzip -c "$vcf" > "$vcf".gz
tabix -p vcf "$vcf".gz
cp "$vcf".gz.tbi "$vcf".tbi
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/filter_invalid_vcf_lines.py
filtered_vcf="${vcf/.vcf}".filtered.vcf.gz
python3 "$script" "$vcf" | bgzip -c > "$filtered_vcf"
tabix -p vcf "$filtered_vcf"

```
  

Now we apply the changes in the pandora genotype VCF to the fasta file (`ref`) using a python script I wrote `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py`
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/k15.w14.14012019
vcf=pandora_genotyped.filtered.vcf.gz
ref=pandora.consensus.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/pandora.genotyped.H152180645.fa
python3 "$script" --fasta "$ref" --vcf "$vcf" --output "$output"

# for the nextflow files

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow
vcf=pandora_genotyped.filtered.vcf.gz
ref=pandora.consensus.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/apply_vcf.py
output=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow/pandora.genotyped.H152180645.fa
python3 "$script" --fasta "$ref" --vcf "$vcf" --output "$output"

```
  

* * *

  

10.  Run 3 on new genotype fasta.

  
Lastly, we’ll compare this final pandora "genotyped" fasta to the polished pacbio assembly for that assembly ("truth") and see how close they are using `dnadiff`. The script I used to run dnadiff is `/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh`
  
```

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery
mkdir dnadiff_reports
assembly=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data/H152180645/H152180645_pacbio_assembly.pilon.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh
query=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/pandora.genotyped.H152180645.fa
prefix=dnadiff_reports/H152180645_genotyped_to_pilon_assembly
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/dnadiff_updated_prg_genotyped_to_pilon_assembly
bsub.py 2 "$log" "$script" "$assembly" "$query" "$prefix"

# for the nextflow files

cd /hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/pandora_map_without_discovery/nextflow
mkdir dnadiff_reports
assembly=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Polishing/data/H152180645/H152180645_pacbio_assembly.pilon.fa
script=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/scripts/run_dnadiff.sh
query=pandora.genotyped.H152180645.fa
prefix=dnadiff_reports/H152180645_genotyped_to_pilon_assembly
log=/hps/nobackup/iqbal/mbhall/Pandora_variation/Analysis/Cardio/Assess_local_assembly/data/H152180645/Logs/dnadiff_nextflow_updated_prg_genotyped_to_pilon_assembly
bsub.py 2 "$log" "$script" "$assembly" "$query" "$prefix"

```
  

* * *

Result
------

The original consensus sequence from pandora for this sample had 9987 SNPs between the consensus fasta and truth and 730 indels. Accounting for genotyped sites by pandora gave 8582 SNPs between the genotype fasta and truth and 734 indels. And finally, after adding denovo candidates into the graph there are 10251 SNPs and 752 indels (plus one extra gene).
  
The candidates from the nextflow pipeline lead to 10591 SNPs and 864 indels (plus 215 extra genes).
```