# Python script template

```
  
```

# Python script template

```
import sys
import argparse
from pathlib import Path

def cli():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--vcf",
                        type=str,
                        required=True,
                        help="VCF file.")

    parser.add_argument("--fasta",
                        type=str,
                        required=True,
                        help="Fasta file to apply variants to.")

    parser.add_argument("-o", "--output",
                        type=str,
                        help="""Path to output fasta file as. 
                        Default is STDOUT (-).""",
                        default="-")

    args = parser.parse_args()

    if args.output == "-":
        args.output = sys.stdout
    else:
        p = Path(args.output)
        assert p.parent.is_dir()
        args.output = p.open('w')

    assert Path(args.vcf).is_file()
    assert Path(args.fasta).is_file()

    return args

def main(args):
    pass

if __name__ == '__main__':
    main(cli()) 
```