# Jupyter on the cluster

```
  
```

# Jupyter on the cluster

\#jupyter



First thing we will define is some port number variables we use in different places to make it easy to control

```
local_port=9000
middle_port=8888
jupyter_port=8080 
```

1. Start and interactive job on the cluster with whatever parameters you need:

```
bsub -Is "$SHELL" 
```

2. Note the hostname the interactive job is running on and start jupyter:

```sh
echo "$HOSTNAME"
local_port=9000
middle_port=8888
jupyter_port=8080
# start jupyter
# export XDG_RUNTIME_DIR=""
python -m jupyter notebook --no-browser --port="$jupyter_port"

# NOTE DOWN URL NOTEBOOK IS RUNNING ON
# http://localhost:8080/?token= 
```

If you get a permission error when trying to run `jupyter notebook` then try running `export XDG_RUNTIME_DIR=""`.

3. In a new terminal window (from your local machine):

```sh
local_port=9000
middle_port=8888
jupyter_port=8080

# on noah
ssh -L "$local_port":localhost:"$middle_port" noah-ext
# log into yoda and forward ports
ssh -L "$local_port":localhost:"$middle_port" yoda-login-1

# log into the interactive job you have running
# change XX-YY to whatever numbers your interactive job has
interactive_host="XX-YY"
bsub -Is -m "$interactive_host" "$SHELL"

# forward the port from the interactive job back to yoda login
local_port=9000
middle_port=8888
jupyter_port=8080
# on ebi-cli - make sure you log back into the same login node as previously
ssh -R "$middle_port":localhost:"$jupyter_port" noah-login-XXX
# or on yoda
ssh -R "$middle_port":localhost:"$jupyter_port" yoda-login-XXX
```

4. Open new tab in your web browser and enter in the URL noted down from Step 2. \*Make sure your replace the `localhost:<PORT_NUM>` with the value of the `local_port` variable you set in the beginning.\* For example - In the above we would change `http://localhost:8080/?token=<TOKEN>` to `http://localhost:9000/?token=<TOKEN>`