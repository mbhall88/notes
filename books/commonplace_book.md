
[TOC levels=1-3]:#

# Table of Contents
- [A](#a)
  - [Marcus Aurelius](#marcus-aurelius)
    - [Meditations](#meditations)
- [B](#b)
  - [Sarah Bakewell](#sarah-bakewell)
    - [At the Existentialist Cafe](#at-the-existentialist-cafe)
- [C](#c)
  - [James Clear](#james-clear)
    - [Atomic Habits](#atomic-habits)
- [E](#e)
  - [Ralph Waldo Emerson](#ralph-waldo-emerson)
  - [Thomas Edison](#thomas-edison)
- [H](#h)
  - [Ryan Holiday](#ryan-holiday)
    - [Ego is the enemy](#ego-is-the-enemy)
    - [Stillness is the Key](#stillness-is-the-key)
- [J](#j)
  - [William James](#william-james)
- [N](#n)
  - [Cal Newport](#cal-newport)
    - [Deep Work](#deep-work)
    - [Digital Minimalism](#digital-minimalism)
- [O](#o)
  - [George Orwell](#george-orwell)
- [S](#s)
  - [Arthur Schopenhauer](#arthur-schopenhauer)
- [Y](#y)
  - [Scott H. Young](#scott-h.-young)
    - [Ultralearning](#ultralearning)



# A

## Marcus Aurelius

### Meditations

Book 1, Meditation 7
> If anyone, after falling out with me in a moment of temper, showed signs of wanting to make peace again, I was to be ready at once to meet them half-way. Also I was to be accurate in my reading, and not content with a mere general idea of the meaning; and not to let myself be too quickly convinced by a glib tongue.

Book 2, Meditation 1
> Begin each day by telling yourself: Today I shall be meeting with interference, ingratitude, insolence, disloyalty, ill-will, and selfishness - all of them due to the offenders' ignorance of what is good or evil.

Book 2, Meditation 2
> But the third, the Reason, the master - on this you must concentrate. Now that your hairs are grey, let it play the part of a slave no more, cease to fume at destiny by ever grumbling at today or lamenting over tomorrow.

Book 2, Meditation 4
> Think of your many years of procrastination; how the gods have repeatedly granted you further periods of grace, of which you have taken no advantage. It is time now to realise the nature of the universe to which you belong, and of that controlling Power whose offspring you are; and to understand that your time has a limit set to it. Use it, then, to advance your enlightenment; or it will be gone, and never in your power again.

Book 2, Meditation 7
> Are you distracted by outward cares? Then allow yourself a space of quiet, wherein you can add to your knowledge of the Good and learn to curb your restlessness. Guard also against another kind of error: the folly of those who weary their days in much business, but lack any aim on which their whole effort, nay, their whole thought, is focused.

Book 2, Meditation 14
> Were you to live three thousand years, or even thirty thousand, remember that the sole life which a man can lose is that which he is living at the moment; and furthermore, that he can have no other life except the one he loses. This means that the longest life and the shortest amount to the same thing. For the passing minute is every man's equal possession, but what has once gone by is not ours. Our loss, therefore, is limited to that one fleeting instant, since no one can lose what is already past, nor yet what is still to come - for how can he be deprived of what he does not possess?

Book 3, Meditation 1
> We must press on, then, in haste; not simply because every hour brings us nearer to death, but because even before then our powers of perception and comprehension begin to deteriorate.

Book 3, Meditation 4
> See then that the flow of your thoughts is kept free from idle or random fancies, particularly those of an inquisitive or uncharitable nature. A man should habituate himself to such a way of thinking that if suddenly asked, 'What is in your mind at this minute?' he could respond frankly and without hesitation.

Book 3, Meditation 5
> In your actions let there be a willing promptitude, yet a regard for the common interest; due deliberation, yet no irresolution; and in your sentiments no pretentious over-refinement. Avoid talkativeness, avoid officiousness.

Book 4, Meditation 3
> Or does the bubble reputation distract you? Keep before your eyes the swift onset of oblivion, and the abysses of eternity before us and behind; mark how hollow are the echoes of applause, how fickle and undiscerning the judgements of professed admirers, and how puny the arena of human fame.

Book 4, Meditation 7
> Put from you the belief that 'I have been wronged', and with it will go the feeling. Reject your sense of injury, and the injury itself disappears.

Book 4, Meditation 18
> A good man does not spy around for the black spots in others, but presses unswerveringly on towards his mark.

Book 4, Meditation 19
> The man whose heart is palpatating for fame after death does not reflect that out of all those who remember him every one will himself soon be dead also, and in course of time the next generation after that, until in the end, after flaring and sinking by turns, the final spark of memory is quenched. Furthermore, even supposing that those who remember you were never to die at all, nor their memories to die either, yet what is that to you? Clearly, in your grave, nothing; and even in your lifetime, what is the good of praise - unless maybe to subserve some lesser design? Surely, then, you are making an inopportune rejection of what Nature has given you today, if all your mind is set on what men will say of you tomorrow.

Book 7, Meditation 46
> 'But I beg you, my friend, to think it possible that nobility and goodness may be something different from keeping oneself and one's friends from danger, and to consider whether a true man, instead of clinging to life at all costs, ought not to dismiss from his mind the question how long he may have to live. Let him leave that to the will of God, in the belief that the womenfolk are right when they tell us that no man can escape his destiny, and let him devote himself to the next problem, how he can best live the life allotted to him.' (From Plato.)

Book 7, Meditation 62
> Always get to know the characters of those whose approval you wish to earn, and the nature of their guiding principles. Look into the sources of their opinions and their motives, and then you will not blame any of their involuntary offences, or feel the want of their approbation.

Book 7, Meditation 64
> When in pain, always be prompt to remind yourself that there is nothing shameful about it and nothing prejudicial to the mind at the helm, which suffers no injury either in its rational or its social aspect. In most cases the saying of Epicurus should prove helpful, that 'Pain is never unbearable or unending, so long as you remember its limitations and do not indulge in fanciful exaggerations.' Bear in mind also that, though we do not realise it, many other things which we find uncomfortable are, in fact, of the same nature as pain: feelings of lethargy, for example, or a feverish temperature, or loss of appetite. When inclined to grumble at any of these, tell yourself that you are giving in to pain.

Book 7, Meditation 73
> When you have done a good action, and another has had the benefit of it, why crave for yet more in addition - applause for your kindness, or some favour in return - as the foolish do?

Book 8, Meditation 8
> You cannot hope to be a scholar. But what you can do is to curb arrogance; what you can do is to rise above pleasures and pains; you can be superior to the lure of popularity; you can keep your temper with the foolish and ungrateful, yes, and even care for them.

Book 8, Meditation 26
> A man's true delight is to do the things he was made for. He was made to show goodwill to his kind, to rise above the promptings of his senses, to distinguish appearances from realities, and to pursue the study of universal Nature and her works.

Book 8, Meditation 33
> Accept modestly; surrender gracefully.

Book 8, Meditation 44
> Make the best of today. Those who aim instead at tomorrow's plaudits fail to remember that future generations will be nowise different from the contemporaries who so try their patience now, and nowise less mortal. In any case, can it matter to you how the tongues of prosperity may wag, or what views of yourself it may entertain?

Book 8, Meditation 57
> The sun is seen to pour down and expend itself in all directions, yet is never exhausted. For this downpuring is but a self-extension; sunbeams, in fact, derive their very name from a word signifying 'to be extended'. To understand the property of a sunbeam, watch the light as it streams into a darkened room through a narrow chink. It prolongs itself forward in a straight line, until it is held up by encountering some solid body which blocks its passage to the air beyond; and then it remains at rest there, without slipping off or falling away. The emission, and the diffusion, of thought should be the counterpart of this: not exhausting, but simply extending itself; not dashing violently or furiously against the obstacles it encounters, nor yet falling away in despair; but holding its ground and lighting up that upon which it rests. Failure to transmit it is mere self-deprivation if light.

Book 9, Meditation 42
> Once you have done a man a service, what more would you have? Is it not enough to have obeyed the laws of your nature, without expecting to be paid for it? That is like the eye demanding a reward for seeing, or the feet for walking. It is for that very purpose that they exist; and they have their due in doing what they were created to do. Similarly, man is born for deeds of kindness; and when he has done a kindly action, or otherwise served the common welfare, he has done what he was made for, and has received his quittance.

Book 10, Meditation 16
> Waste no more time arguing what a good man should be. Be one.

Book 10, Meditation 32
> Let no one have the right to say truthfully of you that you are without integrity or goodness; should any think such thoughts, see that they are without foundation. This all depends upon yourself, for who else can hinder you from attaining goodness and integrity?

Book 10, Meditation 34
> When true principles have once been etched into the mind, even the briefest commonplace will suffice to recall the futility of regrets or fears; such as, for example,  
> 'What are the children of men, but as leaves that  
> drop at the wind's breath?'  
> Just such leaves were those beloved children of yours; leaves, too, are the multitudes, those would-be-convincing voices that scream their plaudits, hurl their curses, or sneer and scoff in secret; leaves, again, are all they into whose hands your fame shall fall hereafter. One and all, they 'flower in the season of springtime', the gales lay them low, and anon the forest puts forth new verdure in their room. Impermanence is the badge of each and every one; and yet you chase after them, or flee from them, as though they were to endure for all eternity. A short time, and your eyes will close; and for the man who bears you to your grave, too, the tears will soon enough be falling.

Book 11, Meditation 4
> Have I done an unselfish thing? Well then, I have my reward. Keep this thought ever present, and persevere.

Book 11, Meditation 16
> The good life can be achieved to perfection by any soul capable of showing indifference to the things that are themselves indifferent. This can be done by giving careful scrutiny first to the elements that compose them, and then to the things themselves; bearing also in mind that none of them is responsible for the opinion we form of it. They make no approaches to us, they remain stationary; it is we who produce judgements about them, and proceed to inscribe these, so to speak, in our minds; despite the fact that it is perfectly in our power either to inscribe nothing at all, or at least to delete promptly anything that may have inscribed itself unawares.

Book 11, Meditation 18
> [When offended] You have no assurance that they are doing wrong at all, for the motives of men's actions are not always what they seem. There is generally much to learn before any judgement can be pronounced with certainty on another's doings.

Book 11, Meditation 18
> Our anger and annoyance are more detrimental to us than the things themselves which anger or annoy us.

Book 11, Meditation 18
> In moments of anger, let the thought always be present that loss of temper is no sign of manliness, but that there is more virility, as well as more natural humanity, in one who shows himself gentle and peaceable; he it is who gives proof of strength and nerve and manliness, not his angry and discontented fellow. Anger is as much a mark of weakness as is grief; in both of them men receive a wound, and submit to a defeat.

Book 12, Meditation 4
> So much more regard have we for our neighbours' judgement of us than our own.

Book 12, Meditation 8
> Observe how man's disquiet is all of his own making, and how troubles come never from another's hand, but like all else are creatures of our own opinion.

Book 12, Meditation 17
> If it is not the right thing to do, never do it; if it is not the truth, never say it. Keep your impulses in hand.

# B

## Sarah Bakewell

### At the Existentialist Cafe

p9

> [Satre on factors that play a role in your 'situation'] Even if the situation is unbearable - perhaps you are facing execution, or sitting in a Gestapo prison, or about to fall off a cliff - you are still free to decide what to make of it in mind and deed. Starting from where you are now, you choose. And in choosing, you also choose who you will be.

p11

> Satre's big question in the mid-1940s was: given that we are free, how can we use our freedom well in such challenging times? In his essay "The End of the War", written just after Hiroshima and published in October 1945 he exhorted his readers to decide what kind of world they wanted, and make it happen. From now on, he wrote, we must always take into account our knowledge that we can destroy ourselves at will, with all our history and perhaps life on earth itself. Nothing stops us but our own free choosing. If we want to survive, we have to *decide* to live. Thus, he offered a philosophy designed for a species that had just scared the hell out of itself, but that finally felt ready to grow up and take responsibility.

p15

> When Satre was offered the Legion d'honneur for his Resistance activities in 1945, and the Nobel Prize in Literature in 1964, he rejected them both citing a writer's need to stay independent of interests and influences. Beauvoir rejected the Legion d'honneur in 1982 for the same reason. In 1949, Francois Mauriac put Satre forward for election to the Academie francaise, but Satre refused it.

p16

> By reflecting on life's vagaries in philosophical ways, [Stoics and Epicureans] believed they could become more resilient, more able to rise above circumstances, and better equipped to manage grief, fear, anger, disappointment or anxiety. In the tradition they passed on, philosophy is neither a pure intellectual pursuit nor a collection of cheap self-help tricks, but a discipline for flourishing and living a fully human, responsible life.

p18

> *Cogito ergo sum*: I think, therefore I am. For Kierkegaard, Descartes had things back to front. In his own view, human existence comes first: it is the starting point for everything we do, not the result of a logical deduction.

p19

> 'Anxiety is the dizziness of freedom', wrote Kierkegaard. Our whole lives are lived on the edge of that precipice, in his view and also Sartre's.

p20

> The way to live is to throw ourselves, not into faith, but into our own lives, conducting them in affirmation of every moment , exactly as it is, without wishing that anything was different, and without harbouring peevish resentment against others or against our fate.

p23

> Sartre was the bridge to all the traditions that he plundered, modernised, personalised and reinvented.

p29

> In the years following existentialism's decline, [the topic of freedom] went out of focus in parts of the world, perhaps because the great liberation movements of the 1950s and 1960s achieved so much in civil rights, decolonisation, women's equality and gay rights. It seemed as though these campaigns had got what they wanted, and that no point remained in talking about liberation politics.

p31

> This notion of 'inhabited philosophy' is one I've borrowed from the English philosopher and novelist Iris Murdoch, who wrote the first full-length book on Sartre and was an early adopter of existensialism (though she later moved away from it). She observed that we need not expect moral philosophers to 'live by' their ideas in a simplistic way, as if they were following a set of rules. But we can expect them to show how their ideas are lived *in*.

p39

> Husserl's philosophy became an exhausting but exciting discipline in which concentration and effort must constantly be renewed. To practise it, he wrote, '*a new way of looking at things* is necessary' - a way that brings us back again and again to our project, so as 'to see what stands before our eyes, to distinguish, to describe'. This was Husserl's natural style of working. It was also a perfect definition of phenomonology.

p41

> Husserly therefore says that, to phenomenologically describe a cup of coffee, I should set aside both the abstract suppositions and any intrusive emotional associations. Then I can concentrate on the dark, fragrant, rich phenomenon in front of me now. This 'setting aside' or 'bracketing out' of speculative add-ons Husserl called *epoché* - a term borrowed from the ancient Sceptics, who used it to mean a general suspension of judgement about the world. Husserl sometimes referred to it as a phenomenological 'reduction' instead: the process of boiling away extra theorising about what coffee 'really' is, so we are left only with the intense and immediate flavour - the phenomenon.

> The result is a great liberation. Phenomenology frees me to talk about my experienced coffee as a serious topic of investigation. It likewise frees me to talk about many areas that come into their own *only* when discussed phenomenologically. An obvious example, close to the coffee case, is expert wine-tasting - a phenomenological practice if ever there was one, and one in which the ability both to discern and to describe experiential qualities are equally important.

p43

> There is another side effect: it ought in theory to free us from ideologies, political and otherwise. In forcing us to be loyal to experience, and to sidestep authorities who try to influence how we interpret that experience, phenomonology has the capacity to neutralise all the 'isms' around it, from scientism to relgious fundamentalism to Marxism to fascism. All are to be set aside in the *epoché* - they have no business intruding on the things themselves. This gives phenomonology a surprisingly revolutionary edge, if done correctly.


p45

> If I dream that a white rabbit runs past me checking its pocket watch, I am dreaming *of* my fantastical dream-rabbit. If I gaze up at the ceiling trying to make sense of the structure of consciousness, I am thinking *about* the structure of consciousness. Except in deepest sleep, my mind is always engaged in this *aboutness*: it has 'intentionality'. Having taken the germ of this from Brentano, Husserl made it central to his whole philosophy.

> Understood in this way, the mind hardly *is* anything at all: it is its aboutness. This makes the human mind (and possibly some animal minds) different from any other naturally occurring entity. Nothing else can be as thoroughly *about* or *of* things as the mind is: even a book only reveals what it's 'about' to someone who picks it up and peruses it, and is otherwise merely a storage device. But a mind that is experiencing nothing, imagining nothing, or speculating about nothing can hardly be said to be a mind at all.

p59

> Heidegger sums this up by saying that Being is not itself a being. That is, it is not a defined or delineated entity of any kind. He distinguishes between the German word *Seiende*, which can refer to any individual entity, such as a mouse or a church door, and *Sein*, which means the Being that such particular beings have. (In English, one way of signalling the distinction is by using the captial 'B' for the latter.)

> Unlike beings, Being is hard to concentrate on and it is easy to forget to think about it. But one particular entity has a more noticeable Being than others, and that is myself, because, unlike clouds and portals, I am the entity who wonders about its being. It even turns out that I have a vague, preliminary, non-philosophical understanding of Being already - otherwise I would not have thought of asking about it. This makes me the best starting point for ontological inquiry. I am both the being whose Being is up for question and the being who sort of already knows the answer.

p70

> But it is in questions and discomfort that philosophy begins.  
> This was the sort of powerful, personal stuff that people craved from philosophy in troubled times: it was one reason why Heidegger acquired such influence. His starting point was reality in its everyday clothes, yet he also spoke in Kierkegaardian tones about the strangest experiences in life, the moments when it all goes horribly wrong - and even the moments when we confront the greatest wrongness of all, which is the prospect of death.

p78

> *Being and Time* contained at least one big idea that should have been of use in resisting totalitarianism. Dasein, Heidegger wrote there, tends to fall under the sway of something called *dasMan* or 'the they' - an impersonal entity that robs us of the freedom to think for ourselves. To live authentically requires resisting or outwitting this influence, but this is not easy because *das Man* is so nebulous. *Man* in German does not mean 'man' as in English (that's *der Mann*), but a neutral abstraction, something like 'one' in the English phrase 'one doesn't do that', or 'they' in 'they say it will be all over by Christmas'.

> If I am to resist *das Man*, I must become answerable to the call of my 'voice of conscience'. This call does not come from God, as a traditional Christian definition of the voice of conscience might suppose. It comes from a truly existentialist source: my own authentic self. Alas, this voice is one I do not recognise and may not hear, because it is not the voice of my habitual 'they-self'. It is an alien or uncanny version of my usual voice. I am familiar with my they-self, but not with my unalienated voice - so, in a weird twist, my real voice is the one that sounds strangest to me.

p88

> If we are temporal beings by our very nature, then authentic existence means accepting, first, that we are finite and mortal. We will die: this all-important realisation is what Heidegger calls authetic 'Being-towardsDeath', and it is fundamental to his philosophy.

> Dasein discovers 'that its uttermost possibility lies in giving itself up'. At that moment, through Being-towards-Death and resoluteness in facing up to one's time, one is freed from the they-self and attains one's true, authentic self.

p103 quoting Flaubert advising Maupassant to consider things 'long and attentively'

> There is a part of everything that remains unexplored, for we have fallen into the habit of remembering, whenever we use our eyes, what people before us thought of the thing we are looking at. Even the slightest thing contains a little that is unknown. We must find it. To describe a blazing fire or a tree in a plain, we must remain before that fire or that tree until they no longer resemble for us any other tree or any other fire.

p103

> Flaubert was talking about literary skill, but he could have been talking about phenomonological method, which follows exactly that process. With the *epoché*, one first discards second-hand notions or received ideas, then one describes the thing as it directly presents itself. For Husserl, this ability to describe a phenomenon without influence from others' theories is what liberates the philosopher.

p120

> [Sartre] gave money away as fast as it came, in order to get it away from him, like a hand grenade. If he did spend it himself, he preferred not to use it on objects but 'on an *evening out*: going to some dancehall, spending big, going everywhere by taxi, etc. etc. - and in short, nothing must remain in place of the money but a memory, sometimes *less* than a memory'.

p130

> These late works of Husserl's are different in spirit from the earlier ones. To Merleau-Ponty, they suggested that Husserl had begun moving away from his inward, idealist interpretation of phenomenology in his last years, towards a less isolated picture of how one exists in a world alongside other people and immersed in sensory experience. Merleau-Ponty even wondered whether Husserl had absorbed some of this from Heidegger - an interpretation with which not everyone agrees.

p131

> For Husserl, therefore, cross-cultural encounters are generally good, because they stimulate people to self-questioning. He suspected that philosophy started in ancient Greece not, as Heidegger would imagine, because the Greeks had a deep, inward-looking relationship with their Being, but because they were a atrading people (albeit sometimes a warlike one) who constantly came across alien-worlds of all kinds.

p149

> [Albert] Camus asks: if life is revealed to be as futile as the labour of Sisyphus, how should we respond?  
> Like Sartre in *Nausea*, he points out that mostly we don't see the fundamental problem of life because we don't stop to think about it. We get up, commute, work, eat, work, commute, sleep. But occasionally a breakdown occurs, a Chandos-like moment in which a beat is skipped and the question of purpose arises. At such moments, we experience 'weariness tinged with amazement', as we confront the most basic question of all: why exactly do we go on living?

p150

> In a way, this is Camus' variant on Heidegger's Question of Being. Heidegger thought the questionable nature of existence looms up when a hammer breaks; Camus thought similarly basic collapses in everyday projects allow us to ask the biggest question in life. Also like Heidegger, he thought the answer took the form of a decision rather than a statement: for Camus, we must decide whether to give up or keep going.

p151

> Much as they liked Camus personally, neither Sartre nor Beauvoir accepted his vision of absurdity. For them, life is *not* absurd, even when viewed on a cosmic scale, and nothing can be gained by saying it is. Life for them is full of real meaning, although that meaning emerges differently for each of us.

> As Sartre argued in his 1943 review of [Camus'] *The Outsider*, basic phenomenological principles show that experience comes to us already charged with significance. A piano sonata *is* a melancholy evocation of longing. If I watch a football match, I see it *as* a football match, not as a meaningless scene in which a number of people run around taking turns to apply their lower limbs to a spherical object. If the latter is what I'm seeing, then I am not watching some essential, truer version of football; I am failing to watch it properly as football at all.

> Sartre knew very well that we can lose sight of the sense of things. If I am sufficiently upset at how my team is doing, or undergoing a crisis in my grasp of the world in general, I might stare hopelessly at the players as though they were indeed a group of random people running around. Many such moments occur in *Nausea*, when Roquentin finds himself flummoxed by a doorknob or a beer glass. But for Sartre, unlike for Camus, such collapses reveal a pathological state: they are failures of intentionality, not glimpses into a greater truth.

p153

> The notion of a specific nothingness sounds odd, but Sartre explains it with a story of Parisian café life. Let's imagine, he suggests, that I have made an appointment to meet my friend Pierre at a certain café at four o'clock. I arrive fifteen minutes late abd look around anxiously. Is Pierre still here? I perceive lots of other things: customers, tables, mirrors and lights, the café's smoky atmosphere, the sound of rattling crockery and a general murmuring hubbub. But there is no Pierre. Those other things form a field against which on item blares out loud and clear: the Absence of Pierre.

p154

> It is a peculiar idea - but what Sartre is trying to get at is the structure of Husserlian intentionality, which defines consciousness as only an insubstantial 'aboutness'. My consciousness is specifically mine, yet it has no real being: it *is* nothing but its tendancy to reach out or point to things. If I look into myself and seem to see a mass of solidified qualities, of personality traits, tendencies, limitations, relics of past hurts and so on, all pinning me down to an identity, I am forgetting that none of these can define me at all. In a reversal of Descartes' 'I think, therefore I am', Sartre argues, in effect, 'I am nothing, therefore I am free'.

p155

> All these devices work because they allow us to pretend that we are not free. We know very well that we can always reset the alarm clock or disable the software, but we arrange things so that this option does not seem readily available. If we didn't resort to such tricks, we would have to deal with the whole vast scope of freedom at every instant, and that would make life extremely difficult.

p157

> But the Stoics cultivated indifference in the face of terrible events, whereas Sartre thought we should remian passionately, even furiously engaged with what happens to us and with what we can achieve. We should not expect freedom to be anything less than fiendishly difficult.

p160

> [Beauvoir's thoughts] We do not thrive in satiety and rest. Human existence mean 'transendence', or going beyond, not 'immanence', or reposing passively inside oneself. It means constant action until the day one runs out of things to do - a day that is unlikely to come as along as you have breath. For Beauvoir and Sartre, this was the big lesson of the war years: the art of life lies in getting things done.

p182

> With [the attitute of machination], we brazenly challenge the earth to give up what we want from it, instead of patiently whittling or cajoling things forth as peasant smallholders or craftsmen do. We bully things into yielding up their goods.

p183

> If we pay proper attention to technology, or rather to what technology reveals about us and our Being, we can gain insight into the truth of human 'belongingness'.

p196

> [Emmanuel Levinas] once said that this shift in thinking [the relationship of Self and Other] had its origin in an experience he had in the [concentration] camp. Like the other prisoners, he had got used to the guards treating them without respect as they worked, as if they were inhuman objects unworthy of fellow feeling. But each evening, as they were marched back behind the barbed-wire fence again, his work group would be greeted by a stray dog who had somehow found its way inside the camp. The dog would bark and fling itself around with delight at seeing them, as dogs do. Through the dog's adoring eyes, the men were reminded each day of what it meant to be acknowledged by another being - to receive the basic recognition that one living creature grants to another.

p197

> Ever since Husserl, phenomenologists and existentialists had been trying to stretch the definition of existence to incorporate our social lives and relationships. Levinas did more: he turned philosophy around entirely so that these relationships were the *foundation* of our existence, not an extension of it.

p210

> The first influences begin in early childhood, [Beauvoir] wrote. While boys are told to be brave, a girl is expected to cry and be weak. Both sexes hear similar fairy tales, but in them the males are heroes, princes or warriors, while the females are locked up in towers, put to sleep, or chained to a rock to wait to be rescued. Hearing the stories, a girl notices that her own mother stays mostly in her home, like an imprisoned princess, while her father goes off to the outside world like a warrior going to war. SHe understand which way her own role will lie.

p212

> Sartre then adds a twist. This time he puts us in the hallway of a Parisian hotel, peering through the keyhole of someone's door - perhaps because of jealousy, lust or curiosity. I am absorbed in whatever I'm seeing, and strain towards it. Then I hear footsteps in the hall - someone is coming! The whole set-up changes. Instead of being lost in the scene inside the room, I am now aware of myself as a peeping tom, which is how I'll appear to the third party coming down the hall. My look, as I peer through the keyhole, becomes 'a *look-looked-at*'. My 'transcendence' - my ability to pour out of myself towards what I am perceinving - is itself 'transcended' by the transcendence of another. That Other has the power to stamp me as a certain kind of object, ascribing definite characteristics to me rather than leaving me to be free.

p213

> 'Hell is other people.' Sartre later explained that he did not mean to say that other people were hellish in general. He meant that *after death* we become frozen in their view, unable any longer to fend of their interpretation. In life, we can still do something to manage the impression we make; in death, this freedom goes and we are left entombed in other's people's memories and perceptions.

p215

> Women, in other words, live much of their lives in what Sartre would have called bad faith, pretending to be objects. They do what the waiter does when he glides around playing the role of waiter; they identify with their 'immanent' image rather than with their 'transcendent' consciousness as a free for-itself. The waiter does it when he's at work; women do it all day and to a greater extent. It is exhausting, because, all the time, a woman's subjectivity is trying to do what comes naturally to subjectivity, which is to assert itself as the centre of the universe. A struggle rages inside every woman, and because of this Beauvoir considered the problem of how to be a woman the existentialist problem par excellence.

p229

> Merleau-Ponty instead saw quite calmly that we exist only through compromise with the world - and that this is fine. The point is not to fight that fact, or to inflate it into too great a significance, but to observe and understand exactly how that compromise works.

p231

> But Merleau-Ponty also followed Husserl and the gestalt psycologists in reminding us that we rarely have these sense experiences 'raw'. Phenomena come to us already shaped by the interpretations, meanings, and expectations with which we are going to grasp them, based on previous experience and the general context of the encounter. We percieve a multicoloured blob on a table directly *as* a bag of sweets, not as a collection of of angles, colours, and shadows that must be decoded and identified.

> Of course we have to learn this skill of interpreting and anticipating the world, and this happens in early childhood, which is why Merleau-Ponty thought child psycology was essential to philosophy.

# C

## James Clear

### Atomic Habits

p15

> It is so easy to overestimate the importance of one defining moment and underestimate the value of making small improvements on a daily basis.

> Here's how the math works out: if you can get 1 percent better each day for one year, you'll end up thirty-seven times better by the time you're done. Conversely, if you get 1 percent worse each day for one year, you'll decline nearly down to zero.

p18

> Your outcomes are a lagging measure of your habits. Your net worth is a lagging measure of your financial habits. Your weight is a lagging measure of your eating habits. Your knowledge is a lagging measure of your learning habits. Your clutter is a lagging measure of your cleaning habits. You get what you repeat.

p20

> In the early and middle stages of any quest, there is often a Valley of Disappointment. You expect to make progress in a linear fashion and it's frustrating how ineffective changes can seem during the first days, weeks, and even months. It doesn't feel like you are going anywhere. It's a hallmark of any compounding process: the most powerful outcomes are delayed.

p21

> When you finally break through the Plateau of Latent Potential, people will call it an overnight success. The outside world only sees the most dramatic event rather than all that preceded it. But you know that it's the work you did long ago - when it seemed that you weren't making any progress - that makes the jump today possible.

> Mastery requires patience. The San Antonio Spurs, one of the most successful teams in NBA history, have a quote from social reformer Jacob Riis hanging in their locker room: "When nothing seems to help, I go and look at a stonecutter hammering away at his rock, perhaps a hundred times without as much as a crack showing in it. Yet at the hundred and first blow it will split in two, and I know it was not that last blow that did it - but all that had gone before."

p23

> Eventually, I began to realise that my results had very little to do with the goals I set and nearly everything to do with the systems I followed.

> Goals are about the results you want to achieve. Systems are about the processes that lead to those results.

p25

> Every Olympian wants to win a gold medal. Every candidate wants to get the job. And if successful and unsuccessful people share the same goals, then the goal cannot be what differentiates the winners from the losers.

p27

> True long-term thinking is goal-less thinking. It's not about any single accomplishment. It is about the cycle of endless refinement and continuous improvement. Ultimately, it is your commitment to the process that will determine your progress.

p28

> You do not rise to the level of your goals. You fall to the level of your systems.

p32

> You may want better health, but if you continue to prioritise comfort over accomplishment, you'll be drawn to relaxing rather than training.

p33

> The ultimate form of intrinsic motivation is when a habit becomes part of your identity. It's one thing to say I'm the type of person who wants this. It's something very different to say I'm the type of person who is this.

p35

> In time, you begin to resist certain actions because "that's not who I am."

p37

> I didn't start out as a writer. I became one through my habits.

p38

> Small habits can make a meaningful difference by providing evidence of a new identity.

> Putting this all together, you can see that habits are the path to changing your identity. The most practical way to change who you are is to change what you do.

p39

> These are big questions, and many people aren't sure where to begin - but they do know what kind of results they want: to get six-pack abs or to feel less anxious or to double their salary. That's fine. Start there and work backward from the results you want to the type of person who could get those results. Ask yourself, "Who is the type of person that could get the outcome I want?" Who is the type of person who could lose forty pounds? Who is the type of person who could learn a new language? Who is the type of person that could run a successful start-up?

p41

> Your identity emerges out of your habits. Every action is a vote for the type of person you wish to become.

p46

> Habits do not restrict freedom. They create it. In fact, the people who don't have their habits handled are often the ones with the least amount of freedom. Without good financial habits, you will always be struggling for the next dollar. Without good health habits, you will always seem to be short on energy. Without good learning habits, you will always feel like you're behind the curve. If you're always being forced to make decisions about simple tasks - when should I work out, where do I go to write, when do I pay the bills - then you have less time for freedom.

p70

> Broadly speaking, the format for creating an implementation intention is: "When situation X arises, I will perform response Y."

p73

> In fact, the tendency for one purchase to lead to another one has a name: the Diderot Effect. The Diderot Effect states that obtaining a new possession often creates a sprial of consumption that leads to additional purchases.

p74

> Habit stacking is a special form of an implementation intention. Rather than pairing your new habit with a particular time and location, you pair it with a current habit.

p95

> This is the secret to self-control. Make the cues of your good habits obvious and the cues of your bad habits invisible.

p109

> You're more likely to find a behavious attractive if you get to do one of your favourite things at the same time. Perhaps you want to hear about the latest celebrity gossip, but you need to get in shape. Using temptation bundling, you could only read the tabloids and watch reality shows at the gym.

p111

> Temptation bundling is one way to create a heightened version of any habit by connecting it with something you already want.

p117

> One of the most effective things you can do to build better habits is to join a culture where your desired behaviour is the normal behaviour. New habits seem achievable when you see others doing them every day.

p142

> It is easy to get bogged down trying to find the optimal plan for change: the fastest way to lose weight, the best program to build muscle, the perfect idea for a side hustle. We are so focused on figuring out the best approach that we never get around to taking action.

> I refer to this as the difference between being in motion and taking action. The two ideas sound similar, but they're not the same. When you're in motion, you're planning and strategizing and learning. Those are all good things, but they don't produce results. Action, on the other hand, is the type of behaviour that will deliver an outcome. If I outline twenty ideas for articles I want to write, that's motion. If I actually sit down and write an article, that's action. If I search for a better diet plan and read a few books on the topic, that's motion. If I actually eat a healthy meal, that's action. Sometimes motion is useful, but it will never produce an outcome by itself. It doesn't matter how many times you go talk to the personal trainer, that motion will never get you in shape. Only the action of working out will get the result you're looking to achieve.

> ...more often than not, we do it because motion allows us to feel like we're making progress without running the risk of failure.

p143

> If you want to master a habit, the key is to start with repetition, not perfection. You don't need to map out every feature of a new habit. You just need to practise it. This is the first takeaway of the 3rd Law: you just need to get your reps in.

p144

> Both common sense and scientific evidence agree: repetition is a form of change.

p146

> One of the most common questions I hear is, "How long does it take to build a new habit?" But what people really should be asking is, "How many does it take to form a new habit?" That is, how many repetitions are required to make a habit automatic?

p153

> ...when deciding where to practice a new habit, it is best to choose a place that is already along the path of your daily routine. Habits are easier to build when they fit into the flow of your life. You are more likely to go to the gym if it is on your way to work because stopping doesn't add much friction to your lifestyle.

p157

> Whenever possible, I leave my phone in a different room until lunch. When it's right next to me, I'll check it all morning for no reason at all. But when it is in another room, I rarely think about it. And the friction is high enough that I won't go get it without a reason. As a result I get three to four hours each morning when I can work without interruption.

p158

> Redesign your life so the actions that matter most are also the actions that are easiest to do.

p162

> "When you start a new habit, it should take less than two minutes to do."

p163

> The idea is to make your habits as easy as possible to start. Anyone can meditate for one minute, read one page, or put one item of clothing away.

> People often think it's weird to get hyped about reading one page or meditating for one minute or making one sales call. But the point is not to do one thing. The point is to master the habit of showing up. The truth is, a habit must be established before it can be improved. If you can't learn the basic skill of showing up, then you have little hope of mastering the final details. Instead of trying to engineer a perfect habit from the start, do the easy thing on a more consistent basis. You have to standardise before you can optimise.

p165

> We rarely think about change this way because everyone is consumed by the end goal. But one push-up is better than not exercising. One minute of guitar practice is better than none at all. One minute of reading is better than never picking up a book. It's better to do less than you hoped than to do nothing at all.

p166

> The more you ritualise the beginning of a process, the more likely it becomes that you can slip into the state of deep focus that is required to do great things.

p169

> Sometimes success is less about making good habits easy and more about making bad habits hard.

p170

> Commitment devices are useful because they enable you to take advantage of good intentions before you can fall victim to temptation.

p175

> If I feel bored for just a fraction of a second, I reach for my phone. It's easy to write off these minor distractions as "just taking a break," but over time they can accumulate into a serious issue.

> The average person spends over two hours per day on social media. What could you do with an extra six hundred hours per year?

p189

> Put another way, the cost of your good habits are in the present. The costs of your bad habits are in the future.

p192

> The more a habit becomes part of your life, the less you need outside encouragement to follow through. Incentives can start a habit. Identity sustains a habit.

p195

> Dyrsmid began each morning with two jars on his desk. One was filled with 120 paper clips. The other was empty. As soon as he settled in each day, he would make a sales call. Immediately after, he would move one paper clip from the full jar to the empty jar and the process would begin again. "Every morning I would start with 120 paper clips in one jar and I would keep dialing the phone until I moved them all to the second jar," he told me.

p197

> Habit tracking is powerful because it leverages multiple Laws of Behavioural Change. It simultaneously makes a behaviour obvious, attractive, and satisying.

> One study of more than sixteen hundred people found that those who kept a daily food log lost twice as much weight as those who did not. The mere act of tracking a behaviour can spark the urge to change it.

> Habit tracking also keeps you honest. Most of us have a distorted view of our own behaviour. We think we act better than we do. Measurement offers one way to overcome our blindness to our own behaviour and notice what's really going on each day.

p201

> Whenever this happens to me [life getting in the way of habits], I try to remind myself of a simple rule: **never miss twice**.

> If I miss one day, I try to get back into it as quickly as possible. Missing one workout happens, but I'm not going to miss two in a row.

> As soon as one streak ends, I get started on the next one.

> The first mistake is never the one that ruins you. It is the spiral of repeated mistakes that follows. Missing once is an accident. Missing twice is the start of a new habit.

> This is a distinguishing feature between winners and losers. Anyone can have a bad performance, a bad workout, or a bad day at work. But when successful people fail, they rebound quickly.

> The problem is not slipping up; the problem is thinking that if you can't do something perfectly, then you shouldn't do it all.

p202

> Furthermore, it's not always about what happens during the workout. It's about being the type of person who doesn't miss workouts. It's easy to train when you feel good, but it's crucial to show up when you don't feel like it - even if you do less than you hope.

> The dark side of tracking a particular behaviour is that we become driven by the number rather than the purpose behind it.

p203

> "When a measure becomes a target, it ceases to become a good measure." Measurement is only useful when it guides you and adds context to a larger picture, not when it consumes you. Each number is simply one piece of feedback in the overall system.

> All of this is to say, it's crucial to keep habit tracking in its proper place. It can feel satisfying to record a habit and track your progress, but the measurement is not the only thing that matters.

p231

> The Goldilocks Rule states that humans experience peak motivation when working on tasks that are right on the edge of their current abilities. Not too hard. Not too easy.

p233

> "What's the difference between the best athletes and everyone else?" I asked, "What do the really successful people do that most don't?" He mentioned the factors you might expect: genetics, luck, talent. But then he said something I wasn't expecting: "At some point it comes down to who can handle the boredom of training every day, doing the same lifts over and over and over."

p234

> As a result, many of us get depressed when we lose focus or motivation because we think that successful people have some bottomless reserve of passion. But this coach was saying that really successful people feel the same lack of motivation as everyone else. The difference is that they still find a way to show up despite the feelings of boredom.

> The greatest threat to success is not failure but boredom. We get bored with habits because they stop delighting us.

p235

> Variable rewards or not, no habit will stay interesting forever. At some point, everyone faces the same challenge on the journey of self-improvement: you have to fall in love with boredom.

p236

> But stepping up when it's annoying or painful or draining to do so, that's what makes the difference between a professional and an amateur.

> Professionals stick to the schedule; amateurs let life get in the way. Professionals know what is important to them and work toward it with purpose; amateurs get pulled off course by the urgencies of life.

p239

> But then, as a habit becomes automatic, you become less sensitive to feedback. You fall into mindless repetition. It becomes easier to let mistakes slide. When you can do it "good enough" on autopilot, you stop thinking about how to do it better.

> The upside of habits is that we can do things without thinking. The downside of habits is that you get used to doing things a certain way and stop paying attention to little errors.

p240

> You can't repeat the same things blindly and expect to become exceptional. Habits are necessary, but not sufficient for mastery. What you need is a combination of automatic habits and deliberate practice. Habits + Deliberate Practice = Mastery

p245

> Personally, I employ two primary modes of reflection and review. Each December, I perform an Annual Review, in which I reflect on the previous year.

p246

> Six months later, when summer rolls around, I conduct an Integrity Report. Like everyone, I make a lot of mistakes. My Integrity Report helps me realise where I went wrong and motivates me to get back on course.

# E

## Ralph Waldo Emerson

> The essence of greatness is the perception that virtue is enough.

## Thomas Edison

> Results? Why, I have gotten lots of results! I know several thousand things that won't work.

# H

## Ryan Holiday

### Ego is the enemy

p3

> Especially for successful people who can’t see what ego prevents them from doing because all they can see is what they’ve already done.

p5

> We assume the symptoms of success are the same as success itself - and in our naivety, confuse the by-product with the cause.

p28

> The only relationship between work and chatter is that one kills the other.
> Let the others slap each other on the back while you’re back in the lab or the gym or pounding the pavement. Plug that hole - that one, right in the middle of your face - that can drain you of your vital life force. Watch what happens. Watch how much better you get.

p31

> “To be or to do? Which way will you go?”

p32

> Appearances are deceiving. *Having* authority is not the same as *being* an authority. *Having* the right and *being* right are not the same either. Being promoted doesn’t necessarily mean you’re doing good work and it doesn’t mean you are worthy of promotion (they call it failing upwards in bureaucracies). *Impressing people is utterly different from being truly impressive*.

p34

> Harder because each opportunity - no matter how gratifying or rewarding - must be evaluated along strict guidelines: Does this help me do what I have set out to do? Does this *allow* me to do what I need to do? Am I being selfish or self*less*?

p48

> Passion is seen in those who can tell you in great detail who they intend to become and what their success will be like - they might even be able to tell you specifically when they intend to achieve it or describe to you legitimate and sincere worries they have about the burdens of such accomplishments. They can tell you all the things they’re going to do, or have begun, but they cannot show you their progress. Because there rarely is any.
> How can someone be busy and not accomplish anything? Well, that’s the passion paradox.
> If the definition of insanity is trying the same thing over and over and expecting different results, then passion is a form of mental retardation - deliberately blunting our most critical cognitive functions. The waste is often appalling in retrospect; the best years of your life burned out like a pair of spinning tires against the asphalt.

p49

> What humans require in our ascent is purpose and realism. Purpose, you could say, is like passion with boundaries. Realism is detachment and perspective.

p53

> When you are just starting out, we can be sure of a few fundamental realities: 1) You’re not nearly as good or as important as you think you are; 2) You have an attitude that needs to be readjusted; 3) Most of what you think you know or most of what you learned in books or in school is out of date or wrong.

p58

> Because if you pick up this mantle once, you’ll see what most people’s ego prevent them from appreciating: the person who clears the path ultimately controls its direction, just as the canvas shapes the painting.

p75

> Pride is a masterful encroacher. John D. Rockefeller, as a young man, practiced a nightly conversation with himself. “Because you have got a start,” he’d say aloud or in his diary, “you think you are quite a merchant; look out or you will lose your head - go steady.”

p80

> Is it ten thousand hours or twenty thousand hours to mastery? The answer is that it doesn’t matter. There is no end zone. To think of a number is to live in a conditional future. We’re simply talking about a lot of hours - that to get where we want to go isn’t about brilliance, but continual effort.

p83

> Work is finding yourself alone at the track when the weather kept everyone else indoors. Work is pushing through the pain and crappy first drafts and prototypes. It is ignoring whatever plaudits others are getting, and more importantly, ignoring whatever plaudits *you* may be getting.

p104

> With accomplishment comes a growing pressure to pretend that we know more than we do. To pretend we already know everything. *Scientia infla* (knowledge puffs up). That’s the worry and the risk - thinking that we’re set and secure, when in reality understanding and mastery is a fluid, continual process.
>
> Too often, convinced of our own intelligence, we stay in a comfort zone that ensures that we never feel stupid (and are never challenged to learn or reconsider what we know).

p110

> Here’s the other part: once you win, everyone is gunning for you. It’s during your moment at the top that you can afford ego the least - because the stakes are so much higher, the margins for error are so much smaller. If anything, your ability to listen, to hear feedback, to improve and grow matter more now than ever before.

p113

> The same goes for us, whatever we do. Instead of pretending that we are living some great story, we must remain focused on the execution - and on executing with excellence. We must shun the false crown and continue working on what got us here.
> Because that’s the only thing that will keep us here.

p117

> Let’s be clear: competitiveness is an important force in life. It’s what drives the market and is behind some of mankind’s most impressive accomplishments. On an individual level, however, it’s absolutely critical that you know *who* you’re competing with and *why*, that you have a clear sense of the space you’re in.
>
> Far too often, we look at other people and make their approval the standard we feel compelled to meet, and as a result, squander our very potential and purpose.

p130

> As you become successful in your own field, your responsibilities may begin to change. Days become less and less about *doing* and more and more about making decisions. Such is the nature of leadership. The transition requires reevaluating and updating your identity. It requires a certain humility to put aside some of the more enjoyable or satisfying parts of your previous job. It means accepting that others might be more qualified or specialised in areas in which you consider yourself competent - or at least their time is better spent on them than yours.

p137

> When it comes to Marshall, the old idea that selflessness and integrity could be weaknesses or hold someone back are laughably disproven. Sure, some people might have trouble telling you much about him - but each and every one of them lives in a world he was largely responsible for shaping.
> The credit? Who cares.

p139

> Nothing draws us away from those questions like material success - when we are always busy, stressed, put upon, distracted, reported to, relied on, apart from. When we’re wealthy and told that we’re important or powerful. Ego tells us that meaning comes from activity, that being the centre of attention is the only way to matter.

p151

> We can use the golden mean to navigate our ego and our desire to achieve.
> Endless ambition is easy; anyone can put their foot down hard on the gas. Complacency is easy too; it’s just a matter of taking the foot *off* the gas.

p165

> Ego loves this notion, the idea that something is “fair” or not. Psychologists call it narcissistic injury when we take personally totally indifferent and objective events. We do that when our sense of self is fragile and dependent on life going our way all the time. Whether what you’re going through is your fault or your problem doesn’t matter, because it’s yours to deal with right now.

p178

> We have only minimal control over the rewards for our work and effort - other people’s validation, recognition, rewards. So what are we going to do? Not be kind, not work hard, not produce, because there is a chance it wouldn’t be reciprocated? C’mon.
>
> It’s far better when doing good work is sufficient. In other words, the less attached we are to *outcomes* the better. When fulfilling our *own* standards is what fills us with pride and self-respect. When the effort - not the results, good or bad - is enough.

p180

> How do you carry on then? How do you take pride in yourself and your work? John Wooden’s advice to his players says it: Change the definition of success. “Success is peace of mind, which is a direct result of self-satisfaction in knowing you made the *effort* to do your best to become the best that you are capable of becoming.” “Ambition,” Marcus Aurelius reminded himself, “means tying your well-being to what other people say or do…Sanity means tying it to your own actions.”

p189

> The problem is that when we get our identity tied up in our work, we worry that any kind of failure will then say something bas about us *as a person*.

p192

> Shit happens and, as they say, sometimes shit happens *in public*. It’s not fun. The questions remain: Are you going to make it worse? Or are you going to emerge from this with your dignity and character intact? Are you going to live to fight another day?

p194

> The only real failure is abandoning your principles. Killing what you love because you can’t bear to part from it is selfish and stupid. If your reputation can’t absorb a few blows, it wasn’t worth anything in the first place.

p197

> This is characteristic of how great people think. It’s not that they find failure in every success. They just hold themselves to a standard that exceeds what society might consider to be objective success. Because of that, they don’t much care what other people think; they care whether they meet their own standards. And these standards are much, much higher than everyone else’s.

p206

> In failure or adversity, it’s so easy to hate. Hate defers blame. It makes someone else responsible. It’s a distraction too; we don’t do much else when we’re busy getting revenge or investigating the wrongs that have supposedly been done to us.

p209

> As Harold Geneen put it, “People learn from failures. Seldom do they learn anything from success.” It’s why the old Celtic saying tells us, “See much, study much, suffer much, that is the path to wisdom.”

p212

> My friend the philosopher and martial artist Daniele Bolelli once gave me a helpful metaphor. He explained that training was like sweeping the floor. Just because we’ve done it once, doesn’t mean the floor is clean forever. Every day the dust comes back. Every day we must sweep.

### Stillness is the Key

p14
> Our job is not to "go with our gut" or fixate on the first impression we form about an issue. No, we need to be strong enough to resist thinking that is too neat, too plausible, and therefore almost always wrong. Because if the leader can't take the time to develop a clear sense of the bigger picture, who will? If the leader isn't thinking through all the way to the end, who is?

p29
> Be present.  
> And if you've had trouble with this in the past? That's okay.
> That's the nice thing about the present. It keeps showing up to give you a second chance.

p31
> There is way too much coming at us. In order to think clearly, it is essential that each of us figures out how to filter out the inconsequential from the essential. It's not enough to be inclined toward deep thought and sober analysis; a leader must create time and space for it.

p34
> [Eisenhower's way of prioritising] ... a mile deep on what matters rather than an inch on too many things.

p46
> Appearances are misleading. First impressions are too. We are disturbed and deceived by what's on the surface, by what others see. Then we make bad decisions, miss opportunities, or feel scared or upset. Particularly when we don't slow down and take the time to really look.

> Epictetus talked about how the job of a philosopher is to take our impressions - what we see, hear, and think - and put them to the test. He said we need to hold up our thoughts and examine them, to make sure we weren't being led astray by appearances or missing  what couldn't be seen by the naked eye.

p47
> The world is like muddy water. To see through it, we have to let things settle. We can't be disturbed by initial appearances, and if we are patient and still, the truth will be revealed to us.

p49
> So much of the distress we feel comes from acting instinctually instead of acting with conscientious deliberation. So much of what we get wrong comes from the same place. We're reacting to shadows. We're taking as certainties impressions we have yet to test. We're not stopping to put on our glasses and really *look*.

p60
> We were given two ears and only one mouth for a reason, the philosopher Zeno observed. What you'll notice when you stop to listen can make all the difference in the world.

p62
> Each of us needs to cultivate those moments [alone time to think] in our lives. Where we limit our inputs and turn down the volume so that we can access a deeper awareness of what's going on around us. In shutting up - even if only for a short period - we can finally hear what the world has been trying to tell us. Or what we've been trying to tell ourselves.

> That quiet is so rare is a sign of its value. Seize it.

> The ticking of the hands of your watch is telling you how time is passing away, never to return. Listen to it.

p70
> Of course, this insecurity ["imposter syndrome"] exists almost entirely in our heads. People aren't thinking about you. They have their own problems to worry about!

p72
> Confident people know what matters. They know when to ignore other people's opinions. They don't boast or lie to get ahead (and then struggle to deliver). Confidence is the freedom to set your own standards and unshackle yourself from the need to prove yourself.

p94
> Mental stillness will be short-lived if our hearts are on fire, or our souls ache with emptiness. We are incapable of seeing what is essential in the world if we are blind to what's going on within us. We cannot be in harmony with anyone or anything if the need for more, more, more is gnawing at our insides like a maggot.

p96
> Since ancient times, people have strived to train and control the forces that reside deep inside them so that they can find serenity, so that they can preserve and protect their accomplishments. What good is it to be rational at work if our personal lives are a hot-blooded series of disasters? How long can we keep the two domains separate anyway? You might rule cities or a great empire, but if you're not in control of yourself, it is all for naught.

p98
> Virtue, the Stoics believed, was the highest good - the *summum bonum* - and should be the principle behind all our actions. Virtue is not holiness, but rather moral and civic excellence in the course of daily life. It's a sense of pure rightness that emerges from our souls and is made real through the actions we take.

p99
> If the concept of "virtue" seems a bit stuffy to you, consider the evidence that a virtuous life is worthwhile for *its own sake*. No one has less serenity than the person who does not know what is right or wrong. No one is more exhausted than the person who, because they lack a moral code, must belabour every decision and consider every temptation. No one feels worse about themselves than the cheater or the liar, even if - often especially if - they are showered with rewards for their cheating and lying. Life *is* meaningless to the person who decides their choices have no meaning.

p100
> Different situations naturally call for different virtues and different epithets for the self. When we're going into a tough assignment, we can say to ourselves over and over again, "Strength and courage". Before a tough conversation with a significant other: "Patience and kindness". In times of corruption and evil: "Goodness and honesty".

> [We can choose what standards to hold ourselves too] Which is why each of us needs to sit down and examine ourselves. What do we stand for? What do we believe to be essential and important? What are we really *living* for? Deep in the marrow of our bones, in the chamber of our heart, we know the answer. The problem is that the busyness of life, the realities of pursuing a career and surviving in the world, come between us and that self-knowledge.

p103
> Virtue, on the other hand, as crazy as it might seem, is a far more attainable and sustainable way to succeed.  
> How's that? Recognition is dependent on other people. Getting rich requires business opportunities. You can be block from your goals by the weather just as easily as you can by a dictator. But virtue? No one can stop you from knowing what's right. Nothing stands between you and it...but yourself.

> Each of us must cultivate a moral code, a higher standard that we love almost more than life itself. Each of us must sit down and ask: *What's important to me? What would I rather die for than betray? How am I going to live and why?*

p114
> A person enslaved to their urges is not free - whether they are a plumber or a president.

p118
> To have an impulse and to resist it, to sit with it and examine it, to let it pass by like a bad smell - this is how we develop spiritual strength. This is how we become who we want to be in this world.

p121
> Most people *never* learn that their accomplishments will ultimately fail to provide the relief and happiness we tell ourselves they will. Or they come to understand this only after so much time and money, so many relationships and moments of inner peace, were sacrificed on the altar of achievement. We get to the finish line only to think: *This is it? Now what?*

p124
> You will never feel okay by way of external accomplishments. *Enough* comes from the inside. It comes from stepping off the train. From seeing what you already have, what you've always had.

> We work so hard "for our families" that we don't notice the contradiction - that it's because of work that we never see them.

p137
> If we told a Zen Buddhist from Japan in the twelfth century that in the future everyone could count of greater wealth and longer lives but that in most cases those gifts would be followed by a feeling of utter purposeless and dissatisfaction, do you think they would want to trade places with us?

p140
> Epicurus was right - if God exists, why would they possibly want you to be afraid of them? And why would they care what clothes you wear or how many times you pay obeisance to them per day? What interest would they have in monuments or in fearful pleas for forgiveness? At the purest level, the only thing that matters to any mother or father - or any creator - is that their children find peace, find meaning, find purpose. They certainly did not put us on this planet so we could judge, control, or kill each other.

p141
> We have so little control of the world around us, so many inexplicable events created this world, that it works out almost exactly the same way as if there was a god.

p144
> Anyone can be rich or famous. Only you can be *Dad* or *Mum* or *Daughter* or *Son* or *Soul Mate* to the people in your life.

p148
> The notion that isolation, that total self-driven focus, will get you to a supreme state of enlightenment is not only incorrect, it misses the obvious: Who will even care if you did all that?

p188
> You don't solve a maze by rushing through. You have to stop and think.You have to walk slowly and carefully, reigning in your energy - otherwise you'll get hopelessly lost.The same is true for the problems we face in life.

p189
> Each of us needs to get better at saying no. As in, "No, sorry, I'm not available." "No, sorry, that sounds great but I'd rather not." "No, I'm going to wait and see." "No, I don't like that idea." "No, I don't need that - I'm going to make the most of what I have." "No, because if I said yes to you, I'd have to say yes to everyone else."

p190
> A pilot gets to say, "Sorry, I'm on standby," as an excuse to get out of things.Doctors and firemen and police officers get to use being "on call" as a shield. But are we not on call in our own lives? Isn't there something (or someone) that we're preserving our full capacities for? Are our own bodies not on call for our families, for our self-improvement, for our own work?

> Always think about what you're really being asked to give. Because the answer is often *a piece of your life*, usually in exchange for something you don't even want. Remember, that's what time is. It's your life, it's your flesh and blood, that you can never get back.

> In every situation ask:
>
> What is it?
> Why does it matter?
> Do I need it?
> Do I want it?
> What are the hidden costs?
> Will I look back from the distant future and be glad I did it?
> If I never knew about it at all - if the request was lost in the mail, if they hadn't been able to pin me down and ask me - would I even notice that I missed out?

p191
> When we know what to say no to, we can say yes to the things that matter.

p196
> [Taking a walk] This isn't about burning calories or getting your heart rate up. On the contrary, it's not about anything. It is instead just a manifestation, an embodiment of the concepts of presence, of detachment, of emptying the mind, of noticing and appreciating the beauty of the world around you.Walk away from the thoughts that need to be walked away from; walk towards the ones that have now appeared.

p201
> [Building a routine] Ah, but the greats know that complete freedom is a nightmare. They know that order is a prerequisite of excellence and that in an unpredictable world, good habits are a safe haven of certainty.

> It was Eisenhower who defined freedom as the opportunity for self-discipline. In fact, freedom and power and success *require* self-discipline. Because without it, chaos and complacency move in. Discipline, then, is how we maintain that freedom.

p205
> [Building a routine] Limit the number of choices you need to make.

> A master is in control. A master has a system. A master turns the ordinary into the sacred.  
> And so must we.

p207
> In short, mental and spiritual independence matter little if the things we own in the physical world end up owning us.

p208
> We don't need to get rid of *all* our possessions, but we should constantly question what we own, why we own it, and whether we could do without.

p215
> People don't have enough silence in their lives because they don't have enough solitude. And they don't get enough solitude because they don't seek out and cultivate silence. It's a vicious cycle that prevents stillness and reflection, and then stymies good ideas, which are almost always hatched in solitude.

> Breakthroughs seem to happen with stunning regularity in the shower or on a long hike. Where don't they happen? Shouting to be heard in a bar. Three hours into a television binge. Nobody realises just how much they love someone while they're booking back-to-back-to-back meetings.

p224
> Yes, there is purity and meaning in giving your best to whatever you do - but life is much more of a marathon than it is a sprint.In a way, this is the distinction between confidence and ego.Can you trust yourself and your abilities enough to keep something in reserve? Can you protect the stillness and the inner peace necessary to win the longer race of life?

p225
> Good decisions are not made by those who are running on empty. What kind of interior life can you have, what kind of thinking can you do, when you are utterly and completely overworked? It's a vicious cycle: We end up having to work more to fix the errors we made when we would have been better off resting, having consciously said no instead of reflexively saying yes.

p236
> In Greek, "leisure" is rendered as *scholé* - that is, *school*. Leisure historically meant simply freedom from the work needed to survive, freedom *for* intellectual or creative pursuits. It was learning and study and the pursuit of higher things.

p249
> High-minded thoughts and inner work are one thing, but all that matters is what you *do*. The health of our spiritual ideas depends on what we do with our bodies in moments of truth.

p250
> Virtue is not an abstract notion. We are not clearing our minds and separating the essential from the inessential for the purposes of a parlour trick. Nor are we improving ourselves so that we can get richer or more powerful.  
> We are doing it to live better and *be* better.  
> Every person we meet and every situation we find ourselves in is an opportunity to prove that.

# J

## William James

> The more of the details of our daily life we can hand over to the effortless custody of automatism, the more our higher powers of mind will be set free for their own proper work. There is no more miserable human being that one in whom nothing is habitual but indecision, and for whom the lighting of every cigar, the drinking of every cup, the time of rising and going to bed every day, and the beginning of every bit of work, are subjects of express volitional deliberation.

# N

## Cal Newport

### Deep Work

p5

> [Quoting Neal Stephenson] "If I organize my life in such a way that I get lots of long, consecutive, uninterrupted time-chunks, I can write novels. [If I instead get interrupted a lot] what replaces it? Instead of a novel that will be around for a long time...there is a bunch of e-mail messages that I have sent out to individual persons."

p6

> The average knowledge worker now spends more than 60 percent of the workweek engaged in electronic communication and Internet searching, with close to 30 percent of a worker's time dedicated to reading and answering e-mail alone.

p13

> To remian valuable in our economy, therefore, you must master the art of quickly learning complicated things. This task requires deep work. If you don't cultivate this ability, you're likely to fall behind as technology advances.

p14

> **The Deep Work Hypothesis:** The ability to perform deep work is becoming increasingly *rare* at exactly the same time it is becoming increasingly *valuable* in our economy. As a consequence, the few who cultivate this skill, and then make it the core of their working life, will thrive.

p29

> Two core abilities for thriving in the new economy
>
> 1. The ability to quickly master hard things.
> 2. The ability to produce at an elite level, in terms of both quality and speed.

p37

> To learn hard things quickly, you must focus intensely without distraction. To learn, in other words, is an act of deep work. If you're comfortable going deep, you'll be comfortable mastering the increasingly complex systems and skills needed to thrive in our economy. If you instead remain one of the many for whom depth is uncomfortable and distraction ubiquitous, you shouldn't expect these systems and skills to come easily to you.

p40

> By maximising his intensity when he works, he maximizes the results he produces per unit of time spent working.

p42

> [Talking about multitasking] The problem this research identifies with this work strategy is that when you switch from some Task A to another Task B, your attention doesn't immediately follow - a *residue* of your attention remains stuck thinking about the original task. This residue gets especially thick if your work on Task A was unbounded and of low intensity before you switched, but even if you finish Task A before moving on, your attention remains divided for a while.

p44

> To produce at your peak level you need to work for extended periods with full concentration on a single task free from distraction.

p53

> Big trends in business today actively decrease people's ability to perform deep work, even though the benefits promised by these trends (e.g., increased serendipity faster responses to requests, and more exposure) are arguably dwarfed by the benefits that flow from a commitment to deep work (e.g., the ability to learn hard things fast and produce at an elite level).

p58

> **The Principle of Least Resistance:** In a business setting, without clear feedback on the impact of various behaviours to the bottom line, we will tend toward behaviours that are easiest in the moment.

p59

> The second reason that a culture of connectivity makes life easier is that it creates an environment where it becomes acceptable to run your day out of your inbox - responding to the latest missive with alacrity while others pile up behind it, all the while feeling satisfyingly productive.

p60

> Also consider the frustratingly common practice of forwarding an e-mail to one or more colleagues, labeled with a short open-ended interrogative, such as: "Thoughts?" These e-mails take the sender only a handful of seconds to write but can command many minutes (if not hours, in some cases) of time and attention from their recipients to work towards a coherent response. A little more care in crafting the message by the sender could reduce the overall time spent by all parties by a significant fraction. So why are these easily avoidable and time-sucking e-mails so common? From the sender's perspective, *they're easier*. It's a way to clear something out of their inbox - at least, temporarily - with a minimum amount of energy invested.

p64

> **Busyness as Proxy for Productivity:** In the absence of clear indicators of what it means to be productive and valuable in their jobs, many knowledge workers turn back toward an industrial indicator of productivity: doing lots of stuff in a visible manner.

p67

> Writing in the early 1990s, as the personal computer revolution first accelerated, Postman argued that our society was sliding into a troubling relationship with technology. We were, he noted, no longer discussing the trade-offs surrounding new technologies, balancing the new efficiencies against the new problems introduced. If it's high-tech, we began to instead assume, then it's good. Case closed.

p70

> In such a culture [idolising all things Internet], we should not be surprised that deep work struggles to compete against the shiny thrum of tweets, likes, tagged photos, walls, posts, and all the other behaviours that we're now taught are necessary for no other reason than that they exist.

> The myopia of your peers and employers uncovers a great personal advantage. Assuming the trends outlined here continue, depth will become increasingly rare and therefore increasingly valuable.

p75

> Depth-destroying behaviours such as immediate e-mail responses and an active social media presence are lauded, while avoidance of these trends generates suspicion.

p77

> Our brains instead construct our worldview based on *what we pay attention to*. If you focus on a cancer diagnosis, you and your life become unhappy and dark, but if you focus instead on an evening martini, you and your life become more pleasant - even though the circumstances in both scenarios are the same. As Gallagher summarises: "Who you are, what you think, feel, and do, what you love - is the sum of what you focus on."

p82

> A workday driven by the shallow, from a neurological perspective, is likely to be a draining and upsetting day, even if most of the shallow things that capture your attention seem harmless or fun.

p90

> Whether you're a writer, marketer, consultant, or lawyer: Your work is craft, and if you hone your ability and apply it with respect and care, then like the skilled wheelwright you can generate meaning in the daily efforts of your professional life.

p91

> Put another way, a wooden wheel is not noble, but its shaping can be. The same applies to knowledge work. You don't need a rarified job; you need instead a rarified approach to your work.

p105

> Stephenson sees two mutually exclusive options: He can write good novels at a regular rate, or he can answer a lot of individual e-mails and attend conferences, and as a result produce lower-quality novels at a slower rate.

p108

> Jung's approach is what I call the *bimodal philosophy* of deep work. This philosophy asks that you divide your time, dedicating some clearly defined stretches to deep pursuits and leaving the rest open to everything else.

p110

> As Jung, Grant, and Perlow's subjects discovered, people will usually respect your right to become inaccessible if these periods are well defined and well advertised, and outside these stretches, you're once again easy to find.

p111

> ... the *rhythmic philosophy*. This philosophy argues that the easiest way to consistently start deep work sessions is to transform them into a simple regular habit. The goal, in other words, is to generate a *rhythm* for this work that removes the need for you to invest energy in deciding if an when you're going to go deep.

p115

> Isaacson was methodic: Any time he could find some free time, he would switch into a deep work mode and hammer away at his book. This is how, it turns out, one can write a nine-hundred-page book on the side while spending the bulk of one's day becoming one of the country's best magazine writers.  
> I call this approach, in which you fit deep work wherever you can into your schedule, the *journalist philosophy*.

p119

> To make the most out of your deep work sessions, build rituals of the same level of strictness and idiosyncrasy as the important thinkers mentioned previosuly.

> **Where you'll work and for how long.** Your ritual needs to specify a location for your deep work efforts.

p120

> **How you'll support your work.** For example, the ritual might specify that you start with a cup of good coffee...

p122

> *the grand gesture*. The concept is simple: By leveraging a radical change to your normal environment, coupled perhaps with a significant investment of effort or money, all dedicated toward supporting a deep work task, you increase the perceived importance of the task. This boost in importance reduces your mind's instinct to procrastinate and delivers an injection of motivation and energy.

p134

> By working side-by-side with someone on a problem, you can push each other toward deeper levels of depth, and therefore toward the generation of more and more valuable output as compared to working alone.

p137

> For an individual focused on deep work, the implication is that you should identify a small number of ambitious outcomes to pursue with your deep work hours. The general exhortation to "spend more time working deeply" doesn't spark a lot of enthusiasm. To instead have a specific goal that would return tangible and substantial professional benefits will generate a steadier stream of enthusiasm.

p139

> In the preceding discipline, I argued that for an individual focused on deep work, hours spent working deeply should be the lead measure. It follows, therefore, that the individual's scoreboard should be a physical artifact in the workspace that displays the individual's current deep work hour account.

p141

> During my experiments with 4DX [The four disciplines of execution], I used a weekly review to look over my scoreboard to celebrate good weeks, help understand what led to bad weeks, and most important, figure out how to ensure a good score for the days ahead. This led me to adjust my schedule to meet the needs of my lead measure - enabling significantly more deep work that if I had avoided such reviews altogether.

p143

> [quoting from Tim Kreider] Idleness is not just a vacation, an indulgence or a vice; it is as indispensable to the brain as vitamin D is to the body, and deprived of it we suffer a mental affliction as disfiguring as rickets...it is, paradoxically, necessary to getting any work done.

p144

> At the end of the workday, shut down your consideration of work issues until the next morning - no after-dinner e-mail check, no mental replays of conversations, and no scheming about how you'll handle an upcoming challenge; shut down work thinking completely. If you need more time, then extend your workday, but once you shut down, your mind must be left free...

p148

> Put another way, when walking through nature, you're freed from having to direct your attention, as there are few challenges to navigation (like crowded street crossings), and experience enough interesting stimuli to keep your mind sufficiently occupied to avoid the need to actively aim your attention.

p149

> Only the confidence that you're done with work until the next day can convince your brain to downshift to the level where it can begin to recharge for the next day to follow. Put another way, trying to squeeze a little more work out of your evenings might reduce your effectiveness the next day enough that you end up getting *less* done than if you had instead respected a shutdown.

p151

> Another key commitment for suceeding with this strategy is to support your commitment to shutting down with a strict *shutdown ritual* that you use at the end of the workday to maximise the probability that you suceed. In more detail, this ritual should ensure that every incomplete task, goal, or project has been reviewed and that for each you have confirmed either (1) you have a plan you trust for its completion, or (2) it's captured in a place where it will be revisited when the time is right. The process should be an algorithm: a series of steps you always conduct, one after another. When you're done, have a set phrase you say that indicates completion (to end my own ritual, I say, "Shutdown complete"). This final step sounds cheesy, but it provides a simple cue to your mind that it's safe to release work-related thoughts for the rest of the day.

p159

> If every moment of potential boredom in your life - say, having to wait five minutes in line or sit alone in a restaurant until a friend arrives - is relieved wit a quick glance at your smartphone, then you brain has likely been rewired to a point where, like the "mental wrecks" in Nass's research, it's not ready for deep work - even if you regularly schedule time to practice this concentration.

p171

> In my experience, productive meditation builds on both of the key ideas introduced at the beginning of this rule. By forcing you to resist distraction and return your attention repeatedly to a well-defined problem, it helps strengthen your distraction-resisting muscles, and by forcing you to push your focus deeper and deeper on a single problem, it sharpens your concentration.

p220

> Shallow work, therefore, doesn't become dangerous until after you add enough to begin to crowd out your bounded deep efforts for the day.

p223

> Here's my suggestion: At the beginning of each workday, turn to a new page of lined paper in a notebook you dedicate to this purpose. Down the left-hand side of the page, mark every other line with an hour of the day, covering the full set of hours you typically work. Now comes the important part: Divide the hours of your workday into *blocks* and assign activities to the blocks.

p229

> The purpose of this strategy is to give you an accurate metric for resolving such ambiguity - providing you with a way to make clear and consistent decisions about where given work tasks fall of the shallow-to-deep scale. To do so, it asks that you evaluate activities by asking a simple (but surprisingly illuminating) question:
> *How long would it take (in months) to train a smart recent college graduate with no specialised training in my field to complete this task?*

p236

> I call this commitment *fixed-schedule productivity*, as I fix the firm goal of not working past a certain time, then work backwards to find productivity strategies that allow me to satisfy this declaration. I've practiced fixed-schedule productivity happily for more than half a decade now, and it's been crucial to my efforts to build a productive professional life centred on deep work.

p239

> I, too, am incredibly cautious about my use of the most dangerous word in one's productivity vocabulary: "yes". It takes a lot to convince me to agree to something that yields shallow work.

p241

> A commitment to fixed-schedule productivity, however, shifts you into a scarcity mind-set. Suddenly any obligation beyond your deepest efforts is suspect and seen as potentially disruptive. Your default answer becomes no, the bar for gaining access to your time and attention rises precipitously, and you begin to organise the efforts that pass these obstacles with a ruthless efficiency. It might also lead you to test assumptions about your company's work culture that you thought were ironclad but turn out to be malleable. It's common, for example, to receive e-mails from your boss after hours. Fixed-schedule productivity would have you ignore these messages until the next morning. Many suspect that this would cause problems, such as responses are *expected*, but in many cases, the fact that your boss happens to be clearing her inbox at night doesn't mean that she expects an immediate response - a lesson this strategy would soon help you discover.

p249

> In particular, interrogative e-mails like these generate an initial instinct to dash off the quickest possible response that will clear the message - temporarily - out of your inbox. A quick response will, in the short term, provide you with some minor relief because you're bouncing the responsibility implied by the message off your court and back onto the sender's. This relief, however, is short-lived, as this responsibility will continue to bounce back again and again, continually sapping your time and attention. I suggest, therefore, that the right strategy when faced with a question of this type is to pause a moment before replying and take the time to answer the following key prompt:
> *What is the project represented by this message, and what is the most efficient (in terms of messages generated) process for bringing this project to a successful conclusion?*

p252

> Process-centric e-mails might not seem natural at first. For one thing, they require that you spend more time thinking about your messages before you compose them. In the moment, this might seem like you're spending *more* time on e-mail. But the important point to remember is that the extra two to three minutes you spend at this point will save you many more minutes reading and responding to unnecessary extra messages later.

### Digital Minimalism

pXIV

> I've become convinced that what you need instead is a full-fledged philosophy of technology use, rooted in your deep values, that provides clear answers to the questions of what tools you should use and how you should use them and, equally important, enables you to confidently ignore everything else.

pXVIII

> ...a philosophy that prioritizes long-term meaning over short-term satisfaction.

p6

> the average modern user would spend around two hours per day on social media and related messaging services, with close to half that time dedicated to Facebook's products alone.

p11

> "Philip Morris just wanted your lungs," Maher concludes. "The App Store wants your soul."

p13

> These apps and slick sites were not, as Bill Maher put it, gifts from "nerd gods building a better world." They were, instead, designed to put slot machines in our pockets.

p18

> Alter goes on to describe users as "gambling" every time they post something on a social media platform: Will you get likes (or hearts or retweets), or will it languish with no feedback?

p20

> In other words, there's nothing fundamental about the unpredictable feedback that dominates most social media services. If you took these features away, you probably wouldn't diminish the value most people derive from them.

p27

> [turning off notifications] I'm always skeptical about these quick-fix tales. In my experience covering these topics, it's hard to permanently reform your digital life through the use of tips and tricks alone.

p28

> To reestablish control, we need to move beyond tweaks and instead rebuild our relationship with technology from scratch, using our deeply held values as a foundation.

> what all of us who struggle with these issues need - is a philosophy of technology use, something that covers from the ground up which digital tools we allow into our life, for what reasons, and under what constraints.

> **Digital Minimalism** A philosophy of technology use in which you focus your online time on a small number of carefully selected an optimised activities that strongly support things you value, and then happily miss out on everything else.

p29

> Even when a new technology promises to support something the minimalist values, it must still pass a stricter test: Is this the best way to use technology to support this value? If the answer is no, the minimalist will set to work trying to optimise the tech, or search out a better option.

p35

> **Principle #1**: Clutter is costly.

p36

> **Principle #2**: Optimisation is important.

> **Principle #3**: Intentionality is satisfying.

p39

> Thoreau establishes early in Walden: "The cost of a thing is the amount of what I will call life which is required to be exchanged for it, immediately or in the long run."

p41

> How much of your time and attention, he would ask, must be sacrificed to earn the small profit of occasional connections and new ideas that is earned by cultivating a significant presence on Twitter?

p42

> This is why clutter is dangerous. It's easy to be seduced by the small amounts of profit offered by the latest app or service, but then forget its cost in terms of the most important resource we possess: the minutes of our life.

p43

> He asks us to treat the minutes of our life as a concrete and valuable substance - arguably the most valuable substance we possess - and to always reckon with how much of this life we trade for the various activities we allow to claim our time.

p47

> Gabriella adopted an optimisation to this process: she's not allowed to watch Netflix alone.

> "Now [streaming shows is] a social activity instead of an isolating activity,"

p48

> By contrast, if you think of these services as offering a collection of features that you can carefully put to use to serve specific values, then almost certainly you'll spend much less time using them.

p49

> Finding useful new technologies is just the first step to improving your life. The real benefits come once you start experimenting with how best to use them.

p51

> The Amish, it turns out, do something that's both shockingly radical and simple in our age of impulsive and complicated consumerism: they start with the things they value most, then work backward to ask whether a given new technology performs more harm than good with respect to these values.

p56

> Part of what makes this philosophy so effective is that the very act of being selective about your tools will bring you satisfaction, typically much more than what is lost from the tools you decide to avoid.

p64

> Once you've identified the class of technologies that are relevant, you must then decide which of them are sufficiently "optional" that you can take a break from them for the full thirty days of the declutter process. My general heuristic is the following: consider the technology optional unless its temporary removal would harm or significantly disrupt the daily operation of your professional or personal life.

p70

> This detox experience is important because it will help you make smarter decisions at the end of the declutter when you reintroduce some of these optional technologies to your life.

p71

> you're more likely to succeed in reducing the role of digital tools in your life if you cultivate high-quality alternatives to the easy distraction they provide. For many people, their compulsive phone use papers over a void created by a lack of a well-developed leisure life.

p75

> for each optional technology that you're considering reintroducing into your life, you must first ask: Does this technology directly support something that I deeply value?

p76

> Once a technology passes the first screening question, it must then face a more difficult standard: Is this technology the best way to support this value?

> If a technology makes it through both of these screening questions, there's one last question you must ask yourself before it's allowed back into your life: How am I going to use this technology going forward to maximise its value and minimise its harms?

p77

> **The Minimalist Technology Screen**: To allow an optional technology back into your life at the end of the digital declutter, it must: Serve something you deeply value (offering some benefit is not enough). Be the best way to use technology to serve this value (if it's not, replace it with something better). Have a role in your life that is constrained with a standard operating procedure that specifies when and how you use it.

p80

> Abby, a Londoner who works in the travel industry, removed the web browser from her phone - a nontrivial hack. "I figured I didn't need to know the answer to everything instantly," she told me. She then bought an old-fashioned notebook to jot down ideas when she's bored on the tube.

p94

> Solitude requires you to move past reacting to information created by other people and focus instead on your own thoughts and experiences - wherever you happen to be.

p97

> To Woolf, in other words, solitude is not a pleasant diversion, but instead a form of liberation from the cognitive oppression that results in its absence.

p101

> The smartphone provided a new technique to banish these remaining slivers of solitude: the quick glance.

p103

> **Solitude deprivation**: A state in which you spend close to zero time alone with your own thoughts and free from input from other minds.

p106

> these shifts in mental health correspond "exactly" to the moment when American smartphone ownership became ubiquitous.

p111

> As Thoreau's example emphasizes, there's nothing wrong with connectivity, but if you don't balance it with regular doses of solitude, its benefits will diminish.

p115

> To live permanently without these devices would be needlessly annoying, but to regularly spend a few hours away from them should give you no pause.

> I recommend that you try to spend some time away from your phone most days. This time could take many forms, from a quick morning errand to a full evening out, depending on your comfort level. Succeeding with this strategy requires that you abandon the belief that not having your phone is a crisis.

p125

> Writing a letter to yourself is an excellent mechanism for generating exactly this type of solitude. It not only frees you from outside inputs but also provides a conceptual scaffolding on which to sort and organise your thinking.

p126

> The key is the act of writing to yourself. This behaviour necessarily shifts you into a state of productive solitude - wrenching you away from the appealing digital baubles and addictive content waiting to distract you, and providing you with a structured way to make sense of whatever important things are happening in your life at the moment. It's a simple practice that's easy to deploy, but it's also incredibly effective.

p148

> In this philosophy, connection is downgraded to a logistical role. This form of interaction now has two goals: to help set up and arrange conversations, or to efficiently transfer practical information (e.g., a meeting location or time for an upcoming event). Connection is no longer an alternative to conversation; it's instead its supporter.

p151

> **PRACTICE**: DON'T CLICK "LIKE"

p153

> To click "Like", within the precise definitions of information theory, is literally the least informative type of nontrivial communication, providing only a minimal one bit of information about the state of the sender (the person clicking the icon on a post) to the receiver.

> Earlier, I cited extensive research that supports the claim that the human brain has evolved to process the flood of information generated by face-to-face interactions. To replace this rich flow with a single bit is the ultimate insult to our social processing machinery.

p155

> The idea that it's valuable to maintain vast numbers of weak-tie social connections is largely an invention of the past decade or so - the detritus of overexuberant network scientists spilling inappropriately into the social sphere. Humans have maintained rich and fulfilling social lives for our entire history without needing the ability to send a few bits of information each month to people we knew briefly during high school.

p159

> To conclude, let's agree on the obvious claim that text messaging is a wonderful innovation that makes many parts of life significantly more convenient. This technology only becomes a problem when you treat it as a reasonable alternative to a real conversation.

p162

> Coffee shop hours are also popular. In this variation, you pick some time each week during which you settle into a table at your favourite coffee shop with the newspaper or a good book. The reading, however, is just the backup activity. You spread the word among people you know that you're always at the shop during these hours with the hope that you soon cultivate a rotating group of regulars that come hang out.

p166

> if your life consists only of actions whose "worth depends on the existence of problems, difficulties, needs, which these activities aim to solve," you're vulnerable to the existential despair that blooms in response to the inevitable question, Is this all there is to life? One solution to this despair, he notes, is to follow Aristotle's lead and embrace pursuits that provide you a "source of inward joy."

p168

> In recent years, as the boundary between work and life blends, jobs become more demanding, and community traditions degrade, more and more people are failing to cultivate the high-quality leisure lives that Aristotle identifies as crucial for human happiness. This leaves a void that would be near unbearable if confronted, but can be ignored with the help of digital noise. It's now easy to fill the gaps between work and caring for your family and sleep by pulling out a smartphone or tablet, and numbing yourself with mindless swiping and tapping.

p169

> When the void is filled, you no longer need distractions to help you avoid it.

p176

> but these arguments all build on the same general principle that the value you receive from a pursuit is often proportional to the energy invested.

p177

> **Leisure Lesson #1**: Prioritise demanding activity over passive consumption.

p180

> In the absence of a well-built wood bench or applause at a musical performance to point toward, you can instead post a photo of your latest visit to a hip restaurant, hoping for likes, or desperately check for retweets of a clever quip. But as Crawford implies, these digital cries for attention are often poor substitute for the recognition generated by handicraft, as they're not backed by the hard-won skill required to tame the "infallible judgement" of physical reality, and come across instead as "the boasts of a boy."

p182

> **Leisure Lesson #2**: Use skills to produce valuable things in the physical world.

p190

> **Leisure Lesson #3**: Seek activities that require real-world, structured social interactions.

p192

> The state I'm helping you escape is one in which passive interaction with your screens is your primary leisure. I want you to replace this with a state where your leisure time is now filled with better pursuits, many of which will exist primarily in the physical world. In this new state, digital technology is still present, but now subordinated to a support role: helping you set up or maintain your leisure activities, but not acting as the primary source of leisure itself.

p193

> Spending an hour browsing funny YouTube clips might sap your vitality, while - and I'm speaking from recent experience here - using YouTube to teach yourself how to replace a motor in a bathroom ventilation fan can provide the foundation for a satisfying afternoon tinkering.

p198

> My suggestion is that you try to learn and apply one new skill every week, over a period of six weeks. Start with easy projects like those suggested above, but as soon as you feel the challenge wane, ramp up the complication of the skills and steps involved.

p202

> I conjecture that the vast majority of regular social media users can receive the vast majority of the value these services provide their life in as little as twenty to forty minutes of use per week.

p207

> Without a well-considered approach to your high-quality leisure, it's easy for your commitment to these pursuits to degrade due to the friction of everyday life.

p219

> try the following experiment. Assuming that you use Facebook, list the most important things it provides you - the particular activities that you would really miss if you were forced to stop using the service altogether. Now imagine that Facebook started charging you by the minute. How much time would you really need to spend in the typical week to keep up with your list of important Facebook activities?

p240

> When an issue catches your attention, in other words, you're usually better served checking in on what the people you respect most think about it than wading into the murk of a Twitter hashtag search or the back-and-forth commenting littering your Facebook timeline.

p241

> As we've known since the time of Socrates, engaging with arguments provides a deep source of satisfaction independent of the actual content of the debate.

p242

> The key to embracing Slow Media is the general commitment to maximising the quality of what you consume and the conditions under which you consume it. If you're serious about joining the attention resistance, you should be serious about these ideas when confronting how you interact with information on the internet.

p244

> These products, which include, notably, a Kickstarter darling called the Light Phone, don't replace your existing smartphone, but instead extend it to a simpler form.

p252

> Digital minimalists see new technologies as tools to be used to support things they deeply value - not as sources of value themselves.

# O

## George Orwell

> In real life, it is the anvil that always breaks the hammer.

# S

## Arthur Schopenhauer

> Sleep is the interest we have to pay on the capital which is called in at death. The higher the interest rate and the more regularly it is paid, the further the date of redemption is postponed.

# Y

## Scott H. Young

### Ultralearning

p7

> Lewis explained his approach: Start speaking the very first day. Don't be afraid to talk to strangers. Use a phrasebook to get started; save formal study for later. Use visual mnemonics to memorize vocabulary. What struck me were not the methods but the boldness with which he applied them. While I had timidly been trying to pick up some French, worrying about saying the wrong things and being embarrassed by my insufficient vocabulary, Lewis was fearless, diving straight into conversations and setting seemingly impossible challenges for himself.

p25

> **ULTRALEARNING**: A strategy for acquiring skills and knowledge that is both self-directed and intense.

p27

> Your deepest moments of happiness don't come from doing easy things; they come from realising your potential and overcoming your own limiting beliefs about yourself. Ultralearning offers a path to master those things that will bring you deep satisfaction and self-confidence.

p38

> Whenever you read about an intensive schedule in this book, feel free to adapt it to your own situation, taking a more leisurely pace while employing the same ruthless efficient tactics.

> The second way is pursuing ultralearning during gaps in work and school.

> The third way is to integrate ultralearning principles into the time and energy you already devote to learning. Think about the last business book you read or the time you tried to pick up Spanish, pottery, or programming. What about that new software you needed to learn for work? Those professional development hours you need to log to maintain your certification? Ultralearning doesn't have to be an additional activity; it can inform the time you already spend learning.

p47

> Ultralearning, in my view, works best when you see it through a simple set of principles, rather than trying to copy and paste exact steps or protocols.

p49

> Beyond principles and tactics is a broader ultralearning ethos. It's one of taking responsibility for your own learning: deciding what you want to learn, how you want to learn it, and crafting your own plan to learn what you need to. You're the one in charge, and you're the one who's ultimately responsible for the results you generate. If you approach ultralearning in that spirit, you should take these principles as flexible guidelines, not as rigid rules. Learning well isn't just about following a set of prescriptions.

p53

> metalearning means learning about learning.

p56

> Metalearning exists in all subjects, but it can often be harder to examine independently from regular learning.

p57

> The more practice you get with it, the more skills and knowledge you'll pick up for how to do it well. This long-term advantage likely outweighs the short-term benefits and is what's easiest to mistake for intelligence or talent when seen in others.

> I find it useful to break down metalearning research that you do for a specific project into three questions: "Why?," "What?," and "How?" "Why?" refers to understanding your motivation to learn.

p58

> The first question to try to answer is why are you learning and what that implies for how you should approach the project. Practically speaking, the projects you take on are going to have one of two broad motivations: instrumental and intrinsic.

> Instrumental learning projects are those you're learning with the purpose of achieving a different, nonlearning result.

> Intrinsic projects are those that you're pursuing for their own sake.

p59

> Determine if learning a topic is likely to have the effect you want it to before you get started.

p61

> Once you've gotten a handle on why you're learning, you can start looking at how the knowledge in your subject is structured. A good way to do this is to write down on a sheet of paper three columns with the headings "Concepts," "Facts," and "Procedures." Then brainstorm all the things you'll need to learn.

p62

> In general, if something needs to be understood, not just memorized, I put it into this column [Concepts] instead of the second column for facts.

> In the second column, write down anything that needs to be memorized.

p63

> Knowing what the bottlenecks will be can help you start to think of ways of making your study time more efficient and effective, as well as avoid tools that probably won't be too helpful to your goal.

p65

> Even if you're eager to start learning right away, investing a few hours now can save you dozens or hundreds later on.

p66

> One question you may face is when to stop doing research and just get started. The literature on self-directed learning, as typically practiced, demonstrates that most people fail to do a thorough investigation of possible learning goals, methods, and resources.

> A good rule of thumb is that you should invest approximately 10 percent of your total expected learning time into research prior to starting.

p75

> Once you can easily and automatically recognise your tendency to procrastinate, when it occurs, you can take steps to resist the impulse. One way is to think in terms of a series of "crutches" or mental tools that can help you get through some of the worst parts of your tendency to procrastinate.

> A first crutch comes from recognising that most of what is unpleasant in a task (if you are averse to it) or what is pleasant about an alternative task (if you're drawn to distraction) is an impulse that doesn't actually last that long. If you actually start working or ignore a potent distractor, it usually only takes a couple minutes until the worry starts to dissolve, even for fairly unpleasant tasks.

> Telling yourself that you need to spend only five minutes on the task before you can stop and do something else is often enough to get you started.

p81

> If I have difficult reading to do, I will often make an effort to jot down notes that reexplain hard concepts for me. I do this mostly because, while I'm writing, I'm less likely to enter into the state of reading hypnosis where I'm pantomiming the act of reading while my mind is actually somewhere else.

p90

> directly learning the thing we want feels too uncomfortable, boring, or frustrating, so we settle for some book, lecture, or app, hoping it will eventually make us better at the real thing.

p92

> Though first covering the material is often essential to begin doing practice, the principle of directness asserts that it's actually while doing the thing you want to get good at when much of learning takes place.

p97

> Why have educational institutions struggled to demonstrate significant transfer, if transfer is something we all need to function in the world? Haskell suggests that a major reason is that transfer tends to be harder when our knowledge is more limited. As we develop more knowledge and skill in an area, they become more flexible and easier to apply outside narrow contexts in which they were learned. However, I'd like to add my own hypothesis as an explanation for the transfer problem: most formal learning is woefully indirect.

p98

> as learners, we must accept that initial learning efforts often stick stubbornly to the situations we learn them in.

p99

> The simplest way to be direct is to learn by doing. Whenever possible, if you can spend a good portion of your learning time just doing the thing you want to get better at, the problem of directness will likely go away.

p101

> **Tactic 1: Project-Based Learning** - Many ultralearners opt for projects rather than classes to learn the skills they need. The rationale is simple: if you organise your learning around producing something, you're guaranteed to at least learn how to produce that thing. If you take classes, you may spend a lot of time taking notes and reading but not achieve your goal.

p102

> **Tactic 2: Immersive Learning** - Immersion is the process of surrounding yourself with the target environment in which the skill is practiced. This has the advantage of requiring much larger amounts of practice that would be typical, as well as exposing you to a fuller range of situations in which the skill applies.

p103

> For example, novice programmers might join open-source projects to expose themselves to new coding challenges.

p108

> When reading an English grammar book, he was exposed to the idea of Socratic method, of challenging another's ideas through probing questions rather than direct contradiction.

p109

> Learning, I'd like to argue, often works similarly, with certain aspects of the learning problem forming a bottleneck that controls the speed at which you can become more proficient overall.

p110

> By identifying a rate-determining step in your learning reaction, you can isolate it and work on it specifically.

p116

> One strategy I've seen repeatedly from ultralearners is to start with a skill that they don't have all the prerequisites for. Then, when they inevitably do poorly, they go back a step, learn one of the foundational topics, and repeat the exercise.

p122

> Testing yourself - trying to retrieve information without looking at the text - clearly outperformed all other conditions.

p123

> The act of trying to summon up knowledge from memory is a powerful learning tool on its own, beyond its connection to direct practice or feedback.

p124

> If a learning task feels easy and smooth, we are more likely to believe we've learned it. If the task feels like a struggle, we'll feel we haven't learned it yet.

> The feeling that you're learning more when you're reading rather than trying to recall with a closed book isn't inaccuracte. The problem comes after. Test again days later, and retrieval practice beats passive review by a mile. What helped in the immediate time after studying turns out not to create the long-term memory needed for actual learning to take place.

> Whether you are ready or not, retrieval practice works better. Especially if you combine retrieval with the ability to look up the answers, retrieval practice is a much better form of studying than the ones most students apply.

p125

> Free recall tests, in which students need to recall as much as they can remember without prompting, tend to result in better retention than cued recall tests, in which students are given hints about what they need to remember. Cued recall tests, in turn, are better than recognition tests, such as multiple-choice answers, where the correct answer needs to be recognised but not generated.

> Difficulty, far from being an obstacle to making retrieval work, may be part of the reason it does so.

p126

> An interesting observation from retrieval research, known as the forward-testing effect, shows that retrieval not only helps enhance what you've learned previously but can even help prepare you to learn better. Regular testing of previously studied information can make it easier to learn new information. This means that retrieval works to enhance future learning, even when there is nothing to retrieve yet!

p127

> An analogy is that trying to retrieve an answer that doesn't yet exist in your mind is like laying down a road leading to a building that hasn't been constructed yet. The destination doesn't exist, but the path to get to where it will be, once constructed, is developed regardless.

p128

> There will always be some things you choose to master and others you satisfy yourself with knowing you can look up if you need to.

p129

> Being able to look things up is certainly an advantage, but without a certain amount of knowledge inside your head, it doesn't help you solve hard problems.

> This may sound abstract. but I'd argue that this is quite common with programmers, and often the thing separating mediocre programmers from great ones isn't the range of problems they can solve but that the latter often know dozens of ways to solve problems and can select the best one for each situation. This kind of breadth requires a certain amount of passive exposure, which in turn benefits from retrieval practice.

p131

> While doing research for this book, for instance, I would often print out journal articles and put them in a binder with a few blank sheets of paper after each of them. After I had finished reading, I'd do a quick free recall exercise to make sure I would retain the important details when it came time for writing.

> Most students take notes by copying the main points as they encounter them. However, another strategy for taking notes is to rephrase what you've recorded as questions to be answered later. Instead of writing that the Magna Carta was signed in 1215, you could instead write the question "When was the Magna Carta signed?" with a reference to where to find the answer in case you forgot.

p132

> One rule I've found helpful for [taking notes from a textbook] is to restrict myself to one question per section of a text, thus forcing myself to acknowledge and rephrase the main point rather than zoom in on a detail that will be largely irrelevant later.

p133

> By preventing yourself from consulting the source, the information becomes knowledge stored inside your head instead of inside a reference manual.

p160

> This curve shows that we tend to forget things incredibly quickly after learning them, there being an exponential decay in knowledge, which is steepest right after learning. However, Ebbinghaus noted, this forgetting tapers off, and the amount of knowledge forgotten lessens over time. Our minds are a leaky bucket; however, most of the holes are near the top, so the water that remains at the bottom leaks out more slowly.

p164

> One of the pieces of studying advice that is best supported by research is that if you care about long-term retention, don't cram. Spreading learning sessions over more intervals over longer periods of time tends to cause somewhat lower performance in the short run but much better performance in the long run.

p165

> However, other programs, such as open-source Anki, are the preferred tool for more extreme ultralearners who want to squeeze out a little more performance.

p174

> I believe that mnemonics, like SRS, are incredibly powerful tools. And as tools, they can open new possibilities for people who are not familiar with them. However, as someone who has spent much time exploring them and applying them to real-world learning, their applications are quite a bit narrower than they first appear, and in many real-world settings they simply aren't worth the hassle.

p182

> Intuition sounds magical, but the reality may be more banal - the product of a large volume of organised experience dealing with the problem.

p185

> **Rule 1: Don't Give Up on Hard Problems Easily**. The process of figuring it out the hard way will increase your fundamental understanding of the problem.

p186

> **Rule 2: Prove Things to Understand Them**

p189

> **Rule 3: Always Start with a Concrete Example**

p190

> The Dunning-Kruger effect occurs when someone with inadequate understanding of a subject nonetheless believes he or she possesses more knowledge about the subject than the people who actually do. This can occur because when you lack knowledge about a subject, you also tend to lack the ability to assess your own abilities. It is true that the more you learn about a subject, the more questions arise. The reverse also seems to be true, in that the fewer questions you ask, the more likely you are to know less about the subject.

p191

> How many of us lack the confidence to ask "dumb" questions? Feynman knew he was smart and had no problem asking them. The irony is that by asking questions with seemingly obvious answers, he also noticed the not-so obvious implications of the things he studied.

> Explaining things clearly and asking "dumb" questions can keep you from fooling yourself into thinking you know something you don't.

p192 (see following pages for great examples of this method in practice)

> The method [The Feynman Technique] is quite simple:  
> 1. Write down the concept or problem you want to understand at the top of a piece of paper.  
> 2. In the space below, explain the idea as if you had to teach it to someone else.  
>    a. If it's a concept, ask yourself how you would convey the idea to someone who has never heard of it before.  
>    b. If it's a problem, explain how to solve it - crucially - why that solution procedure makes sense to you.  
> 3. When you get stuck, meaning your understanding fails to provide a clear answer, go back to your book, notes, teacher, or reference material to find the answer.  
> The crux of this method is that it tries to dispel the illusion of explanatory depth. Since many of our understandings are never articulated, it's easy to think you understand something you don't. The Feynman Technique bypasses this problem by forcing you to articulate the idea you want to understand in detail.

p203

> The difference between a novice programmer isn't usually that the novice cannot solve certain problems. Rather, it's that the master knows the best way to solve a problem, which will be the most efficient and clean and cause the fewest headaches later on. As mastery becomes a process of unlearning over accumulation, experimentation becomes synonymous with learning as you force yourself to go outside your comfort zone and try new things.

p216

> It's usually the first project that requires the most thought and care. A solid, well-researched, well-executed plan can give you the confidence to face harder challenges in the future. A bungled attempt is not a disaster, but it may make you reluctant to pursue future projects of a similar nature.

p217

> **Step 1: Do the research**. The first step in any project is to do the metalearning research required to give you a good starting point.

p219

> If you don't set aside time to learn, it will be a lot harder to summon up the motivation to do so.

> The first decision you should make is how much time you're going to commit.

p220

> I recommend setting a consistent schedule that is the same every week, rather than trying to fit in learning when you can. Consistency breeds good habits, reducing the effort required to study.

> The third decision you need to make is the length of time for your project. I generally prefer shorter commitments to longer ones because they are easier to stick with.

> Finally, take all this information and put it into your calendar. Scheduling all the hours of work on the project in advance has important logistical and psychological benefits.

p221

> If you're unwilling to put time into your calendar, you're almost certainly unwilling to put in time to study.
