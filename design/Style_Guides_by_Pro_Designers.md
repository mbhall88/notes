# Style Guides by Pro Designers – Inspiration Supply – Medium

```
  
```

# Style Guides by Pro Designers – Inspiration Supply – Medium

Recommended by you and

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*vMN0HudGWx0fWPq8gL0NgA.png)

[Emma Drews](https://medium.com/@emmadrews?source=post_header_lockup) Designer from England living in Belgium focusing on web and mobile. Hot chocolate addict ☕️. <https://dribbble.com/emmadrews>
Apr 8·3 min read

# Style Guides by Pro Designers

A selection of UI and brand style guides by talented designers.

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*UZNv2k1v62m5hErkNo8dwQ.png)

Part of the new Style Guide by [Pawel Olek](https://medium.com/@emblemo)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*YA_O646mRbL3ZXzmbTYI4Q.jpeg)

Color Palette by [Bill S Kenney](https://medium.com/@billskenney) from [Focus Lab](https://dribbble.com/focuslab)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*Xp7PFUJXP58KHoFN7przgw.png)

Brian Johnson — Style Guide by [Tomasz Zagórski](https://medium.com/@tomaszzagorski) from [We Build Ideas](https://dribbble.com/webuild)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*4PN51uA641qi7P9fwGRIDQ.jpeg)

Scratch Track Style Guide by [Justin Renninger](https://medium.com/@justinr)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*TwWdsHhisxT_un5c_MvDZA.jpeg)

Commutr Brand Concept by [Zachary Burghardt](https://medium.com/@zburgh)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*hUKoNbcjQq9Unsd-u_FaCA.png)

Grovo UI Bible by [Alex Collins](https://dribbble.com/acolli) from [Grovo](https://dribbble.com/Grovo)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*N9deftjvRdnwMNy1dtG81Q.png)

Housing UI Style Guide by [Jonathan Howell](https://medium.com/@ItsJonHowell) from [Focus Lab](https://dribbble.com/focuslab)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*pHdu9yeW0y0XZKfPcyJd1Q.png)

UI Style Guide by [Greg](https://medium.com/@gregdlubacz) from [Templatly](https://dribbble.com/templatly)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*9mAGHE2dvizfZXouXbDuNA.gif)

Style Guide by [Sebastiano Guerriero](https://medium.com/@guerriero_se) from [CodyHouse](https://dribbble.com/CodyHouse)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*mAssJoQON2hyAN8DLwapRQ.png)

UI Style Guide by [Greg](https://medium.com/@gregdlubacz)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*R64B5OaL0jjuwx9WxClp7w.jpeg)

Commutr Brand Concept by [Zachary Burghardt](https://medium.com/@zburgh)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*7UWzowMlABV-Y6GYGLYaiw.png)

Salesforce1 UI Kit by [Brad Haynes](https://medium.com/@bradhaynes) from [Salesforce UX](https://medium.com/@SalesforceUX)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/image_8.png)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*8zmt4zS5Dk5VQgwaeY5jWw.png)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*qXdoknmkJcgwACwAvaakgA.png)

UI Style guide by [Randyjlee](https://medium.com/@randyjleehj)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*vNyMtuE3b3u3mNqx0VlcVg.png)

Style Guide Template by [Michael Leigeber](https://dribbble.com/leigeber) from [Caddis](https://dribbble.com/caddis)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*cYpGTAWnf9r1-KpQwI-3dw.jpeg)

Style Guide by [spovv](https://medium.com/@spovv)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*TrutBBuDtADZN33qrKeS0A.jpeg)

UI Style Guide Template (.psd) by [Medialoot](https://medium.com/@MediaLoot)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*FUx5_3c9jH-GAJYCxzDqxA.png)

Style Guide by [Jessica Ko](https://dribbble.com/kojessica)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*pww19aXfnB2xhg7ThAmtqg.png)

Rentinue Brand Guide by [Mike Busby](https://medium.com/@mikebusby)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*Vxy2AOn3jS9sJaa2fOkuhQ.png)

Affirm Android Guidelines by [John Francis](https://dribbble.com/jsf) from [Affirm, Inc.](https://medium.com/@AffirmInc)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*0taJRrQkpSfAYab8ULttKQ.png)

Style Guide by [Naoshad Alam](https://medium.com/@naoshad) d

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*iIpdZT4zKKjQd6yhLEb7Kw.png)

Style Guide Fun by [Emily Richard](https://medium.com/@e_wils)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*onkRKoZVR9e44jPRY3wXfw.png)

(Unused) Product Style Guide by [Sean Crebbs](https://dribbble.com/seancrebbs) from [FullScreens](https://medium.com/@fullscreens)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*L9dnaAUTwJPsPdgs_i4Weg.jpeg)

Process — Assets by [Alex Deruette](https://medium.com/@AlexDeruette) from [Kickpush](https://medium.com/@kickpush)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*MSo1e0HqyPo4wkIZzzgWLw.png)

Typeface and hierarchy by [Kerem Suer](https://medium.com/@kerem) from [Omada Health](https://medium.com/@OmadaHealth)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*8lBEhEcho7e3KsszAfN9yw.jpeg)

I love style guides :-) by [Leonid Jack](https://medium.com/@JacksDesignLab)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*RMGhbWKhuEyYJAG4Q_KCXg.png)

Logo Guide for Foodbooking by [Bota Iusti](https://dribbble.com/botaiusti)

* * *

...
*Don’t forget to press the* 💜 *button below!*

* [Style Guides](https://medium.com/tag/style-guides?source=post)
* [Design](https://medium.com/tag/design?source=post)
* [Colors](https://medium.com/tag/colors?source=post)
* [Typography](https://medium.com/tag/typography?source=post)
* [Branding](https://medium.com/tag/branding?source=post)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*vMN0HudGWx0fWPq8gL0NgA%202.png)

### \[Emma Drews](<https://medium.com/@emmadrews>)

Designer from England living in Belgium focusing on web and mobile. Hot chocolate addict ☕️. <https://dribbble.com/emmadrews>

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*VI--S-n6BnMn7X1g4M1y-g.png)

### \[Inspiration Supply](<https://medium.com/inspiration-supply?source=footer\_card>)

A daily dose of design inspiration to boost your creativity.

[More from Inspiration SupplyModern Identity and Logo Designs](https://medium.com/inspiration-supply/modern-identity-and-logo-designs-9a31af68b0a7?source=placement_card_footer_grid---------0-41)
![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*vMN0HudGWx0fWPq8gL0NgA%203.png)

[Emma Drews](https://medium.com/@emmadrews?source=placement_card_footer_grid---------0-41) 3 min read

[Related reads8-Point Grid: Borders and Layouts](https://builttoadapt.io/8-point-grid-borders-and-layouts-e91eb97f5091?source=placement_card_footer_grid---------1-40)
![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*rrrWuyapNIB72faNOq9rug.png)

[Elliot Dahl](https://builttoadapt.io/@Elliotdahl?source=placement_card_footer_grid---------1-40) 5 min read

[Also tagged DesignHow Fonts Are Fueling the Culture Wars](https://backchannel.com/how-fonts-are-fueling-the-culture-wars-f9d692101fea?source=placement_card_footer_grid---------2-43)
![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*Vclw3q_d2vNAk-EQM-_HPw.jpeg)

[Ben Hersh](https://backchannel.com/@MostlyCogent?source=placement_card_footer_grid---------2-43) 8 min read

Responses

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*Y0EcvN_OhAszj_fe.jpg)

Write a response…
Michael Hall

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*d6BBSTEGzmNpUSRCNtg_XQ.jpeg)

[Ross Angus](https://medium.com/@ross.angus?source=responses---------0-31----------) [Apr 12](https://medium.com/@ross.angus/these-are-very-pretty-but-lack-two-elements-in-my-opinion-a1f51911f506?source=responses---------0-31----------) ·1 min read

[These are very pretty, but lack two elements, in my opinion:1Some indication of when it is appropriate to use one style in favour of another (for example which of the fifteen different headings to choose)2Example markup](https://medium.com/@ross.angus/these-are-very-pretty-but-lack-two-elements-in-my-opinion-a1f51911f506?source=responses---------0-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*zQFQCr5DHUjH7a5Ym1E42g.jpeg)

[Jon Moore](https://medium.com/@jon.moore?source=responses---------1-31----------) [Apr 12](https://medium.com/@jon.moore/heres-another-one-to-add-great-collection-emma-972e8586b87?source=responses---------1-31----------)

[Here’s another one to add! Great collection, Emma!](https://medium.com/@jon.moore/heres-another-one-to-add-great-collection-emma-972e8586b87?source=responses---------1-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/1*IXjGCyJOMpFUqfAal6kKgw.jpeg)

[Leon Ephraïm](https://medium.com/@leonephraim?source=responses---------2-31----------) [Apr 9](https://medium.com/@leonephraim/nice-roundup-885ae5a0e3d7?source=responses---------2-31----------)

[Nice roundup. I think this one would fit in nicely too 👉 https://dribbble.com/shots/3366716-Styleguide-Car-Project](https://medium.com/@leonephraim/nice-roundup-885ae5a0e3d7?source=responses---------2-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*cxEWhtAcnYkdlPd2.jpeg)

[Osvaldo Uribe](https://medium.com/@osvaldouribe?source=responses---------3-31----------) [Apr 17](https://medium.com/@osvaldouribe/we-are-working-in-a-very-small-tool-to-help-designer-to-do-digital-graphic-manuals-lets-take-a-8417d29077c1?source=responses---------3-31----------)

[We are working in a very small tool to help designer to do Digital Graphic Manuals, let’s take a look. http://emble.ma ;)](https://medium.com/@osvaldouribe/we-are-working-in-a-very-small-tool-to-help-designer-to-do-digital-graphic-manuals-lets-take-a-8417d29077c1?source=responses---------3-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*2UjeTokCLkizl3Ls.jpg)

[Duc Hong](https://medium.com/@duchong?source=responses---------4-31----------) [Apr 8](https://medium.com/@duchong/wow-very-informative-and-inspiring-post-thanks-for-sharing-with-us-7471a5f0f3a5?source=responses---------4-31----------)

[Wow, very informative and inspiring post, thanks for sharing with us](https://medium.com/@duchong/wow-very-informative-and-inspiring-post-thanks-for-sharing-with-us-7471a5f0f3a5?source=responses---------4-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*wCmoyT7w-9RIMJMR.jpeg)

[iSands](https://medium.com/@iSands?source=responses---------5-31----------) [Apr 10](https://medium.com/@iSands/this-is-beautiful-emma-thanks-for-sharing-and-for-the-time-you-invested-in-this-great-piece-678b36a943b1?source=responses---------5-31----------)

[This is beautiful, Emma! Thanks for sharing and for the time you invested in this great piece.](https://medium.com/@iSands/this-is-beautiful-emma-thanks-for-sharing-and-for-the-time-you-invested-in-this-great-piece-678b36a943b1?source=responses---------5-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*YdnNhzrUIN8F_dXp..jpg)

[Junsang Dong](https://medium.com/@junsangdong?source=responses---------6-31----------) [Apr 10](https://medium.com/@junsangdong/awesome-read-emma-thanks-for-the-article-4bca41cdb993?source=responses---------6-31----------)

[Awesome read Emma, Thanks for the article.](https://medium.com/@junsangdong/awesome-read-emma-thanks-for-the-article-4bca41cdb993?source=responses---------6-31----------)

Recommended by [Emma Drews](https://medium.com/@emmadrews) (author)

![](Style%20Guides%20by%20Pro%20Designers%20%E2%80%93%20Inspiration%20Supply%20%E2%80%93%20Medium/0*sFD8CtSNW99GbI1e.jpg)

[bmsheahan](https://medium.com/@bmsheahan?source=responses---------7-31----------) [Apr 9](https://medium.com/@bmsheahan/love-this-inspiration-design-value-great-piece-thanks-2fcb94cb3923?source=responses---------7-31----------)

[Love this – inspiration, design, value. Great piece. Thanks.](https://medium.com/@bmsheahan/love-this-inspiration-design-value-great-piece-thanks-2fcb94cb3923?source=responses---------7-31----------)

<https://cdn-images-1.medium.com/max/800/1*8lBEhEcho7e3KsszAfN9yw.jpeg>

\#design #UI