# Predoc TAC Info

```
  
```

# Predoc TAC Info

\#tac #embl
TAC must be assembled in first 6 months
QA within the first 9 months
Times are from the start of your contract. i.e includes predocs course.
TAC must be at least 4 members

* Your supervisor
* A PI from the same unit
* A PI from another unit
* University advisor (speak with Tracey about criteria).

Additional members can be added as required.
Make sure to meet the people beforehand. You need to feel comfortable with them.
Thesis is not to exceed 60,000 words ([from](https://www.cambridgestudents.cam.ac.uk/your-course/examinations/graduate-exam-information/submitting-and-examination/phd-msc-mlitt/word#biology))
1st TAC - Well-defined thesis project including risk assessment. Predoc is well acquainted with the thesis project and it’s scientific background.
Internal policy IP48 - Predoctoral fellows at EMBL.