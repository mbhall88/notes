# SSH Keypair | Joint HPC Exchange

```
  
```

# SSH Keypair | Joint HPC Exchange

# Setting Up your local hosts for password-less login via SSH

This is a technology based on [public-key cryptography](http://en.wikipedia.org/wiki/Public-key_cryptography). The idea is to set up your desktop/laptop so that you are recognized by our login servers without you having to enter your password every time . In the documentation below your desktop/laptop is referred to as the “local host” and our login server(s) are referred to as the “remote host”. There is a one-time setup for each of the local hosts that you want to set up with keypair authentication.

**Instructions for unix-based machines (e.g. linux and mac osx)**

First, on your local host, cd into your home directory and invoke the ssh key generator:

```
ssh-keygen -t rsa
```

The key generator will ask a bunch of questions. If you don’t understand a question, just hit return in response to the question. The default configuration is just fine. The key generator will put two files in the .ssh subdirectory of your home directory, i.e. ~/.ssh/id_rsa and ~/.ssh/id_rsa.pub . The former contains your private key while the latter contains your public key. You should only ever have to run the ssh key generator once on your local host. **\*If you have already configured passwordless login and you run the key generator a second**\* **\*time, it will overwrite your previous public and private key files. This will break all password-less logins that you set up with your previous keys.**\*

Next you want to copy your public key file to the remote host and append it to the authorized_keys file in the .ssh subdirectory of your home directory on the remote host (one of the jhpce login servers, e.g. jhpce01 – jhpce04). If there is no ~/.ssh directory in the remote host, you will need to login to the remote host and create a .ssh subdirectory in your home directory. You can perform the copy and append operations in one line as follows;

```
cat ~/.ssh/id_rsa.pub | ssh @jhpce01.jhsph.edu \
'cat >> ~/.ssh/authorized_keys'
```

Where you replace with your JHPCE userid and where you enter your JHPCE password when you are prompted for it by ssh.

To test that everything is working you should be able to log into the remote host from your local host with the following command:

```
ssh @jhpce01.jhsph.edu
```

**Fine points and troubleshooting**

1. Many users set up an alias for the ssh command so they don’t have to type as much to log into the remote host. For example, suppose I want to setup my local host so all I have to do is type ‘hpc’ to log into the cluster, i.e.:

```
hpc
```

You can do this by adding the following line to your ~/.bashrc

```
alias hpc='ssh -X @jhpce01.jhsph.edu'
```

2. If you followed the directions and ssh is still asking for a password, then it is likely that the permissions of the ~/.ssh directory (or the files in it) on the remote host, are not set correctly. To fix the permissions, execute the following command on the remote host:

```
chmod -R o-rwx ~/.ssh
```

Finally, if your changed the permissions of your .ssh directory and you are still being asked for a password, then perhaps the permissions on your home directory are such that others are allowed to write to it. If this is the case, then this needs to be changed so that only you can write to it. Contact bit support to have this changed.

**Instructions for Windows machines**

If your desktop/laptop runs MIcrosoft Windows then you first need to install puTTY on your windows machine. After puTTY installed, follow the these directions:

1. Launch puttygen.exe.


2. Click “generate” and move your mouse around randomly in the blank area. Add a passphrase if you’d like, though this is not necessary. Click ‘Save private key’ and type in a file name.


3. Select and copy the text from the area labeled “Public key for pasting into Open Ssh …”. Next SSH to jhpce01.


4. log into our login server using your password and cd to your .ssh directory. Edit your “authorized_keys” file. Paste the text you just copied into the end of your authorized_keys file and save it. Close puTTY.


5. Next create and save a session in puTTY. Open up puTTY again. For the hostname type in [YOUR_USER_NAME@jhpce01.jhsph.edu](mailto:YOUR_USER_NAME@jhpce01.jhsph.edu). In the left had panel click on “ssh” and then “auth”. Browse to you private key that saved earlier. In the left hand panel, click on “session” then under “saved session” type a name. Any name will do buy “[jhpce01.biostat.jhsph.edu](http://jhpce01.biostat.jhsph.edu)” is memorable. Click on save.

You should only have to perform the setup steps (1-5) just once. From now on to get to get to jhpce01, open putty, load your saved session, and click on “open”.

You can create a shortcut for this by right clicking in a directory then “new” then “shortcut”. Have the target of the shortcut be:

```
"PATH TO PUTTY\PUTTY.EXE" -load "NAME OF SAVED PUTTY SESSION"
```

For example, we use:

```
"C:\Users\my_dirname\putty\PUTTY.EXE" -load "jhpce01.jhsph.edu"
```

Now you can double click on that icon and it will automatically bring up a putty session on jhpce01 without requiring a password.

**Configuring SSH Keys in Filezilla**

* Open a Terminal window on your Mac
* Do you already have SSH keys? You can check by running “ls -la ~/.ssh/id\_\*”
* If you do not already have SSH keys for your account on your Mac, Run “ssh-keygen -t rsa” , and just hit “Enter” when prompted for questions.
* You will need to copy your public key to the JHPCE cluster, so on your mac. The easiest way is to run:

$ cat .ssh/id_rsa.pub | ssh @jhpce01.jhsph.edu “cat >> ~/.ssh/authorized_keys”

* If you hadn’t set up ssh keys, this will allow you to ssh into the JHPCE cluster without a password. You can test this by running “ssh [jhpce01.jhsph.edu](http://jhpce01.jhsph.edu)”

To then add your key to Filezilla:

* Start Filezilla, and go to “Filezilla” “Settings”
* In the Settings screen, go to “Connections SFTP”
* Click the “Add keyfile…” button
* You will need to go to the hidden .ssh directory on your Mac. To do so, press --G . This will open the “Go to the folder” prompt.

– Enter “~/.ssh” , and click on “Go”
– Select “id_rsa”
– You will then get a Filezilla prompt about converting the file to a supported format. Click “Yes”
– Give the file a name (like JHPCE-rsa) , and click “Save”

* Now, in Filezilla, go to “File” “Site Manager” Click “New Site”
* In the Host field, enter **[transfer01.jhpce.jhu.edu](http://transfer01.jhpce.jhu.edu)**. (The **transfer01** system should be used instead of jhpce01 for transferring files)
* For “Protocol”, select “ **SFTP** ”
* For “Login Type”, select “Normal”
* For “User”, enter your JHPCE User ID.
* Clear out the “Password” field.
* On the left side, rename “New Site” to “JHPCE” (or something similar)
* Click “OK”

You can now connect to the JHPCE cluster using Filezilla by clicking “Connect” for that site. There is a shortcut to the “Sites” list at the upper left corner of the main Filezilla screen.

<https://jhpce.jhu.edu/knowledge-base/authentication/login/>

\#unix