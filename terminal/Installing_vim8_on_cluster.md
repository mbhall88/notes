# Installing vim8 on cluster

This installation was a bit complex as I needed to compile with python3.6 in order to get the `black` extension to work.

Download latest release
```sh
cd /nfs/research1/zi/mbhall/Software/
wget https://github.com/vim/vim/archive/v8.1.1282.tar.gz -O - | tar -xzf -
cd vim
```

Then need to configure with some python3.6 paths

```sh
LDFLAGS=-L/nfs/research1/zi/software/Python-3.6.4/lib/ LIBS=-lpython3.6m ./configure \
  --enable-fail-if-missing \
  --disable-perlinterp \
  --disable-pythoninterp \
  --enable-python3interp \
  --with-python3-command=/nfs/research1/zi/software/Python-3.6.4/bin/python3.6 \
  --disable-rubyinterp \
  --enable-cscope -\
  -enable-gui=auto \
  --with-features=huge \
  --enable-multibyte \
  --with-compiledby="$USER" \
  --prefix=$(realpath ../)
  make
  make check
  make install
```