# Mount cluster directory locally

```sh
# mount
sshfs ebi-cli-ext:<remote_dir> <local_dir>
# unmount
fusermount -u <local_dir>
# unmount (issues)
umount -l <local_dir>
```
**NEVER DELETE THE MOUNTED FOLDER. IT WILL DELETE THE REMOTE DIR TOO!!!**

Issues, see: https://linuxdiscoveries.blogspot.com/2012/02/how-to-force-unmount-busy-device-in.html