# Install boost

```
  
```

# Install boost

\#boost
I generally do this from `/usr/local/`
Other versions can be found at <https://sourceforge.net/projects/boost/files/boost/>

```
sudo wget https://sourceforge.net/projects/boost/files/boost/1.62.0/boost_1_62_0.tar.gz -O - | sudo tar xzf -
cd boost_1_62_0
sudo ./bootstrap.sh --prefix=/usr/local --with-libraries=system,filesystem,iostreams,log,thread,date_time
sudo ./b2 install
```

Or on the cluster

```
wget https://sourceforge.net/projects/boost/files/boost/1.62.0/boost_1_62_0.tar.gz -O - | tar xzf -
cd boost_1_62_0
./bootstrap.sh --prefix=/nfs/research1/zi/software --with-libraries=system,filesystem,iostreams,log,thread,date_time
./b2 install
```

To uninstall

```
# If you ever want to uninstall Boost, this should do it:
cd
for pfx in /usr /usr/local;
do
  sudo find ${pfx}/include -maxdepth 1 -type d -name "boost-*"\
   -exec rm -r {} \;
  test -d ${pfx}/include/boost && rm -r ${pfx}/include/boost
  sudo rm -f ${pfx}/lib*/libboost_*
done
ldconfig
exit
sudo find ~/src -maxdepth 1 -type d -name "boost_*" -exec rm -r {} \;
sudo rm -f ~/installed/boost_*.tar.*
```

To do this on the cluster

```
pfx=/nfs/research1/zi/software
find ${pfx}/include -maxdepth 1 -type d -name "boost-*"\
   -exec rm -r {} \;
test -d ${pfx}/include/boost && rm -r ${pfx}/include/boost
rm -f ${pfx}/lib*/libboost_* 
```