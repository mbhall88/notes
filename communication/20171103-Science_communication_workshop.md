# Science communication workshop

```
  
```

# Science communication workshop

\#science communication#

## Create clear content

1. **Set your goal**. Spend some time thinking about what your audience will be experiencing (i.e time of day, background, interests etc.). Think about what your goal is for content to deliver to them. **Write it down!**
2. **1 take home message. 3 core messages.** 3 weeks after your talk, what should be in people’s minds? If you had 30 seconds, what would you say? All 3 core messages support the take home message. For each core message, have 3-5 sub-messages.
3. **Structure.**

![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%20112030%20am.jpg)
How to present data diagrams:

1. **Preview** - talk about the slide *before* you show it. What data was used to create it? How was the data generated?

————SHOW SLIDE————

2. **Guide** - What do the audience see? This is like the legend on a figure, explain what axes are, colours, etc. Say something like *“What you see here is…”*
3. **Highlight** - more detail/complexity about what the figure shows. Similar to the figure caption.
4. **Spin** - rephrase in general terms. *”And what that means is…”*.

In written science communication, redundancy is not necessary. You don’t repeat yourself. In oral communication, you must repeat yourself.

4. **Illustrate.** Search for metaphors, examples, comparisons you can use to clearly get the message across to the audience.

> After all of this, you may open PowerPoint.

Think about the environment you are presenting in. If it is a lab meeting you likely want to illustrate your short-comings and **invite people to criticise you!**

A good example of point 4 is [this TED talk](https://www.ted.com/talks/bonnie_bassler_on_how_bacteria_communicate) from Bonnie Bassler.

## Slide design for scientific presentations

Who is my audience? What do they know already? What visuals will be beneficial to get my message across?
Reduce the number of “alignments” on the slide. i.e columns.

> Build your slides on messages - not on topics!

How many slides for a 20 minute talk? General rule is allow 2 minutes for each slide.
Use presenter mode! You can see what the next slide is. You can move all your dot points to the notes section on the presenter mode, thus freeing up your slides of clutter.
Have you slides and then also have something people can take home. Possibly have a link to public version of slides with notes on what you said on each.

## How to start and end

Have 2 titles:

1. For publishing that is complex and thus searchable.
2. For conferences that invites people to come to your talk.

Include a blank/black slide so that the attention must be on you.
Don’t use an agenda slide - just tell people what you are going to talk about.
Start with a nice fact or question about your talk. In other words, start with an audience-centred thing, rather than a self-centred thing. Then you have a slide about yourself and where you come from etc.

Use SITCOM-Q-A format for introduction.

* SIT - **Situation**. Broad and general.
* COM - **Complication.**
* Q - **Question.**
* A - **Answer.** Could use take home message here.

Good presenters jump back and forth between what is state-of-the-art and what “could be”. Always finish with what could be - based on your work. Start with what is current.
You can start with brain cinema (metaphors, examples etc.) before even going into SITCOM-Q-A.

![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%2090040%20pm.jpg)
![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%2090055%20pm.jpg)
![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%2090115%20pm.jpg)

## Slide design

Is about making communication as easy and clear for the viewer as possible.
Reduce
Recommended **no more than 25 words.**
Reduce the parts of charts that aren’t important and bring the important ones to the front (i.e de-colour all lines except the one of interest).
Align
Margins keep your content contained and also mean projector issues will likely not effect you.

## Mastering Q&A

![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%2090527%20pm.jpg)
![](Science%20communication%20workshop/Photo%203%20Nov%202017%20at%2090542%20pm.jpg)