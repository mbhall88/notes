# revealjs tips

```
  
```

# revealjs tips

### Export to PDF

To export a #revealjs presentation to PDF using #decktape run:

```
decktape automatic /path/to/html /save/as.pdf -s 2560x1440
```

May need to play around with the size (`-s`) setting if the layout comes out all weird.

### Install

```
npm install -g decktape
```

The docs can be found at [GitHub - astefanutti/decktape: PDF exporter for HTML presentation frameworks](https://github.com/astefanutti/decktape)

### Code styles

To view different code syntax highlighting options, go to [highlight.js demo](https://highlightjs.org/static/demo/).
All styles should be within `reveal.js/lib/css/`

### Host on GitHub

Set up using info from [Stackoverflow post](https://stackoverflow.com/questions/31163633/how-to-host-a-reveal-js-presentation)

Now all I have to do is add a new presentation to this repository and it will be accessible at `mbhall88.github.io/reveal_presentations/pres.html`